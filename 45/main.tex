\input{header/report.tex}
\addbibresource{lit.bib}
\SetExperimentNumber{45}

\begin{document}
  \maketitlepage{NMR-Spektroskopie an Hydraten}{12.05.2014}{06.10.2014}
  \section{Ziel}
    Ziel des Experiments ist es, die Lage der Wasserstoff-Kerne in einem Gips-Kristall zu bestimmen.
  \section{Theorie}
    Die magnetischen Momente einer Probe können durch eine äußeres Magnetfeld teilweise orientiert werden, so dass eine makroskopische Magnetisierung entsteht.
    Durch ein zweites, hoch-frequentes Magnetfeld kann Energie der Probe zugeführt werden, wobei die Absorption von der Frequenz abhängig ist.
    Die Frequenzen der Resonanzstellen in diesem Spektrum können verwendet werden, um die Position von magnetischen Momenten in der Einheitszelle der Kristallprobe zu berechnen.
    \subsection{Resonanzfrequenz in einem äußeren Feld}
      Für einen quantenmechanischen Drehimpuls $\v{J}$ gilt
      \begin{eqn}
        \v{J}² = ℏ² J \g(J + 1) \\
        J_z = ℏ m \., \quad m \in \g\{-J, -J + 1, \ldots, J - 1, J\} \.,
      \end{eqn}
      wobei $ℏ$ die Planck-Konstante ist.
      Mit dem Drehimpuls ist ein magnetisches Moment
      \begin{eqn}
        \v{μ} = γ \v{J}
      \end{eqn}
      verbunden, wobei $γ$ das teilchenspezifische gyromagnetische Verhältnis ist.
      Die Energie eines magnetischen Momentes in einem Magnetfeld $\v{B}₀ = B₀ \uv{z}$ ist gegeben durch
      \begin{eqn}
        E = - \v{B}₀ \cdot \v{μ} = - ℏ γ B₀ m \..
      \end{eqn}
      Für den Energieabstand der aufgespaltenen Niveaus gilt somit
      \begin{eqn}
        \Del{E} = ℏ γ B₀ \..
      \end{eqn}
      Übergänge zwischen den Niveaus können durch elektromagnetische Strahlung der zugehörigen Frequenz
      \begin{eqn}
        ω₀ = \frac{\Del{E}}{ℏ} = γ B₀
      \end{eqn}
      angeregt werden.
    \subsection{Bloch’sche Gleichungen}
      Obwohl ein Teilchen quantenmechanisch beschrieben werden muss, kann ein System aus vielen Teilchen klassisch betrachtet werden.
      Dies wird in der Herleitung der Bloch’schen Gleichungen verwendet.

      Auf ein magnetisches Moment $\v{μ}$ in einem homogenen Magnetfeld $\v{B}₀$ wirkt das Drehmoment
      \begin{eqn}
        \v{μ} \times \v{B}₀ = \d{t}{\v{J}}; \.,
      \end{eqn}
      was als
      \begin{eqn}
        \d{t}{\v{μ}}; = γ \v{μ} \times \v{B}₀
      \end{eqn}
      ausgedrückt werden kann.
      Für die Magnetisierung
      \begin{eqn}
        \v{M} = \sum_i \v{μ}_i
      \end{eqn}
      gilt also
      \begin{eqn}
        \d{t}{\v{M}}; = γ \v{M} \times \v{B}₀ \..
      \end{eqn}

      Im thermischen Gleichgewicht gilt
      \begin{eqn}
        \v{M}₀ = χ₀ \v{B}₀ \.,
      \end{eqn}
      wobei $χ₀$ die statische Suszeptibilität der Probe ist.
      Das System bewegt sich in Richtung des Gleichgewichts mittels
      \begin{eqn}
        \eval{\d{t}{\v{M}};}_{\t{Relaxation}} = \frac{\v{M}₀ - \g(\uv{M}₀ \cdot \v{M}) \uv{M}₀}{T₁} + \frac{\v{M}₀ - \g[\v{M} - \g(\uv{M}₀ \cdot \v{M}) \uv{M}₀]}{T₂} \.,
      \end{eqn}
      wobei $T₁$ die longitudinale und $T₂$ die transversale Relaxationszeit sind.
      Insgesamt ergeben sich die Bloch’schen Gleichungen
      \begin{eqn}
        \d{t}{\v{M}}; = γ \v{M} \times \v{B}₀ + \frac{\v{M}₀ - \g(\uv{M}₀ \cdot \v{M}) \uv{M}₀}{T₁} + \frac{\v{M}₀ - \g[\v{M} - \g(\uv{M}₀ \cdot \v{M}) \uv{M}₀]}{T₂} \..
      \end{eqn}
    \subsection{Lösung der Bloch’schen Gleichungen und die Suszeptibilität in einem Hochfrequenzfeld}
      Wird die Probe in das Magnetfeld $\v{B} = \v{B}₀ + \v{B}₁$,
      \begin{eqn}
        \v{B}₀ = B₀ \uv{z} \\
        \v{B}₁ = B₁ \g(\cos.ω t. \uv{x} - \sin.ω t. \uv{y}) \.,
      \end{eqn}
      platziert, ergibt sich für die Bloch’schen Gleichungen
      \begin{eqns}
        \d{t}{M_z}; &=& - γ B₁ \g(M_x \sin.ω t. + M_y \cos.ω t.) + \frac{M₀ - M_z}{T₁} \\
        \d{t}{M_x}; &=& \hphantom{-} γ B₀ M_y + γ B₁ M_z \sin.ω t. - \frac{M_x}{T₂} \\
        \d{t}{M_y}; &=&           -  γ B₀ M_x + γ B₁ M_z \cos.ω t. - \frac{M_y}{T₂} \..
      \end{eqns}
      Mit dem Ansatz
      \begin{eqns}[rCcl]
        M_x &=&   & \f{A}(ω) \cos(ω t + \f{φ}(ω)) \\
        M_y &=& - & \f{A}(ω) \sin(ω t + \f{φ}(ω))
      \end{eqns}
      folgt
      \begin{eqn}
        \f{φ}(ω) = \arctan(\frac{-1}{\g(ω₀ - ω) T₂}) \\
        \f{A}(ω) = \frac{γ B₁ M_z T₂}{\sqrt{1 + \g(ω₀ - ω)² T₂²}} \..
      \end{eqn}
      Daraus folgt für die Quermagnetisierung
      \begin{eqn}
        \v{B}₀ \perp \v{M}_\perp = A \g[\g(\cos.ω t. \cos.φ. - \sin.ω t. \sin.φ.) \uv{x} - \g(\sin.ω t. \cos.φ. + \cos.ω t. \sin.φ.) \uv{y}] \.,
      \end{eqn}
      woraus durch Vergleich mit der Definition von $\v{B}₁$
      \begin{eqns}
        \v{B}₁ \parallel \v{M}_\perp' &=& A \cos.φ. \g(\cos.ω t. \uv{x} - \sin.ω t. \uv{y}) \\
        \v{B}₁ \perp \v{M}_\perp'' &=& - A \sin.φ. \g(\sin.ω t. \uv{x} + \cos.ω t. \uv{y})
      \end{eqns}
      folgt.
      Dabei ist $\v{M}_\perp''$ um $\PI / 2$ gegenüber $\v{B}₁$ phasenverschoben.

      Wird die komplexe Suszeptibilität $χ = χ' + \I χ''$ definiert, so ergibt sich mit $\v{M} = χ \v{B}$
      \begin{eqn}
        \f{χ'}(ω) = \frac{A \cos.φ.}{B₁} = χ₀ \frac{ω₀ \g(ω₀ - ω) T₂²}{1 + \g(ω₀ - ω)² T₂²} \\
        \f{χ''}(ω) = - \frac{A \sin.φ.}{B₁} = χ₀ \frac{ω₀² T₂²}{1 + \g(ω₀ - ω)² T₂²} \.,
      \end{eqn}
      wobei $χ₀ = M_z / B₀$ die statische Suszeptibilität ist.
      Der Realteil $χ'$ und der Imaginärteil $χ''$ der Suszeptibilität heißen Dispersions- und Absorptionsteil.
    \subsection{Einfluss von Kerndipolfeldern auf das NMR-Signal}
      Das Spektrum eines Kerns ist durch das Gesamtmagnetfeld $\v{B}_{\t{ges}} = \v{B}₁ + \v{B}_{\t{loc}}$ bestimmt.
      Hier wird angenommen, dass die Hüllenlektronen keinen Beitrag liefern und dass jeder Kern sich im Magnetfeld genau eines anderen Kerns befindet.
      Der Einfluss des anderen Kerns wird durch quantenmechanische Störungstheorie erster Ordnung berechnet.
      Es werden also die Spins $\v{J}$ und $\v{J}'$ der beiden Kerne betrachtet.
      Für den Hamiltonoperator des Gesamtsystems gilt
      \begin{eqn}
        \mathcal{H} = \mathcal{H}₀ + \mathcal{H}₁ \\
        \mathcal{H}₀ = - ℏ γ B₀ \g(J_z + J'_z) \\
        \mathcal{H}₁ = \frac{μ₀}{4 \PI} ℏ² γ² \frac{1}{r³} \g[\v{J} \cdot \v{J}' - 3 \g(\v{J} \cdot \uv{r}) \g(\v{J}' \cdot \uv{r})] \.,
      \end{eqn}
      wobei $μ₀$ die magnetische Feldkonstante und $\v{r}$ der Verbindungsvektor der Kerne sind.
      Es ist nützlich, die Eigenzustände $\ket{1_{\t{s}}} = \ket{↑↑}$, $\ket{-1_{\t{s}}} = \ket{↓↓}$, $\ket{0_{\t{s}}} = \g(\ket{↑↓} + \ket{↓↑}) / \sqrt{2}$ und $\ket{0_{\t{a}}} = \g(\ket{↑↓} - \ket{↓↑}) / \sqrt{2}$ von $\mathcal{H}₀$ zu verwenden.
      Der vierte Zustand ist antisymmetrisch, während alle anderen symmetrisch sind, also finden kaum Übergänge mit dem vierten Zustand statt.
      Aus diesem Grund kann der antisymmetrische Zustand ignoriert werden.
      Für die Energien der Zustände in erster Ordnung Störungstheorie gilt
      \begin{eqn}
        \bra{±1_{\t{s}}} \mathcal{H} \ket{±1_{\t{s}}} = ∓ ℏ γ B₀ ± \frac{μ₀}{4 \PI} \frac{ℏ² γ²}{4} \frac{1}{r³} \g(1 - 3 \cos^2.θ.) \\
        \bra{0_{\t{s}}} \mathcal{H} \ket{0_{\t{s}}} = - 2 \frac{μ₀}{4 \PI} \frac{ℏ² γ²}{4} \frac{1}{r³} \g(1 - 3 \cos^2.θ.) \.,
      \end{eqn}
      wobei $θ$ der Winkel zwischen $\v{B}₀$ und $\v{r}$ ist,
      also gibt es zwei Resonanzfrequenzen
      Abbildung \ref{fig:theory_ΔB(θ)} enthält den zu $\Del{B}$ proportionalen Verlauf der Differenz der Resonanzfrequenzen in Abhängigkeit des Winkels $θ$.
      \begin{figure}
        \includegraphics{graphic/theory.pdf}
        \caption{Theoretischer Verlauf der zu $\Del{B}$ proportionalen Resonanzfrequenzendifferenz (aus Formel \eqref{eqn:aufspaltung}) als Funktion des Winkels $θ$.}
        \label{fig:theory_ΔB(θ)}
      \end{figure}
      \begin{eqn}
        ω_± = γ B₀ ± \frac{3}{ℏ} \frac{μ₀}{4 \PI} \frac{ℏ² γ²}{4} \frac{1}{r³} \g(1 - 3 \cos^2.θ.) \.. \label{eqn:aufspaltung}
      \end{eqn}
    \subsection{Gips-Einkristall}
      Gips (\ce{CaSO4.2H2O}) kristallisiert in ein monoklines Einkristall, wobei jede Einheitszelle vier Moleküle \ce{CaSO4} und acht Moleküle \ce{H2O} enthält.
      Es existieren zwei Lagen von Verbindungslinien zwischen Protonen des Kristallwassers, gegeben durch $P - P'$ und $P - P''$.
      Diese sind in den Abbildungen \ref{fig:gypsum1} und \ref{fig:gypsum2} gezeigt, wobei $A'$ und $A''$ die Projektionen der Punkte $P'$ und $P''$ auf die (001)-Ebene sind.
      Hieraus folgt, dass es die Winkel $θ'$ und $θ''$ zwischen einem Magnetfeld in der (001)-Ebene und einer Verbindung zweier Protonen gibt.
      Im Allgemeinen ergeben sich also vier Resonanzfrequenzen.
      \begin{figure}
        \includegraphics{graphic/gypsum1.pdf}
        \caption{Darstellung der $\g(001)$-Ebene im Gips-Kristall zur Veranschaulichung des Winkels $φ$ \cite{anleitung45}.
                 $A'$ und $A''$ sind die Projektionen der Punkte $P'$ und $P''$ in die $\g(001)$-Ebene.}
        \label{fig:gypsum1}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/gypsum2.pdf}
        \caption{Lage der Dipolpaare relativ zu den Kristall-Achsen im Gips-Kristall \cite{anleitung45}.}
        \label{fig:gypsum2}
      \end{figure}

      Da die Größen $θ'$ und $θ''$ nicht direkt messbar sind, werden die Größen $φ$, $φ₀$ und $δ$ verwendet.
      Diese sind die Winkel zwischen dem Magnetfeld und der [100]-Richtung, zwischen $P - A'$ beziehungsweise $P - A''$ und der [100]-Richtung und zwischen $P - P'$ und $P - A'$.
      Dann gilt
      \begin{eqns}
        \cos.θ'.  &=& \cos.δ. \cos(φ - φ₀) \\
        \cos.θ''. &=& \cos.δ. \cos(φ + φ₀) \..
      \end{eqns}
      Es gilt außerdem $φ₀ = \SI{54.5}{\degree}$.

      Durch diese Überlegungen können $δ$ und $r$, die Länge von $P - P'$ und $P - P''$, bestimmt werden.
      Bei $φ = α - φ₀$, $α = \SI{53.5}{\degree}$ findet keine Aufspaltung statt.
      Dann gilt
      \begin{eqn}
        \cos.θ. = \frac{1}{\sqrt{3}} \.,
      \end{eqn}
      woraus $δ$ zu
      \begin{eqn}
        δ = \arccos.\frac{1}{\sqrt{3} \cos.α.}. \approx \SI{13.9}{\degree}
      \end{eqn}
      bestimmt werden kann.
      Damit gilt
      \begin{eqns}
        r &=& \sqrt[3]{2 \frac{3}{ℏ} \frac{μ₀}{4 \PI} \frac{ℏ² γ}{4} \frac{1}{\Del{B}} 3 \cos^2.δ. \cos^2(φ - φ_0)} \\
          &=& \sqrt[3]{\frac{3 ℏ μ₀ γ}{8 \PI} \frac{1}{\Del{B}} \frac{\cos^2(φ ± φ_0)}{\cos^2.α.}} \.,
      \end{eqns}
      wobei $\Del{B}$ die Magnetfelddifferenz zweier Resonanzstellen ist.
      Dabei gilt beim $±$ das $+$ für die Differenz der inneren Stellen und das $-$ für die der äußeren.
  \section{Aufbau und Durchführung}
    Zur Veranschaulichung ist in Abbildung \ref{fig:setup} ein schematischer Versuchsaufbau zu sehen.
    \begin{figure}
      \includegraphics[width=\textwidth]{graphic/setup.pdf}
      \caption{Schematischer Versuchsaufbau \cite{anleitung45}.}
      \label{fig:setup}
    \end{figure}
    Die Probe wird zwischen zwei große Polschuhe des Durchmessers \SI{10}{\centi\meter} gebracht, die das Magnetfeld $B_0$ erzeugen.
    Zudem befindet sie sich in einer gewickelten Hochfrequenzspule, die das Magnetfeld $B_1$ erzeugt.
    Die hochfrequente Spule wiederum ist mit den anderen Bauelementen zu einer TT-Brücke zusammengeschaltet.
    Vorteile der hier beschriebenen Apparatur sind eine hohe Messgenauigkeit und die Möglichkeit, den Dispersions- und Absorptionsanteil getrennt zu bestimmen.

    In der Nähe einer Resonanzstelle folgt aus der Änderung der Suszeptibilität der Probe auch eine Änderung der Induktivität
    \begin{eqn}
      L = L_0 \g(1 + \f{χ}(ω))
    \end{eqn}
    der Spule.
    Die Spule ist mit zwei variablen Kondensatoren $C_φ$ und $C_{\t{a}}$ und einem auf die Resonanzfrequenz $ω_0$ eingeregelten Resonanzfrequenzkreis zusammengeschaltet.
    Durch eine Impedanzbetrachtung des Schaltkreises für die Annahme kleiner Suszeptibilitäten stellt sich heraus, dass die Probe im Resonanzbereich nur einen kleinen Effekt auf die Änderung der Spannung hat.
    Durch eine Brückenschaltung wird daher der probenunabhängige Teil der Spannung kompensiert.
    Die nun messbare Spannung enthält, gemischt, die Dispersions- und Absorptionsanteile der Spannung.

    Die Brückenschaltung gestattet die Änderung der relativen Phase und der relativen Amplitude der an der Brückenschaltung anliegenden Spannungen $U_1$ und $U_2$ durch Ändern der Kapazitäten $C_φ$ und $C_{\t{a}}$.
    Der Dispersionsanteil wird gemessen, wenn die Beträge der Spannungen gleich sind, sich die Spannungen aber etwas in der Phase unterscheiden.
    Der Absoprtionsanteil wird gemessen, wenn die beiden Spannungen genau gegenphasig sind und sich im Betrag etwas unterscheiden; hier wird im Folgenden nur der Dispersionsanteil gemessen.

    Es ist erforderlich, die gemessene Spannung zu verstärken und das Signal-Rausch-Verhältnis zu verbessern.
    Zur Verstärkung eines schmalen Bandes um die Resonanzfrequenz wird ein Lock-In-Verstärker verwendet.
    Die Referenzspannung wird von diesem zur Verfügung gestellt und ist an den großen Magneten angeschlossen.
    Der konstante Strom von circa \SI{5}{\ampere} wird daher von einem Wechselstrom aus dem Lock-in-Verstärker überlagert und das große Magnetfeld moduliert.

    Aus praktischen Gründen wird nicht das hochfrequente Feld im Bereich von \SI{20}{\mega\hertz} variiert, sondern das statische Magnetfeld mit einem Potentiometer variiert.
    Damit ein Gleichstrom entsteht, wird dies mit einem Potentiometer realisiert, dass gleichmäßig von einem Motor geregelt wird.
    Die Modulation des Feldes wird klein gegenüber der Halbwertsbreite der Resonanzkurve $\f{χ}(B)$ gewählt; der $x$-$y$-Schreiber zeichnet dann das differenzierte Signal der Suszeptiblität bezüglich des Magnetfeldes auf.
    Etwaige Phasenunterschiede zwischen Referenzspannung des Lock-in-Verstärkers und eingehendem Signal müssen am Verstärker weggeregelt werden.

    Die Apparatur muss mindestens eine Stunde warmlaufen.
    Das erste Messobjekt ist eine mit \ce{Cu^{2+}}-Ionen versetzte Wasserprobe.
    Die generelle Vorgehensweise beginnt immer mit einem Einregeln der TT-Brücke, sodass die ausgehende Spannung minimal wird.
    Daraufhin wird die Brücke bezüglich der Amplitude um \SIrange{150}{500}{\milli\volt} verstimmt.
    Dadurch kann eventuell eine Phase am Lock-in-Verstärker auftreten, die wieder weggeregelt werden muss.
    Die Zeitkonstante am Verstärker wird auf \SI{0.3}{\second} eingestellt.
    Des Weiteren wird der Spannungsoffset am $x$-$y$-Schreiber so eingestellt, dass der Schreiber in der Mitte ist.

    Für die Vermessung der Wasserprobe wird nun mit dem Potentiometermotor das Spektrum durchlaufen und ein Peak gesucht.
    Ist dieser gefunden, so wird dieser unter Optimierung der Parameter Geschwindigkeit des Potentiometermotors, Zeitkonstante, Messbereich und Phase des Lock-In-Verstärkers, Empfindlichkeit, sowie Vorschubgeschwindigkeit des Schreibers maximiert.
    Schließlich ist zu beachten, dass nur in eine Richtung gemessen wird, um einen etwaigen Totgang des Motors, der zu einem Offset führt, zu kompensieren.

    Als nächstes wird eine Gipsprobe, die in einem Goniometer eingespannt ist, für mindestens sieben Winkel vermessen.
    Der Winkel \SI{0}{\degree} entspricht der $\g[100]$-Richtung im Kristall, die parallel zur Bruchstelle des Kristalls liegt.
    Wegen der geringeren Protonendichte kommt es zu kleineren Signalspannungen, die Peaks haben aber keinen großen Abstand voneinander.
    Als Frequenz wird bei Gips ein Wert von \SI{20.01}{\mega\hertz} eingestellt.
  \section{Messwerte und Auswertung}
    \subsection{Digitalisierung der Daten}
      Um die Ergebnisse am Computer auswerten zu können, wird das Papier aus dem $x$\-/$y$\-/Schreiber eingescannt.
      Als Beispiel ist in Abbildung \ref{fig:gypsum-orig} der Scan von Gips bei einer Goniometereinstellung von \SI{0}{\degree} zu sehen.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/gypsum-0-original.png}
        \caption{Eingescanntes $x$-$y$-Schreiberpapier für Gips bei einem Winkel von \SI{0}{\degree}}
        \label{fig:gypsum-orig}
      \end{figure}
      Als Vorbereitung werden daraufhin mit der Bildbearbeitungssoftware GIMP \cite{gimp} in allen Aufnahmen die horizontalen Striche des Hilfrasters im Hintergrund gebleicht, da sie im weiteren Verlauf stören.
      Dies geschieht bei allen Scans durch die Funktion Destripe mit der Standard-Linienbreite von $36$.
      Exemplarisch ist in Abbildung \ref{fig:gypsum-orig-destriped} das Ergebnis mit gebleichten horizontalen Rasterlinien zu sehen.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/gypsum-0-original-destriped.png}
        \caption{Eingescanntes Bild mit bearbeiteten horizontalen Rasterlinien. Als Beispiel ist hier das Messergebnis für Gips bei einer Goniometereinstellung von \SI{0}{\degree} zu sehen.}
        \label{fig:gypsum-orig-destriped}
      \end{figure}
      Das gescannte Bild wird in ein $N{\times}M{\times}3$-Array umgewandelt, sodass in jedem Pixel der RGB-Anteil bekannt ist.
      Durch das Scannen mit 75 dpi entspricht ein Pixel in etwa dem Ausmaß der Kurvenlinie.
      Schließlich wird in jeder Pixelspalte der minimale R-, G- oder B-Anteil bestimmt, jenachdem, bei welcher Methode die Kurve am besten zu erkennen ist.
      Der Nullpunkt der $y$-Achse ergibt sich, indem von der ermittelten Kurve der Mittelwert abgezogen wird.
      Das Resultat dieses Algorithmus’ wird mit der Simpson-Methode integriert.
    \subsection{Kalibrierung des Magnetfeldes und Dispersionskurven für Wasser}
      Die Dispersionskurven für die vermessenen Frequenzen (siehe Tabelle \ref{tab:water-fit}) werden einer nichtlinearen Ausgleichrechnung mit der Funktion
      \begin{eqn}
        \f{χ'}(p) = χ_0 \frac{p_0 \g(p_0 - p)}{a + \g(p_0 - p)^2}
      \end{eqn}
      unterzogen, um die Resonanzstelle $p_0$ zu bestimmen.
      Die Ergebnisse sind in Tabelle \ref{tab:water-fit} zu finden.
      \begin{table}
        \caption{Ergebnisse der nichtlinearen Ausgleichrechnung für Wasser, gemessen bei den Frequenzen $ω_0$.}
        \label{tab:water-fit}
        \input{table/water-fit.tex}
      \end{table}

      Die integrierten Messwerte, also die Dispersionskurven und die Kurven aus der Ausgleichrechnung befinden sich in den Abbildungen \ref{fig:water-disp1} bis \ref{fig:water-disp6}.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/water-19.95.pdf}
        \caption{Dispersionskurve für Wasser und Ergebniskurve der nichtlinearen Ausgleichsrechnung gemessen bei einer Frequenz von \SI{19.95}{\mega\hertz}.}
        \label{fig:water-disp1}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/water-19.98.pdf}
        \caption{Dispersionskurve für Wasser und Ergebniskurve der nichtlinearen Ausgleichsrechnung gemessen bei einer Frequenz von \SI{19.98}{\mega\hertz}.}
        \label{fig:water-disp2}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/water-20.01.pdf}
        \caption{Dispersionskurve für Wasser und Ergebniskurve der nichtlinearen Ausgleichsrechnung gemessen bei einer Frequenz von \SI{20.01}{\mega\hertz}.}
        \label{fig:water-disp3}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/water-20.03.pdf}
        \caption{Dispersionskurve für Wasser und Ergebniskurve der nichtlinearen Ausgleichsrechnung gemessen bei einer Frequenz von \SI{20.03}{\mega\hertz}.}
        \label{fig:water-disp4}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/water-20.05.pdf}
        \caption{Dispersionskurve für Wasser und Ergebniskurve der nichtlinearen Ausgleichsrechnung gemessen bei einer Frequenz von \SI{20.05}{\mega\hertz}.}
        \label{fig:water-disp5}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/water-20.08.pdf}
        \caption{Dispersionskurve für Wasser und Ergebniskurve der nichtlinearen Ausgleichsrechnung gemessen bei einer Frequenz von \SI{20.08}{\mega\hertz}.}
        \label{fig:water-disp6}
      \end{figure}
      Da die Absorption immer bei einer charakteristischen Frequenz von ungefähr \SI{20}{\mega\hertz} auftritt, kann durch eine lineare Regression eine Beziehung zwischen den erfassten Potentiometerdaten und dem tatsächlichen Magnetfeld hergestellt werden.
      Die Ergebnisse der linearen Ausgleichrechnung befinden sich in Tabelle \ref{tab:B0}.
      Dabei gilt
      \begin{eqn}
        B = b p + c \..
      \end{eqn}
      Dort befinden sich ebenfalls die Nullstellen, also die Anregungsfrequenzen.
      \begin{table}
        \caption{Ergebnisse der linearen Ausgleichsrechnung für die Kalibrierung der Koordinatenachse für das Magnetfeld. Naturkonstanten aus \cite{scipy}.}
        \label{tab:B0}
        \input{table/B0.tex}
      \end{table}
      Die Messwerte und die Ausgleichsgerade stehen in \mbox{Abbildung \ref{fig:B0}}.
      \begin{figure}
        \includegraphics{graphic/B0.pdf}
        \caption{Messwerte und lineare Ausgleichsgerade für die Kalibrierung der Koordinatenachse für das Magnetfeld.}
        \label{fig:B0}
      \end{figure}
      \FloatBarrier
    \subsection{Analyse des Gipskristalls}
      Die aufgenommenen Dispersionskurven für die Gipsprobe sind für acht verschiedene Winkel (siehe Tabelle \ref{tab:gypsum-splittings}) in den Abbildungen \ref{fig:gypsum-0} bis \ref{fig:gypsum-90} zu finden.
      Die aufgenommenen Daten werden, wie schon beschrieben, digitalisiert und integriert.
      Anschließend wird die $x$-Achse mit den Ergebnissen aus Tabelle \ref{tab:B0} umskaliert.
      Auch bei Gips befinden sich die, gegebenenfalls verschobenen, Resonanzen wieder bei den Nulldurchgängen.
      Die Resonanzstellen $B₀$ werden mittels der Funktion
      \begin{eqn}
        \f{χ'}(B) = d + \sum_i χ_{0, i} \frac{B_{0, i} \g(B_{0, i} - B)}{a_i + \g(B_{0, i} - B)^2}
      \end{eqn}
      bestimmt.
      Die Differenzen der Resonanzen ergeben die Aufspaltungen und stehen in Tabelle \ref{tab:gypsum-splittings}.
      \begin{figure}
        \includegraphics{graphic/gypsum-0.pdf}
        \caption{Gipsprobe bei $φ = \SI{0}{\degree}$.}
        \label{fig:gypsum-0}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/gypsum-19.pdf}
        \caption{Gipsprobe bei $φ = \SI{13}{\degree}$.}
        \label{fig:gypsum-13}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/gypsum-26.pdf}
        \caption{Gipsprobe bei $φ = \SI{26}{\degree}$.}
        \label{fig:gypsum-26}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/gypsum-39.pdf}
        \caption{Gipsprobe bei $φ = \SI{39}{\degree}$.}
        \label{fig:gypsum-39}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/gypsum-52.pdf}
        \caption{Gipsprobe bei $φ = \SI{52}{\degree}$.}
        \label{fig:gypsum-52}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/gypsum-65.pdf}
        \caption{Gipsprobe bei $φ = \SI{65}{\degree}$.}
        \label{fig:gypsum-65}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/gypsum-78.pdf}
        \caption{Gipsprobe bei $φ = \SI{78}{\degree}$.}
        \label{fig:gypsum-78}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/gypsum-90.pdf}
        \caption{Gipsprobe bei $φ = \SI{90}{\degree}$.}
        \label{fig:gypsum-90}
      \end{figure}
      \FloatBarrier
      \begin{table}
        \caption{Ergebnisse der Ausgleichrechnungen, der Index $i$ der Nullstelle pro Winkel und die ermittelten Nullstellen.}
        \label{tab:gypsum-splittings}
        \OverfullCenter{\input{table/gypsum-roots.tex}}
      \end{table}

      Aus diesen Aufspaltungen lassen sich die Werte für $r$ für die verschiedenen Winkel berechnen; diese sind in Tabelle \ref{tab:material-param} zu finden.
      Für den Fehler gilt
      \begin{eqn}
        \err{r} = \sqrt[3]{\frac{3 ℏ μ₀ γ}{3³ \cdot 8 \PI} \frac{1}{\g(\Del{B})⁴} \frac{\cos^2(φ ± φ_0)}{\cos^2.α.}} \err{\Del{B}} \..
      \end{eqn}
      Bei den Winkeln, bei denen es nur 3 Nullstellen gab wurden die erste und die dritte ausgewählt und als die am weitesten verschobenen, also äußeren, Nullstellen interpretiert.
      Winkel mit einer Nullstelle gingen nicht in die Rechnung ein.
      \begin{table}
        \caption{Ergebnisse für $r$, Literaturwert aus \cite{literature}.}
        \label{tab:material-param}
        \input{table/gypsum-results.tex}
      \end{table}
  \section{Diskussion}
    Das Digitalisieren der Daten funktionierte nach dem Entfernen der Streifen verhältnismäßig gut.
    Trotzdem ist nicht auszuschließen, dass in der digitalen Kurve vereinzelt auch Punkte enthalten sind, die nicht der realen Kurve entsprechen.
    Dies kann man aber vernachlässigen, da das Signal sowieso verrauscht ist.

    Das Bestimmen der Resonanzstellen durch die Ausgleichsrechnung bei Wasser funktionierte gut und mit kleinen Fehlern.
    Auch die lineare Ausgleichsrechnung für die Umskalierung der $x$-Achse funktionierte mit ziemlich kleinen Fehlern.

    Die Bestimmung der Resonanzstellen bei Gips funktionierte auch gut.
    Das Ergebnis für $r$ weicht aber um einen Faktor \num{2} vom Literaturwert ab.
  \makebibliography
\end{document}
