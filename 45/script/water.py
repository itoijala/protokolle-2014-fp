import pickle

import numpy as np

import scipy.integrate
import scipy.ndimage

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

ℏ, μ0 = lt.constant('Planck constant over 2 pi', 'mag. constant')
γ = 2 / ℏ * lt.constant('proton mag. mom. to nuclear magneton ratio') * lt.constant('nuclear magneton')

ω0 = np.loadtxt('data/ω₀', skiprows=2) * 1e6
B0 = ω0 / γ

def χ(p, χ0, p0, a):
    return χ0 * p0 * (p0 - p) / (a + (p0 - p)**2)

p0 = np.empty(len(ω0), np.object)
χ0 = np.empty(len(ω0), np.object)
a = np.empty(len(ω0), np.object)

for i, (pmin, pmax, start) in enumerate([
        (1,   3.3, (-2, 1.9, 1e-4)),
        (2,   3.9, (-1, 2.8, 1e-2)),
        (2.5, 4.9, (-1, 3.7, 1e-3)),
        (3,   4.7, (-1, 3.9, 1e-3)),
        (3,   5,   (-1, 3.7, 1e-3)),
        (4,   5.5, (-1, 4.8, 1e-3)),
        ]):
    image = scipy.ndimage.imread('data/raw/water-{:.2f}.png'.format(ω0[i] * 1e-6))
    y = np.argmin(image[:, :, 1], axis=0)
    y -= np.mean(y)

    p = np.linspace(pmin, pmax, len(y))

    I = np.zeros(len(y))
    for x in range(1, len(y)):
        I[x] = scipy.integrate.simps(y[:x], p[:x])

    χ0[i], p0[i], a[i] = lt.curve_fit(χ, p, I, p0=start)

    fig, ax = plt.subplots(1, 1)
    ax.plot(p, I, 'r-', label='Integrierte Messwerte')
    ax.plot(p, lt.noms(χ(p, χ0[i], p0[i], a[i])), 'b--', label='Ausgleichskurve')
    ax.set_xlabel(r'$p$')
    ax.set_ylabel(r"$χ'$")
    ax.legend(loc='best')
    fig.savefig('build/graphic/water-{:.2f}.pdf'.format(ω0[i] * 1e-6))

b, c = lt.linregress(p0, B0)
pickle.dump((b, c), open('build/result/B0.pickle', 'wb'))

fig, ax = plt.subplots(1, 1)
x = np.linspace(1.5, 5)
ax.plot(x, lt.noms(b * x + c) * 1e3, 'b-')
ax.plot(lt.noms(p0), lt.noms(B0) * 1e3, 'rx')
ax.set_xlabel(r'$p$')
ax.set_ylabel(r'\silabel{B}{\milli\tesla}')
fig.savefig('build/graphic/B0.pdf')

tab = lt.SymbolColumnTable()
tab.add_si_column('ω₀', ω0 * 1e-6, r'\mega\hertz', places=2)
tab.add_si_column('χ₀', χ0)
tab.add_si_column('p₀', p0)
tab.add_si_column('a', a)
tab.savetable('build/table/water-fit.tex')

tab = lt.SymbolRowTable()
tab.add_si_row('γ', γ * 1e-8, r'\per\tesla\per\second', exp='e8')
tab.add_si_row('μ₀', lt.noms(μ0) * 1e6, r'\volt\second\per\ampere\meter', figures=9, exp='e-6')
tab.add_si_row(r'ℏ', ℏ * 1e34, r'\square\meter\kilogram\per\second', exp='e-34')
tab.add_hrule()
tab.add_si_row('b', b * 1e6, r'\micro\tesla')
tab.add_si_row('c', c * 1e3, r'\milli\tesla')
tab.savetable('build/table/B0.tex')
