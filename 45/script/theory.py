import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')


fig, ax = plt.subplots(1, 1)
θ = np.linspace(0, np.pi / 2, 1000)
ax.plot(θ, 1 - 3 * np.cos(θ)**2, 'b-')
ax.set_xlabel(r'$θ$')
ax.set_ylabel(r'$\Del{B} \propto 1 - 3\cos(θ)^2$')
fig.savefig('build/graphic/theory.pdf')
