import itertools
import pickle

import numpy as np

import scipy.integrate
import scipy.ndimage

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

ℏ, μ0 = lt.constant('Planck constant over 2 pi', 'mag. constant')
γ = 2 / ℏ * lt.constant('proton mag. mom. to nuclear magneton ratio') * lt.constant('nuclear magneton')

r_lit = np.loadtxt('data/r_lit', skiprows=3) * 1e-10

α = np.deg2rad(53.5)
φ0 = np.deg2rad(54.5)

b, c = pickle.load(open('build/result/B0.pickle', 'rb'))

φ = np.loadtxt('data/φ', skiprows=3)

def χ(B, χ0, B0, a):
    return χ0 * B0 * (B0 - B) / (a + (B0 - B)**2)

def χsum(num):
    def f(B, d, *args):
        s = d
        for i in range(num):
            s += χ(B, *args[3 * i:3 * i + 3])
        return s
    return f

d = np.empty(len(φ), np.object)
B0 = np.empty(len(φ), np.object)
χ0 = np.empty(len(φ), np.object)
a = np.empty(len(φ), np.object)

φs = np.empty(len(φ), np.object)
i_φ = np.empty(len(φ), np.object)

for i, (xmin, xmax, start) in enumerate([
        (2,   4.5, (0, -2,    0.07475, 1e-3)),
        (2,   5,   (0, -2,    0.07485, 1e-3)),
        (1.5, 4.5, (0, -2e-4, 0.0746,  1e-10, -2e-4, 0.07475, 1e-10)),
        (1.5, 4.8, (0, -2e-4, 0.0747,  1e-10, -2e-4, 0.0749,  1e-10)),
        (1.6, 4.9, (0, -2e-4, 0.07465, 1e-10, -2e-4, 0.07475, 1e-10, -2e-4, 0.07485, 1e-10, -2e-4, 0.07495, 1e-10)),
        (1.5, 5.1, (0,  2e-4, 0.07465, 1e-10,  2e-4, 0.07475, 1e-10,  2e-4, 0.0749,  1e-10)),
        (1.5, 5,   (0, -2e-4, 0.0747,  1e-10, -2e-4, 0.0748,  1e-10, -2e-4, 0.07495, 1e-10)),
        (2,   5,   (0,  2e-4, 0.0747,  1e-10,  2e-4, 0.07485, 1e-10)),
        ]):
    image = scipy.ndimage.imread('data/raw/gypsum-{:.0f}.png'.format(φ[i]))
    y = np.argmin(image[:, :, 1], axis=0)
    y -= np.mean(y)

    p = np.linspace(xmin, xmax, len(y))
    B = lt.noms(b * p + c)

    I = np.zeros(len(y))
    for x in range(1, len(y)):
        I[x] = scipy.integrate.simps(y[:x], p[:x])

    num = len(start) // 3
    params = lt.curve_fit(χsum(num), B, I, p0=start)
    d[i] = np.repeat(params[0], num)
    B0[i] = params[2::3]
    χ0[i] = params[1::3]
    a[i] = params[3::3]

    φs[i] = np.repeat(φ[i], num)
    i_φ[i] = np.arange(num)

    fig, ax = plt.subplots(1, 1)
    ax.plot(B, I, 'r-', label='Integrierte Messwerte')
    ax.plot(B, lt.noms(χsum(num)(B, *params)), 'b--', label='Ausgleichskurve')
    ax.set_xlabel(r'\silabel{B}{\tesla}')
    ax.set_ylabel("$χ'$")
    ax.legend(loc='best')
    fig.savefig('build/graphic/gypsum-{:.0f}.pdf'.format(φ[i]))

ΔB = np.empty(7, np.object)
r = np.empty(7, np.object)
φ_r = np.empty(7)
i_r = np.empty(7, np.object)

for i, (j, k, l, pm) in enumerate([
        (2, 1, 0, -1),
        (3, 1, 0, -1),
        (4, 3, 0, -1),
        (4, 2, 1, +1),
        (5, 2, 1, +1),
        (6, 2, 0, -1),
        (7, 1, 0, -1),
        ]):
    ΔB[i] = B0[j][k] - B0[j][l]
    r[i] = (3 * ℏ * μ0 * γ / (8 * np.pi) / ΔB[i] * np.cos(np.deg2rad(φ[j]) + pm * φ0)**2 / np.cos(α)**2)**(1 / 3)
    φ_r[i] = φ[j]
    i_r[i] = '${} - {}$'.format(k, l)

d = np.concatenate(d)
B0 = np.concatenate(B0)
χ0 = np.concatenate(χ0)
a = np.concatenate(a)
φs = np.concatenate(φs)
i_φ = np.concatenate(i_φ)

tab = lt.SymbolColumnTable()
tab.add_si_column('φ', φs, r'\degree', places=0)
tab.add_si_column('d', d)
tab.add_si_column('i', i_φ)
tab.add_si_column('B_{0, i}', B0 * 1e3, r'\milli\tesla')
tab.add_si_column('χ_{0, i}', χ0 * 1e6, exp='e-6')
tab.add_si_column('a', a * 1e9, r'\tesla\squared', exp='e-9')
tab.savetable('build/table/gypsum-roots.tex')

tab1 = lt.SymbolColumnTable()
tab1.add_si_column('φ', φ_r, r'\degree', places=0)
tab1.add(lt.Column([list(i_r)], 'c', '$i - j$'))
tab1.add_si_column(r'\Del{B}', ΔB * 1e3, r'\milli\tesla')
tab1.add_si_column('r', r * 1e10 , r'\angstrom')

tab2 = lt.SymbolRowTable()
tab2.add_si_row(r'\mean{r}', lt.cumean(r) * 1e10, r'\angstrom')
tab2.add_si_row(r'r_{\t{lit}}', r_lit * 1e10, r'\angstrom', places=2)

tab3 = lt.Table()
tab3.add(lt.Row())
tab3.add_hrule()
tab3.add(lt.Row())
tab3.add(lt.Column([tab1, tab2], "@{}c@{}"))
tab3.savetable('build/table/gypsum-results.tex')
