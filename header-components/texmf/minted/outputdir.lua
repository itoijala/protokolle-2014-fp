outputdir = '.'
for k, v in pairs(arg) do
	if string.find(v, '%-output%-directory=') then
		outputdir = string.explode(v, '=')[2]
	end
end
tex.print(outputdir .. '/')
