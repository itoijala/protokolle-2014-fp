import os
import os.path
import math
from decimal import Decimal

import numpy
import numpy.linalg
import scipy
import scipy.stats
import scipy.constants
import uncertainties
from uncertainties import ufloat, ufloat_fromstr
from uncertainties.unumpy import nominal_values, std_devs, uarray

def heaviside(x):
    if x > 0:
        return x
    else:
        return 0

# default is round to even!!!
#   1.25  -> 1.2
#   1.251 -> 1.3
#   1.15  -> 1.2
#   1.151 -> 1.2
def round_figures(value, figures):
    d = Decimal(value)
    if d == 0:
        return '0'
    d = round(d, figures - int(math.floor(math.log10(abs(d)))) - 1)
    return "{:f}".format(d)

def round_places(value, places):
    d = Decimal(value)
    if d == 0:
        return '0'
    d = round(d, places)
    return "{:f}".format(d)

def round_to(value, error, figures=1):
    de = Decimal(error)
    f = figures - int(math.floor(math.log10(abs(de)))) - 1
    return round_places(value, f)

# More convenient extraction of value and error from ufloats
def noms(*args):
    if len(args) == 1:
        args = args[0]
    return nominal_values(args)

vals = noms

def stds(*args):
    if len(args) == 1:
        args = args[0]
    return std_devs(args)

def linregress(x, y):
    sqrt = math.sqrt

    x = vals(x)
    y = vals(y)

    N = len(y)
    Delta = N * sum(x**2) - (sum(x))**2

    A = (N * sum(x * y) - sum(x) * sum(y)) / Delta
    B = (sum(x**2) * sum(y) - sum(x) * sum(x * y)) / Delta

    sigma_y = sqrt(sum((y - A * x - B)**2) / (N - 2))

    A_error = sigma_y * math.sqrt(N / Delta)
    B_error = sigma_y * math.sqrt(sum(x**2) / Delta)

    return ufloat(A, A_error), ufloat(B, B_error)

def curve_fit(f, x, y, **kwargs):
    popt, pcov = scipy.optimize.curve_fit(f, vals(x), vals(y), **kwargs)
    if len(numpy.shape(pcov)) == 0 or numpy.any(numpy.isinf(pcov)):
        print("popt =", popt)
        print("pcov =", pcov)
        raise Exception("Fit unsuccessful!")
    return uncertainties.correlated_values(popt, pcov)

def ucurve_fit(f, x, y, **kwargs):
    return curve_fit(f, vals(x), vals(y), sigma=stds(y), **kwargs)

def mean(values, axis=0):
    return uncertainties.unumpy.uarray(numpy.mean(vals(values), axis=axis), scipy.stats.sem(vals(values), axis=axis))

# doesn't seem to work, see http://en.wikipedia.org/wiki/Weighted_mean#Dealing_with_variance
def umean(values, axis=0):
    if numpy.shape(values)[axis] == 1:
        return values
    w = numpy.sum(1 / stds(values)**2, axis=axis)
    m = numpy.sum(vals(values) / stds(values)**2, axis=axis) / w
    sigma = numpy.sqrt(1 / w / (len(values) - 1) * numpy.sum((vals(values) - m)**2 / stds(values)**2))
    return uncertainties.unumpy.uarray(m, sigma)

def cumean(values):
    C = numpy.array(uncertainties.covariance_matrix(values))
    Cinv = numpy.linalg.inv(C)
    W = numpy.repeat(1, len(C))
    sigma2 = 1 / (numpy.dot(W.T, numpy.dot(Cinv, W)))
    mu = sigma2 * numpy.dot(W.T, numpy.dot(Cinv, noms(values)))
    return uncertainties.ufloat(mu, numpy.sqrt(sigma2))

def constant(*name):
    if len(name) == 1:
        c = scipy.constants.physical_constants[name[0]]
        return ufloat(c[0], c[2])
    else:
        return [constant(n) for n in name]
