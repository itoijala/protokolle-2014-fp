from labtools.table.cell import Cell, EmptyCell
from labtools.table.common import block_join

class Row:
    def __init__(self, left=None, right=None, rows=None, rule=""):
        if left is None:
            left = EmptyCell(0, rows=(rows if rows != None else 0))
        if not isinstance(left, Cell):
            left = Cell(left, rows)
        if right is None:
            right = EmptyCell(0, rows=(rows if rows != None else 0))
        if not isinstance(right, Cell):
            right = Cell(right, rows)
        self._left = left
        self._right = right
        self._rule = rule
        self._rules = []

    def set_left_columns(self, columns):
        self._left.set_columns(columns)

    def set_right_columns(self, columns):
        self._right.set_columns(columns)

    def cell_rows(self):
        return 1

    def rows(self):
        return max(self._left.rows(), self._right.rows())

    def add_rule_after(self, row):
        self._rules.append(row)

    def render(self, cells, leftColumns=None, rightColumns=None):
        cells = cells[0]
        if leftColumns != None:
            self.set_left_columns(leftColumns)
        if rightColumns != None:
            self.set_right_columns(rightColumns)
        rows = max(max([ c.rows() for c in cells ]), self._left.rows(), self._right.rows())
        if rows > 0:
            blocks = [ c.render(rows) for c in cells if c.columns() > 0 ]
            if self._left.columns() > 0:
                blocks.insert(0, self._left.render(rows))
            if self._right.columns() > 0:
                blocks.append(self._right.render(rows))
            text = block_join(blocks, " & ")
            text = block_join(["\n".join(["  "] * rows), text]).replace("\n", r" \\" + "\n")
            for row in self._rules:
                texts = text.split("\n")
                text = "\n".join(texts[:row + 1] + [r"  \midrule"] + texts[row + 1:])
            text += r" \\" + "\n"
        else:
            text = ""
        if self._rule != "":
            text += "  " + self._rule + "\n"
        return text

class DummyRow(Row):
    def __init__(self, rule):
        Row.__init__(self, None, None, 0, rule)

    def cell_rows(self):
        return 0

    def render(self, cells, leftColumns=None, rightColumns=None):
        return "  " + self._rule + "\n"

class HorizontalRule(DummyRow):
    def __init__(self, rules=1):
        DummyRow.__init__(self, " ".join([r"\midrule"] * rules))
