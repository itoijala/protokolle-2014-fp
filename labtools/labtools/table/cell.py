from labtools.common import heaviside
from labtools.table.common import empty_cell

class Cell:
    def __init__(self, data, rows=None, columns=1):
        if not isinstance(data, list):
            data = [data]
        data = [ str(d) for d in data ]
        if rows is None:
            rows = len(data)
        self._data = data
        self._rows = rows
        self._columns = columns

    def data(self):
        return self._data

    def rows(self):
        return self._rows

    def columns(self):
        return self._columns

    def set_columns(self, columns):
        self._columns = columns

    def render(self, rows):
        return "\n".join([ d.replace("\n", " ") for d in self._data ] + ([empty_cell(self._columns)] * heaviside(rows - len(self._data))))

class EmptyCell(Cell):
    def __init__(self, columns=0, rows=0):
        Cell.__init__(self, [], rows, columns)
