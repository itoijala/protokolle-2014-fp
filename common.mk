MAKEFLAGS += --no-builtin-rules --no-builtin-variables

.SUFFIXES:

.SECONDEXPANSION:

TEX_INPUTS := $(shell pwd)/../:$(shell pwd)/../header-components/:$(shell pwd)/../texmf/
BIB_INPUTS := $(shell pwd)/../

TEXENV := TEXINPUTS="$(shell pwd)/build/:$(shell pwd)/:$(TEX_INPUTS):" max_print_line=1048576

PYTHONENV := $(TEXENV) PYTHONPATH="$(shell pwd)/build/script/:$(shell pwd)/script/:$(shell pwd)/../script/:$(shell pwd)/../labtools/" MATPLOTLIBRC="$(shell pwd)/../"
PYTHONEXEC := $(PYTHONENV) python3

MATPLOTLIB := ../matplotlibrc ../header/matplotlib.tex ../header/matplotlib-packages.tex

DATA = $$(addprefix data/, $(1))
GRAPHIC = $$(addprefix build/graphic/, $$(addsuffix .pdf, $(1)))
RESULT = $$(addprefix build/result/, $(1))
TABLE = $$(addprefix build/table/, $$(addsuffix .tex, $(1)))
PYTHON = @echo '$$(PYTHON)' $(1); $(PYTHONEXEC) $(1)
define SCRIPT_TEXT =
    $(GRAPHIC_$(strip $(1))) \
    $(RESULT_$(strip $(1))) \
    $(TABLE_$(strip $(1))) \
    : build/sync/$(strip $(1)).py.sync
	@:

    build/sync/$(strip $(1)).py.sync \
    : script/$(strip $(1)).py \
    $(DATA_$(strip $(1))) \
    $(strip $(2)) \
    $$(MATPLOTLIB) | build/graphic build/result build/table build/sync
	@touch build/sync/$(strip $(1)).py.sync
	$$(call PYTHON, $$<)
endef
SCRIPT = $(eval $(call SCRIPT_TEXT, $(1),))
SCRIPTD = $(eval $(call SCRIPT_TEXT, $(1), $(2)))
DEPS = $(foreach script, $(1), $(GRAPHIC_$(script)) $(RESULT_$(script)) $(TABLE_$(script)))

build/main.pdf: main.tex ../header/report.tex ../header/report-packages.tex ../common.bib | build
	@../tex-utils/tex.sh --tex-inputs "$(TEX_INPUTS)" --biber --bib-inputs "$(BIB_INPUTS)" main.tex

build/graphic/%.pdf: graphic/%.crop | build/graphic
	@../tex-utils/crop.py "$<" "$@"

build build/graphic build/result build/table build/sync:
	mkdir -p $@

fast:
	@../tex-utils/tex.sh --tex-inputs "$(TEX_INPUTS)" --biber --bib-inputs "$(BIB_INPUTS)" --fast main.tex

clean:
	rm -rf build

.PHONY: clean fast
