ANALOG
Ch 1 Scale 20mV/, Pos -750uV, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.000 : 1, Skew 0.0s
Ch 2 Scale 5.00V/, Pos 0.0V, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.000 : 1, Skew 0.0s

TRIGGER
Sweep Mode Normal, Coup DC, Noise Rej Off, HF Rej On, Holdoff 40.0ns
Mode Edge, Source Ch 1, Slope Rising, Level 0.0V

HORIZONTAL
Mode Normal, Ref Center, Main Scale 2.000ms/, Main Delay 8.200000000ms

ACQUISITION
Mode Normal, Realtime On, Vectors On, Persistence Off

MEASUREMENTS
Pk-Pk(1), Cur 151mV, Mean 3.757V, Min 18mV, Max 19.100V, Std Dev 4.671V, Count 21.13k
Pk-Pk(2), Cur 16.5V, Mean 13.7V, Min 400mV, Max 30.2V, Std Dev 12.0V, Count 23.56k
Frequency(1), Cur 207.13Hz, Mean 131.36Hz, Min 30.62Hz, Max 78.000kHz, Std Dev 545.82Hz, Count 23.56k

