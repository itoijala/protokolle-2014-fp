ANALOG
Ch 1 Scale 5.00V/, Pos 0.0V, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.000 : 1, Skew 0.0s
Ch 2 Scale 5.00V/, Pos -4.31250V, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.000 : 1, Skew 0.0s

TRIGGER
Sweep Mode Normal, Coup DC, Noise Rej Off, HF Rej On, Holdoff 40.0ns
Mode Edge, Source Ch 1, Slope Rising, Level 1.31250V

HORIZONTAL
Mode Normal, Ref Center, Main Scale 2.000ms/, Main Delay 0.0s

ACQUISITION
Mode Normal, Realtime On, Vectors On, Persistence Off

MEASUREMENTS
Pk-Pk(1), Cur 16.5V, Mean 17.4V, Min 800mV, Max 19.7V, Std Dev 1.9V, Count 33.03k
Pk-Pk(2), Cur 9.6V, Mean 12.0V, Min 100mV, Max 36.4V, Std Dev 9.8V, Count 29.48k
Frequency(1), Cur 400.3Hz, Mean 195.24kHz, Min 41.4Hz, Max 516.50kHz, Std Dev 247.09kHz, Count 32.35k

