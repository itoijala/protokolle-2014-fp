import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

U_1, U_B, R_1, R_P = np.loadtxt('data/f-constant', unpack=True, skiprows=2)
t, U_2, U_3 = np.loadtxt('data/f-measure', unpack=True)
t *= 1e3

foo = R_1 / R_P * U_B

fig, ax = plt.subplots(1, 1)
ax.plot(t, U_2, label='Frequenzgenerator')
ax.plot(t, U_3, label='Schmitt-Trigger')
ax.set_xlim(0, t[-1])
ax.set_xlabel(r'$\silabel{t}{\milli\second}$')
ax.set_ylabel(r'$\silabel{U}{\volt}$')
ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)
fig.savefig('build/graphic/f.pdf')

tab = lt.SymbolRowTable()
tab.add_si_row(r'U_{\t{B}}', U_B, r'\volt', places=0)
tab.add_si_row(r'R_1', R_1, r'\ohm', places=0)
tab.add_si_row(r'R_{\t{P}}', R_P * 1e-3, r'\kilo\ohm', places=0)
tab.add_hrule()
tab.add_si_row(r'f_{\t{oo}}', foo, r'\volt', places=1)
tab.add_si_row('U_1', U_1, r'\volt', places=1)
tab.savetable('build/table/f.tex')
