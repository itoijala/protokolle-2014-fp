import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

U_Betrieb, ν, U_B, U_A, R_1, R_p, R, C = np.loadtxt('data/g-constant', unpack=True, skiprows=2)
U_Betrieb /= 2
U_B /= 2
U_A *= 1e-3
R_p *= 1e3
R *= 1e3
C *= 1e-6

ν_th = R_p / R_1 / 4 / R / C
U_A_th = 2 * R_1 / R_p * U_Betrieb

tab = lt.SymbolRowTable()
tab.add_si_row(r'U_{\t{Betrieb}}', U_Betrieb, r'\volt', places=1)
tab.add_si_row(r'R_1', R_1, r'\ohm', places=0)
tab.add_si_row(r'R_{\t{p}}', R_p * 1e-3, r'\kilo\ohm', places=0)
tab.add_si_row('R', R * 1e-3, r'\kilo\ohm', places=0)
tab.add_si_row('C', C * 1e6, r'\micro\farad', places=1)
tab.add_hrule()
tab.add_si_row(r'ν_{\t{ex}}', ν, r'\hertz', places=1)
tab.add_si_row(r'ν_{\t{th}}', ν_th, r'\hertz', places=0)
tab.add_si_row(r'U_{\t{B}}', U_B, r'\volt', places=1)
tab.add_si_row(r'U_{\t{A},\t{ex}}', U_A * 1e3, r'\milli\volt', places=1)
tab.add_si_row(r'U_{\t{A},\t{th}}', U_A_th * 1e3, r'\milli\volt', places=1)
tab.savetable('build/table/g.tex')

t, U_2, U_3 = np.loadtxt('data/g-measure', unpack=True)
t *= 1e3

fig, ax = plt.subplots(1, 1)
ax2 = ax.twinx()
lines1 = ax.plot(t, U_2, label=r'$U_{\t{B}}$', c='b')
lines2 = ax2.plot(t, U_3 * 1e3, label=r'$U_{\t{A}}$', c='g')
lines = lines1 + lines2
ax.set_xlim(0, t[-1])
ax.set_xlabel(r'$\silabel{t}{\milli\second}$')
ax.set_ylabel(r'$\silabel{U_{\t{B}}}{\volt}$')
ax2.set_ylabel(r'$\silabel{U_{\t{A}}}{\milli\volt}$')
labels = [l.get_label() for l in lines]
ax.legend(lines, labels, bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)
fig.savefig('build/graphic/g.pdf')
