import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

e, k_B = lt.constant('elementary charge', 'Boltzmann constant')

U_1, U_A = np.loadtxt('data/i-log', unpack=True)

a, b = lt.linregress(np.log(U_1), U_A)

T = e / k_B * a

fig, ax = plt.subplots(1, 1)
ax.plot(U_1, U_A, 'rx', zorder=3)
x = np.linspace(0.07, 20, 1000)
ax.plot(x, lt.noms(a * np.log(x) + b), 'b-')
ax.set_xscale('log')
ax.set_xlim(0.07, 20)
ax.set_ylim(0.4, 0.7)
ax.set_xlabel(r'$\silabel{U_1}{\volt}$')
ax.set_ylabel(r'$\silabel{U_{\t{A}}}{\volt}$')
fig.savefig('build/graphic/i-log.pdf')

tab = lt.SymbolColumnTable()
tab.add_si_column('U_1', U_1, r'\volt', places=1)
tab.add_si_column(r'U_{\t{A}}', U_A, r'\volt', places=2)
tab.savetable('build/table/i-log-data.tex')

tab = lt.SymbolRowTable()
tab.add_si_row('a', a * 1e3, r'\milli\volt')
tab.add_si_row('b', unp.exp(-b / a) * 1e6, r'\micro\volt')
tab.add_hrule()
tab.add_si_row('T', T, r'\kelvin')
tab.savetable('build/table/i-log-fit.tex')

U_1, U_A = np.loadtxt('data/i-exp', unpack=True)

c, d = lt.linregress(U_1[2:-1], np.log(U_A[2:-1]))

T = e / k_B / c

fig, ax = plt.subplots(1, 1)
ax.plot(U_1, U_A, 'rx', zorder=3)
x = np.linspace(0.3, 0.725, 1000)
ax.plot(x, np.exp(lt.noms(c * x + d)), 'b-')
ax.set_yscale('log')
ax.set_xlim(0.3, 0.725)
ax.set_xlabel(r'$\silabel{U_1}{\volt}$')
ax.set_ylabel(r'$\silabel{U_{\t{A}}}{\volt}$')
fig.savefig('build/graphic/i-exp.pdf')

tab = lt.SymbolColumnTable()
tab.add_si_column('U_1', U_1, r'\volt', places=2)
tab.add_si_column(r'U_{\t{A}}', U_A, r'\volt', places=2)
tab.savetable('build/table/i-exp-data.tex')

tab = lt.SymbolRowTable()
tab.add_si_row('c', c, r'\per\volt')
tab.add_si_row('d', unp.exp(d) * 1e6, r'\micro\volt')
tab.add_hrule()
tab.add_si_row('T', T, r'\kelvin')
tab.savetable('build/table/i-exp-fit.tex')
