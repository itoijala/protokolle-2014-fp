import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

ν, U_e = np.loadtxt('data/c-U_e', unpack=True)

R_N, R_V, U_g, U_A = np.loadtxt('data/c-constant', unpack=True, skiprows=2)
R_N *= 1e3
R_V *= 1e3
U_e *= 1e-3

I_s = U_g / R_V
I = U_A / R_N
r_e = U_e / I_s
V = R_N / r_e

a, b = lt.linregress(np.log(ν), np.log(r_e))

fig, ax = plt.subplots(1, 1)
ax.plot(ν, r_e, 'rx', zorder=3)
x = np.linspace(0.9e2, 2.1e3)
ax.plot(x, np.exp(lt.noms(a * np.log(x) + b)), 'b-')
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlim(0.9e2, 2.1e3)
ax.set_xlabel(r'$\silabel{ν}{\hertz}$')
ax.set_ylabel(r'$\silabel{r_{\t{e}}}{\ohm}$')
fig.savefig('build/graphic/c-r_e.pdf')

fig, ax = plt.subplots(1, 1)
ax.plot(ν, V, 'rx', zorder=3)
ax.plot(x, R_N / np.exp(lt.noms(a * np.log(x) + b)), 'b-')
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlim(0.9e2, 2.1e3)
ax.set_xlabel(r'$\silabel{ν}{\hertz}$')
ax.set_ylabel('$V$')
fig.savefig('build/graphic/c-V.pdf')

tab = lt.SymbolRowTable()
tab.add_si_row(r'R_{\t{N}}', R_N * 1e-3, '\kilo\ohm', places=0)
tab.add_si_row(r'R_{\t{V}}', R_V * 1e-3, '\kilo\ohm', places=0)
tab.add_si_row(r'U_{\t{g}}', U_g, r'\volt', places=1)
tab.add_si_row(r'U_{\t{A}}', U_A, r'\volt', places=1)
tab.add_hrule()
tab.add_si_row(r'I_{\t{s}}', I_s, r'\ampere')
tab.add_si_row(r'I', I, r'\ampere')
tab.savetable('build/table/c-constants.tex')

tab = lt.SymbolColumnTable()
tab.add_si_column('ν', ν, r'\hertz', places=0)
tab.add_si_column(r'U_{\t{e}}', U_e * 1e3, r'\milli\volt', figures=3)
tab.savetable('build/table/c-measure.tex')

tab = lt.SymbolRowTable()
tab.add_si_row('a', a)
tab.add_si_row('b', b)
tab.savetable('build/table/c-fit.tex')
