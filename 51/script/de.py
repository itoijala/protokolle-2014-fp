import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

R, C, U_1 = np.loadtxt('data/de-constant', unpack=True, skiprows=2)

tab = lt.SymbolRowTable()
tab.add_si_row('R', R, r'\ohm', places=0)
tab.add_si_row('C', C, r'\micro\farad', places=1)
tab.add_si_row('U_1', U_1, r'\volt', places=1)
tab.savetable('build/table/de-constants.tex')

ν, U_A_i, U_A_d = np.loadtxt('data/de-U_A', unpack=True, skiprows=2)

a_i, b_i = lt.linregress(np.log(ν[1:5]), np.log(U_A_i[1:5]))
a_d, b_d = lt.linregress(np.log(ν[4:9]), np.log(U_A_d[4:9]))

fig, ax = plt.subplots(1, 1)
ax.plot(ν[1:5], U_A_i[1:5], 'rx', zorder=3)
ax.plot(ν[5:], U_A_i[5:], 'gx', zorder=3)
ax.plot(ν[:1], U_A_i[:1], 'gx', zorder=3)
x = np.linspace(9e1, 1e3)
ax.plot(x, np.exp(lt.noms(a_i * np.log(x) + b_i)), 'b-')
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlim(9e1, 1e3)
ax.set_xlabel(r'$\silabel{ν}{\hertz}$')
ax.set_ylabel(r'$\silabel{U_{\t{A}}}{\volt}$')
fig.savefig('build/graphic/d-U_A.pdf')

tab = lt.SymbolColumnTable()
tab.add_si_column('ν', ν, r'\hertz', places=0)
tab.add_si_column(r'U_{\t{A}}', U_A_i , r'\volt', places=1)
tab.savetable('build/table/d-measure.tex')

tab = lt.SymbolRowTable()
tab.add_si_row('a', a_i)
tab.add_si_row('b', b_i)
tab.savetable('build/table/d-fit.tex')

fig, ax = plt.subplots(1, 1)
ax.plot(ν[4:9], U_A_d[4:9], 'rx', zorder=3)
ax.plot(ν[:4], U_A_d[:4], 'gx', zorder=3)
ax.plot(ν[9:], U_A_d[9:], 'gx', zorder=3)
x = np.linspace(9e1, 1e3)
ax.plot(x, np.exp(lt.noms(a_d * np.log(x) + b_d)), 'b-')
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlim(9e1, 1e3)
ax.set_ylim(1e0, 2e1)
ax.set_xlabel(r'$\silabel{ν}{\hertz}$')
ax.set_ylabel(r'$\silabel{U_{\t{A}}}{\volt}')
fig.savefig('build/graphic/e-U_A.pdf')

tab = lt.SymbolRowTable()
tab.add_si_row('a', a_d)
tab.add_si_row('b', b_d)
tab.savetable('build/table/e-fit.tex')

tab = lt.SymbolColumnTable()
tab.add_si_column('ν', ν, r'\hertz', places=0)
tab.add_si_column(r'U_{\t{A}}', U_A_d , r'\volt', places=1)
tab.savetable('build/table/e-measure.tex')

for name in ['d-dreieck', 'd-rechteck', 'd-sin', 'e-dreieck', 'e-rechteck', 'e-sin']:
    t, U_1, U_A = np.loadtxt('data/{}'.format(name), unpack=True)

    fig, ax = plt.subplots(1, 1)
    ax2 = ax.twinx()
    lines1 = ax.plot(t, U_1, label='$U_1$', c='b')
    lines2 = ax2.plot(t, U_A, label=r'$U_{\t{A}}$', c='g')
    lines = lines1 + lines2
    ax.set_xlim(0, t[-1])
    ax.set_xlabel(r'$\silabel{t}{\milli\second}$')
    if name[0] == 'd':
        ax.set_ylabel(r'$\silabel{U_1}{\milli\volt}$')
    else:
        ax.set_ylabel(r'$\silabel{U_1}{\volt}$')
    ax2.set_ylabel(r'$\silabel{U_{\t{A}}}{\volt}$')
    labels = [l.get_label() for l in lines]
    ax.legend(lines, labels, bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)
    fig.savefig('build/graphic/{}.pdf'.format(name))
