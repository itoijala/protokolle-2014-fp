import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

R_N, U_1 = np.loadtxt('data/a-constant', unpack=True, skiprows=2)
R_N *= 1e3
U_1 *= 1e-3

R_1 = np.loadtxt('data/a-R_1', unpack=True, skiprows=2)

ν = np.empty(4, np.object)
U_A = np.empty(4, np.object)
ν[0], U_A[0] = np.loadtxt('data/a-U_A-100', unpack=True, skiprows=1)
ν[1], U_A[1] = np.loadtxt('data/a-U_A-390', unpack=True, skiprows=1)
ν[2], U_A[2] = np.loadtxt('data/a-U_A-560', unpack=True, skiprows=1)
ν[3], U_A[3], φ = np.loadtxt('data/a-U_A-1000', unpack=True, skiprows=1)

fitborder = [11, 5, 7, 7]

a = np.empty(4, dtype=np.object)
b = np.empty(4, dtype=np.object)
Vprime = np.empty(4)

for i in range(len(R_1)):
    a[i], b[i] = lt.linregress(np.log(ν[i][fitborder[i]:]), np.log(U_A[i][fitborder[i]:] / U_1))

    Vprime[i] = U_A[i][0] / U_1

c =  Vprime / np.sqrt(2)
ν_g = unp.exp((unp.log(c) - b) / a)
ν_g_Vprime = ν_g * Vprime
V_2 = 1 / (1 / Vprime - R_1 / R_N)

for i in range(len(R_1)):
    fig, ax = plt.subplots(1, 1)
    ax.plot(ν[i][:fitborder[i]], U_A[i][:fitborder[i]] / U_1, 'rx', label='nicht gefittete Messwerte', zorder=3)
    ax.plot(ν[i][fitborder[i]:], U_A[i][fitborder[i]:] / U_1, 'gx', label='gefittete Messwerte', zorder=3)
    x = np.linspace(1e1, 1e6, 1000)
    ax.plot(x, np.exp(lt.noms(a[i] * np.log(x) + b[i])), 'm-', label='Fit der Messwerte')
    ax.plot([1e1, 1e6], [c, c], 'b--', label=r"$V' / \sqrt{2}$")
    ax.axvline(x=lt.noms(ν_g[i]), ls='--', c='cyan', label=r'$ν_{\t{g}}$')
    ax.set_xlabel(r'$\silabel{ν}{\hertz}$')
    ax.set_ylabel(r'$U_{\t{A}} / U_{\t{1}}$')
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_ylim(1e0, 1e3)
    ax.legend(loc='best')
    fig.savefig('build/graphic/a-{:.0f}.pdf'.format(R_1[i]))

    tab = lt.SymbolColumnTable()
    tab.add_si_column('ν', ν[i], r'\hertz', places=0)
    tab.add_si_column(r'U_{\t{A}}', U_A[i], r'\volt', figures=3)
    if i == 3:
        tab.add_si_column(r'-φ', φ, r'\degree', places=0)
    tab.savetable('build/table/a-data-{:.0f}.tex'.format(R_1[i]))

V_1 = R_N / R_1

tab = lt.SymbolColumnTable()
tab.add_si_column('R_1', R_1, r'\ohm', places=0)
tab.add_si_column('a', a)
tab.add_si_column('b', b)
tab.add_si_column("V'", Vprime, places=0)
tab.add_si_column('V_1', V_1, figures=3)
tab.add_si_column('V_2', V_2, figures=3)
tab.add_si_column(r'ν_{\t{g}}', ν_g, r'\hertz')
tab.add_si_column(r"ν_{\t{g}} V'", ν_g_Vprime * 1e-3, r'\kilo\hertz')
tab.savetable('build/table/a-fit.tex')

fig, ax = plt.subplots(1, 1)
ax.plot(ν[3] * 1e-3, φ, 'rx')
ax.set_xlim(7e-3, 1e3)
ax.set_xscale('log')
ax.set_xlabel(r'$\silabel{ν}{\kilo\hertz}$')
ax.set_ylabel(r'$\silabel{φ}{\degree}$')
fig.savefig('build/graphic/a-φ.pdf')

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1, projection='polar')
ax.plot(np.radians(φ), U_A[3], 'rx')
ax.set_xlabel(r'$φ$')
fig.text(0.75, 0.65, r'$\silabel{U_{\t{A}}}{\volt}$')
fig.savefig('build/graphic/a-φ-polar.pdf')
