import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt
from labtools.peakdetect import peakdetect

R, R_1, C = np.loadtxt('data/h-constant', unpack=True, skiprows=2)
R *= 1e3
R_1 *= 1e3
C *= 1e-9

ν_calc = 1 / (2 * np.pi * R * C)
τ_calc = 20 * R * C

t, _, U_A = np.loadtxt('data/h', unpack=True)
t *= 1e3

U_max, U_min = peakdetect(U_A, t[t < 16], lookahead=20)
t_max, U_max = np.array(U_max).T
t_min, U_min = np.array(U_min).T

a_p, b_p = lt.linregress(t_max, np.log(U_max))
a_m, b_m = lt.linregress(t_min, np.log(-U_min))

a = lt.umean([a_p, a_m])
τ_meas = -1 / a

t_p = lt.mean(np.ediff1d(t_max))
t_m = lt.mean(np.ediff1d(t_min))
t_mean = lt.umean([t_p[()], t_m[()]])
ν_meas = 1 / (t_mean * 1e-3)

fig, ax = plt.subplots(1, 1)
ax.plot(t, U_A)
ax.plot(t_max, U_max, 'rx', zorder=3)
ax.plot(t_min, U_min, 'gx', zorder=3)
x = np.linspace(0, 30, 1000)
ax.plot(x, np.exp(lt.noms(a_p * x + b_p)), 'g-')
ax.plot(x, -np.exp(lt.noms(a_m * x + b_m)), 'r-')
ax.set_xlim(0, t[-1])
ax.set_xlabel(r'$\silabel{t}{\milli\second}$')
ax.set_ylabel(r'$\silabel{U_{\t{A}}}{\volt}$')
fig.savefig('build/graphic/h.pdf')

tabmax = lt.SymbolColumnTable(valign="t")
tabmax.add_si_column("t", t_max, r"\milli\second", places=2)
tabmax.add_si_column(r"U_{\t{A}}", U_max * 1e3, r"\milli\volt", places=1)

tabmin = lt.SymbolColumnTable(valign="t")
tabmin.add_si_column("t", t_min, r"\milli\second", places=2)
tabmin.add_si_column(r"-U_{\t{A}}", -U_min * 1e3, r"\milli\volt", places=1)

tab = lt.Table()
tab.add(lt.Row())
tab.add(lt.Row())
tab.add(lt.Column(["Maxima", tabmax], "@{}c"))
tab.add(lt.Column(["Minima", tabmin], "c@{}"))
tab.savetable('build/table/h-extrema.tex')

tab = lt.SymbolRowTable()
tab.add_si_row(r'a_{\t{max}}', a_p, r'\per\milli\second')
tab.add_si_row(r'b_{\t{max}}', b_p)
tab.add_hrule()
tab.add_si_row(r'a_{\t{min}}', a_m, r'\per\milli\second')
tab.add_si_row(r'b_{\t{min}}', b_m)
tab.savetable('build/table/h-fit.tex')

tab = lt.SymbolRowTable()
tab.add_si_row(r'ν_{\t{ber}}', ν_calc, '\hertz', places=0)
tab.add_si_row(r'ν_{\t{gem}}', ν_meas, '\hertz')
tab.add_hrule()
tab.add_si_row(r'τ_{\t{ber}}', τ_calc * 1e3, '\milli\second', places=1)
tab.add_si_row(r'τ_{\t{gem}}', τ_meas, '\milli\second')
tab.savetable('build/table/h-results.tex')
