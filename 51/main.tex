\input{header/report.tex}

\SetExperimentNumber{51}

\begin{document}
  \maketitlepage{Schaltungen mit Operationsverstärkern}{25.11.2013}{26.03.2014}
  \section{Ziel}
    Ziel des Versuches ist es, verschiedene Anwendungsbereiche des Operationsverstärkers zu untersuchen.
  \section{Theorie des Operationsverstärkers}
    Das Schaltbild eines Operationsverstärkers befindet sich in Abbildung~\ref{fig:ideal}.
    Für die Ausgangsspannung eines Operationsverstärkers gilt
    \begin{eqn}
      U_{\t{A}} =
      \begin{cases}[l'rCcCl]
        U_{\t{B}} \.,                   &           & & V \g(U_{\t{p}} - U_{\t{n}}) &>& U_{\t{B}} \\
        V \g(U_{\t{p}} - U_{\t{n}}) \., & U_{\t{B}} &≤& V \g(U_{\t{p}} - U_{\t{n}}) &≤& U_{\t{B}} \\
        - U_{\t{B}} \.,                 & U_{\t{B}} &<& V \g(U_{\t{p}} - U_{\t{n}}) \mathrlap{\.,}
        \end{cases}
    \end{eqn}
    wobei $U_{\t{B}}$ die Betriebsspannung und $V$ die Leerlaufverstärkung sind.
    Da die Ausgangsspannung in Phase mit $U_{\t{p}}$ ist, wird der mit $+$ gekennzeichnete Eingang als nicht-invertierender Eingang bezeichnet.
    Der mit $-$ gekennzeichnete Eingang wird invertierender Eingang genannt.
    \begin{figure}
      \includegraphics{graphic/ideal.pdf}
      \caption{Schaltbild eines Operationsverstärkers \cite{anleitung51}.}
      \label{fig:ideal}
    \end{figure}

    Die wichtigsten Attribute eines Operationsverstärkers, die alle frequenzabhängig sein können, sind die Leerlaufverstärkung $V$, die Eingangswiderstände $r_{\t{E}, \t{p}}$ und $r_{\t{E}, \t{n}}$ und der Ausgangswiderstand $r_{\t{A}}$.
    Für einen idealen Operationsverstärker gilt
    \begin{eqn}
      V_{\t{id}} = \infty \., \quad r_{\t{E}, \t{id}} = \infty \., \quad r_{\t{A}, \t{id}} = 0 \..
    \end{eqn}

    Bei realen Operationsverstärkern spielen auch weitere Kenngrößen eine Rolle.
    Die Gleichtaktverstärkung
    \begin{eqn}
      V_{\t{Gl}} = \frac{U_{\t{A}}}{U_{\t{Gl}}}\., \quad U_{\t{p}} = U_{\t{n}} = U_{\t{Gl}}
    \end{eqn}
    beschreibt die Ausgangsspannung für den Fall, dass an beide Eingänge die selbe Spannung $U_{\t{Gl}}$ angeschlossen wird.
    Für eine idealen Operationsverstärker gilt $V_{\t{Gl}} = 0$.

    Die Offsetspannung
    \begin{eqn}
      U_0 = U_{\t{p}} - U_{\t{n}} \., \quad U_{\t{A}} = 0
    \end{eqn}
    ist die Differenz der Eingangsspannungen, für die die Ausgangsspannung verschwindet.
    Für einen idealen Operationsverstärker gilt $U_0 = 0$.

    Aus den endlichen Werten der Eingangswiderstände eines realen Operationsverstärkers folgt, dass auch Eingangsströme fließen.
    Mit den Eingangsströmen $I_{\t{p}}$ und $I_{\t{n}}$ sind der Eingangsruhestrom durch
    \begin{eqn}
      I_{\t{B}} = \frac{I_{\t{p}} - I_{\t{n}}}{2}
    \end{eqn}
    und der Offsetstrom für $U_{\t{p}} = U_{\t{n}} = 0$ durch
    \begin{eqn}
      I_0 = I_{\t{p}} - I_{\t{n}}
    \end{eqn}
    definiert.
    Mit den Eingangsströmen können auch der Differenzeingangswiderstand
    \begin{eqn}
      r_{\t{D}} =
        \begin{cases}
          \frac{U_{\t{p}}}{I_{\t{p}}} \., & U_{\t{n}} = 0 \\
          \frac{U_{\t{n}}}{I_{\t{n}}} \., & U_{\t{p}} = 0
        \end{cases}
    \end{eqn}
    und der Gleichtakteingangswiderstand
    \begin{eqn}
      r_{\t{Gl}} = \frac{U_{\t{Gl}}}{I_{\t{Gl}}} \., \quad I_{\t{Gl}} = I_{\t{p}} + I_{\t{n}}
    \end{eqn}
    definiert werden.
    Für einen idealen Operationsverstärker gilt $I_{\t{B}} = I_0 = 0$ und $r_{\t{D}} = r_{\t{Gl}} = \infty$.
  \section{Linearverstärker}
    \subsection{Theorie}
      Der Operationsverstärker aus Abbildung~\ref{fig:ideal} eignet sich nur bedingt als Linearverstärker, da der Bereich, in dem die Ausgangsspanunng nicht gesättigt ist, klein ist.
      Dieser Bereich kann durch das Einbauen einer Gegenkopplung wie in Abbildung~\ref{fig:invertierend} vergrößert werden.
      Für einen idealen Operationsverstärker gilt für den Verstärkungsgrad des Linearverstärkers
      \begin{eqn}
        V' = - \frac{R_{\t{N}}}{R_1} \..
      \end{eqn}
      Für einen Operationsverstärker mit endlichem Verstärkungsgrad $V$ gilt
      \begin{eqn}
        V' = \g(\frac{1}{V} + \frac{R_1}{R_{\t{N}}} \g(1 + \frac{1}{V}))^{\!\! -1} \stackrel{V \gg 1}{\approx} \g(\frac{1}{V} + \frac{R_1}{R_{\t{N}}})^{\!\! -1} \..
      \end{eqn}
      \begin{figure}
        \includegraphics{graphic/invertierend.pdf}
        \caption{Gegengekoppelter Linearverstärker \cite{anleitung51}.}
        \label{fig:invertierend}
      \end{figure}

      Der Verstärkungsgrad des gegengekoppelten Linearverstärkers fällt mit wachsender Frequenz, wie in Abbildung~\ref{fig:frequenzgang} dargestellt.
      Die Bandbreite ist gleich der Grenzfrequenz, bei der die Verstärkung auf $1/\sqrt{2}$ des Maximums abgefallen ist.
      \begin{figure}
        \includegraphics{graphic/frequenzgang.pdf}
        \caption{Frequenzgang eines gegengekoppelten Linearverstärkers \cite{anleitung51}.}
        \label{fig:frequenzgang}
      \end{figure}
      \FloatBarrier
    \subsection{Aufbau und Durchführung}
      Der Frequenzgang, also die Ausgangsspannung $U_{\t{A}}$ in Abhängigkeit der Frequenz $ν$, des gegengekoppelten Linearverstärkers aus Abbildung~\ref{fig:invertierend} wird für vier verschiedene Verstärkugsgrade gemessen, indem der Widerstand $R_1$ gewechselt wird.
      Die Phasendifferenz $φ$ von Eingangs- und Ausgangssignal wird für einen Verstärkungsgrad gemessen.
    \subsection{Messwerte}
      Verwendet wird ein Widerstand von $R_{\t{N}} = \SI{120}{\kilo\ohm}$ und eine Eingangsspannung von \mbox{$U_1 = \SI{29}{\milli\volt}$.}
      Die gemessenen Frequenzgänge sind in den Tabellen~\ref{tab:a-data-100} bis~\ref{tab:a-data-1000} aufgeführt.
      Die Messwerte der Phasendifferenz befinden sich in Tabelle~\ref{tab:a-data-1000}.
      \begin{table}
        \caption{Messwerte des Frequenzgangs für $R_1 = \SI{100}{\ohm}$.}
        \label{tab:a-data-100}
        \input{table/a-data-100.tex}
      \end{table}
      \begin{table}
        \caption{Messwerte des Frequenzgangs für $R_1 = \SI{390}{\ohm}$.}
        \label{tab:a-data-390}
        \input{table/a-data-390.tex}
      \end{table}
      \begin{table}
        \caption{Messwerte des Frequenzgangs für $R_1 = \SI{560}{\ohm}$.}
        \label{tab:a-data-560}
        \input{table/a-data-560.tex}
      \end{table}
      \begin{table}
        \caption{Messwerte des Frequenzgangs und der Phasendifferenz für $R_1 = \SI{1000}{\ohm}$.}
        \label{tab:a-data-1000}
        \input{table/a-data-1000.tex}
      \end{table}
      \FloatBarrier
    \subsection{Auswertung}
      Die Frequenzgänge sind in den Abbildungen~\ref{fig:a-100} bis~\ref{fig:a-1000} aufgetragen.
      Für den Verstärkungsgrad $V'$ wird der Messwert bei $ν = \SI{10}{\hertz}$ verwendet.
      Der lineare Bereich im doppelt-logarithmischen Diagramm wird durch die lineare Regression
      \begin{eqn}
        \ln.\frac{U_{\t{A}}}{U_{\t{1}}}. = a \ln.ν. + b
      \end{eqn}
      beschrieben.
      Für V werden die Approximationen
      \begin{eqn}
        V_1 = \frac{R_{\t{N}}}{R_1} \\
        V_2 = \g(\frac{1}{V'} - \frac{R_1}{R_{\t{N}}})^{-1}
      \end{eqn}
      berechnet.
      Die Grenzfrequenz wird über
      \begin{eqn}
        ν_{\t{g}} = \exp.\frac{V' / \sqrt{2} - b}{a}.
      \end{eqn}
      berechnet.

      Die Phasendifferenz $φ$ ist in Abbildung~\ref{fig:a-φ} aufgetragen.
      Ein Polarplot von $φ$ gegen $U_{\t{A}}$ befindet sich in Abbildung~\ref{fig:a-φ-polar}.
      \begin{figure}
        \includegraphics{graphic/a-100.pdf}
        \caption{Frequenzgang für $R_1 = \SI{100}{\ohm}$.}
        \label{fig:a-100}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/a-390.pdf}
        \caption{Frequenzgang für $R_1 = \SI{390}{\ohm}$.}
        \label{fig:a-390}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/a-560.pdf}
        \caption{Frequenzgang für $R_1 = \SI{560}{\ohm}$.}
        \label{fig:a-560}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/a-1000.pdf}
        \caption{Frequenzgang für $R_1 = \SI{1000}{\ohm}$.}
        \label{fig:a-1000}
      \end{figure}
      \begin{table}
        \caption{Ergebnisse der linearen Regressionen, Verstärkungsgrad des Linearverstärkers, Schätzungen des Verstärkungsgrads des Operationsverstärkers, Grenzfrequenz.}
        \label{tab:a-fit}
        \OverfullCenter{\input{table/a-fit.tex}}
      \end{table}
      \begin{figure}
        \includegraphics{graphic/a-φ.pdf}
        \caption{Phasendifferenz für $R_1 = \SI{1000}{\ohm}$.}
        \label{fig:a-φ}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/a-φ-polar.pdf}
        \caption{Ausgangspannung und Phasendifferenz für $R_1 = \SI{1000}{\ohm}$.}
        \label{fig:a-φ-polar}
      \end{figure}
      \FloatBarrier
    \subsection{Diskussion}
      Der Theoriewert $V_1$ stimmt ungefähr mit dem gemessenen Wert $V'$ überein.
      Des Weiteren stimmt der Wert $ν_{\t{g}} V'$ bei allen vier Widerständen innerhalb der Fehler überein.

      Wenn die Phase bei höheren Frequenzen um \SI{180}{\degree} gedreht wird, dann würde die Schaltung instabil werden, da nun effektiv der direkte und nicht der invertierende Eingang angeschlossen wäre (Mitkopplung).
      Die Schaltung würde in diesem Fall das Verhalten eines Schmitt-Triggers zeigen.
      Dieser Effekt tritt jedoch nicht auf, da für hohe Frequenzen die Verstärkung kleiner als eins ist und sich somit die Instabilität nicht verstärkt.
  \section{Amperemeter}
    \subsection{Theorie}
      Um einen Verstärker mit geringem Eingangswiderstand zu bauen, zum Beispiel einen Amperemeter, kann die Schaltung aus Abbildung~\ref{fig:amperemeter} verwendet werden.
      Für den Eingangswiderstand gilt
      \begin{eqn}
        r_{\t{e}} = \frac{R_{\t{N}}}{V} \..
      \end{eqn}
      \begin{figure}
        \includegraphics{graphic/amperemeter.pdf}
        \caption{Amperemeter mit niedrigem Eingangswiderstand \cite{anleitung51}.}
        \label{fig:amperemeter}
      \end{figure}
      \FloatBarrier
    \subsection{Aufbau und Durchführung}
      Mit der Schaltung aus Abbildung~\ref{fig:r_e-V} werden der Eingangswiderstand $r_{\t{e}}$ und der Verstärkungsgrad $V$ eines Amperemeters gemessen.
      Dazu wird mit einem Breitbandmillivoltmeter die Spannung $U_{\t{e}}$ in Abhängigkeit der Frequenz $ν$ gemessen.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/r_e-V.pdf}
        \caption{Schaltung zur Messung des Eingangswiderstands und des Verstärkungsgrads eines Amperemeters \cite{anleitung51}.}
        \label{fig:r_e-V}
      \end{figure}
      \FloatBarrier
    \subsection{Messwerte}
      Tabelle~\ref{tab:c-constants} enthält die Werte von Größen, deren Definitionen der Abbildung~\ref{fig:r_e-V} entnommen werden können.
      Tabelle~\ref{tab:c-measure} enthält die Messwerte von $U_{\t{e}}$ in Abhängigkeit der Frequenz $ν$.
      \begin{table}
        \caption{Konstanten der Messung.}
        \label{tab:c-constants}
        \input{table/c-constants.tex}
      \end{table}
      \begin{table}
        \caption{Messwerte.}
        \label{tab:c-measure}
        \input{table/c-measure.tex}
      \end{table}
      \FloatBarrier
    \subsection{Auswertung}
      Die Stromquelle stellt durch ihren Innenwiderstand einen Strom von
      \begin{eqn}
        I_{\t{s}} = \frac{U_{\t{g}}}{R_{\t{V}}} \eqlabel{eqn:I_s}
      \end{eqn}
      zur Verfügung.
      Tabelle \ref{tab:c-constants} enthält den Strom $I_{\t{s}}$ (Gleichung \ref{eqn:I_s}) aus dem Generator und zum Vergleich den Strom $I = U_{\t{A}} / R_{\t{N}}$.
      Abbildung~\ref{fig:c-r_e} zeigt den gemessenen Eingangswiderstand
      \begin{eqn}
        r_{\t{e}} = \frac{U_{\t{e}}}{I_{\t{s}}}\..
      \end{eqn}
      Abbildung~\ref{fig:c-V} zeigt den Verstärkungsgrad
      \begin{eqn}
        V = \frac{R_{\t{N}}}{r_{\t{e}}} \.. \label{eqn:r_e-V}
      \end{eqn}

      Eine lineare Regression von
      \begin{eqn}
        \ln.r_{\t{e}}. = a \ln.ν. + b
      \end{eqn}
      wird durchgeführt.
      Die Ergebnisse befinden sich in Tabelle~\ref{tab:c-fit} und sind in Abbildung~\ref{fig:c-r_e} aufgetragen.
      Die Regressionsgerade ist über Gleichung~\eqref{eqn:r_e-V} auch in Abbildung~\ref{fig:c-V} aufgetragen.
      Der Eingangswiderstand und der Verstärkungsgrad lassen sich beide sehr gut durch lineare Funktionen in der doppelt-logarithmischen Darstellung beschreiben.
      \begin{figure}
        \includegraphics{graphic/c-r_e.pdf}
        \caption{Eingangswiderstand.}
        \label{fig:c-r_e}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/c-V.pdf}
        \caption{Verstärkungsgrad.}
        \label{fig:c-V}
      \end{figure}
      \begin{table}
        \caption{Ergebnisse der linearen Regression.}
        \label{tab:c-fit}
        \input{table/c-fit.tex}
      \end{table}
  \section{Umkehr-Integrator}
    \subsection{Theorie}
      Durch den Austausch des Gegenkopplungswiderstands gegen einen Kondensator ergibt sich der Umkehr-Integrator aus Abbildung~\ref{fig:integrator}.
      Für die Ausgangsspannung gilt
      \begin{eqn}
        U_{\t{A}} = - \frac{1}{R C} \int \!\!\!\! \dif{t} \f{U_1}(t) \.. \label{eqn:integrator}
      \end{eqn}
      Wird eine Sinusspannung
      \begin{eqn}
        \f{U_1}(t) = U_0 \sin.ω t.
      \end{eqn}
      angelegt, ist die Ausgangsspannung
      \begin{eqn}
        U_{\t{A}} = \frac{U_0}{R C ω} \cos.ω t.
      \end{eqn}
      umgekehrt proportional zur Frequenz.
      \begin{figure}
        \includegraphics{graphic/integrator.pdf}
        \caption{Umkehr-Integrator \cite{anleitung51}.}
        \label{fig:integrator}
      \end{figure}
      \FloatBarrier
    \subsection{Aufbau und Durchführung} \label{aufbau-integrator}
      Es wird die Schaltung aus Abbildung~\ref{fig:integrator} verwendet.
      Zuerst wird an einer sinusförmigen Eingangsspannung die Ausgangsspannung für verschiedene Frequenzen gemessen, um die Frequenzabhängigkeit der Amplitude zu bestimmen.
      Dann werden mit einem Speicheroszilloskop die Eingangs- und Ausgangsspannungen für Sinus-, Rechteck- und Dreieckspannungen aufgenommen.
      Ein möglicher Gleichspannungsanteil der Eingangsspannung $U_1$ muss am Funktionsgenerator durch einen Offset verhindert werden.
    \subsection{Messwerte}
      Tabelle~\ref{tab:de-constants} enthält die Werte der verwendeten Bauteile.
      Tabelle~\ref{tab:d-measure} enthält die Messwerte für die Frequenzabhängigkeit der Amplitude.
      \begin{table}
        \caption{Messwerte.}
        \label{tab:d-measure}
        \input{table/d-measure.tex}
      \end{table}
      \begin{table}
        \caption{Konstanten.}
        \label{tab:de-constants}
        \input{table/de-constants.tex}
      \end{table}
      \FloatBarrier
    \subsection{Auswertung}
      Die Messwerte werden einer linearen Regression gemäß
      \begin{eqn}
        \ln.U_{\t{A}}. = a \ln.ν. + b
      \end{eqn}
      unterzogen.
      Die Ergebnisse stehen in Tabelle~\ref{tab:d-fit} und sind mit den Messwerten in Abbildung~\ref{fig:d-U_A} aufgetragen.
      In den Abbildungen \ref{fig:integrator_dreieck}, \ref{fig:integrator_rechteck}, \ref{fig:integrator_sin} befinden sich die integrierten Ausgangsspannungen einer eingehenden Dreieck-, Rechteck- und Sinusspannung.
      \begin{figure}
        \includegraphics{graphic/d-U_A.pdf}
        \caption{Frequenzabhängigkeit der Amplitude.}
        \label{fig:d-U_A}
      \end{figure}
      \begin{table}
        \caption{Ergebnisse der linearen Regression.}
        \label{tab:d-fit}
        \input{table/d-fit.tex}
      \end{table}
      \begin{figure}
        \includegraphics{graphic/d-dreieck.pdf}
        \caption{Integration einer Dreieckspannung.}
        \label{fig:integrator_dreieck}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/d-rechteck.pdf}
        \caption{Integration einer Rechteckspannung.}
        \label{fig:integrator_rechteck}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/d-sin.pdf}
        \caption{Integration einer Sinusspannung.}
        \label{fig:integrator_sin}
      \end{figure}
      \FloatBarrier
  \section{Umkehr-Differentiator}
    \subsection{Theorie}
      Beim Umkehr-Differentiator aus Abbildung~\ref{fig:differentiator} gilt für die Ausgangsspannung
      \begin{eqn}
        U_{\t{A}} = - R C \d{t}{\f{U_1}(t)}; \..
      \end{eqn}
      Für eine Sinusspannung gilt
      \begin{eqn}
        U_{\t{A}} = - ω R C \cos.ω t. \propto ω \..
      \end{eqn}
      \begin{figure}
        \includegraphics{graphic/differentiator.pdf}
        \caption{Umkehr-Differentiator \cite{anleitung51}.}
        \label{fig:differentiator}
      \end{figure}
      \FloatBarrier
    \subsection{Aufbau und Durchführung}
      Es werden die gleichen Schritte wie in Abschnitt~\ref{aufbau-integrator} mit der Schaltung aus Abbildung~\ref{fig:differentiator} durchgeführt.
    \subsection{Messwerte}
      Es werden die Bauteile aus Tabelle~\ref{tab:de-constants} verwendet.
      Tabelle~\ref{tab:e-measure} enthält die Messwerte für die Frequenzabhängigkeit der Amplitude.
      \begin{table}
        \caption{Messwerte.}
        \label{tab:e-measure}
        \input{table/e-measure.tex}
      \end{table}
      \FloatBarrier
    \subsection{Auswertung}
      Die Messwerte werden einer linearen Regression gemäß
      \begin{eqn}
        \ln.U_{\t{A}}. = a \ln.ν. + b
      \end{eqn}
      unterzogen.
      Die Ergebnisse stehen in Tabelle~\ref{tab:e-fit} und sind mit den Messwerten in Abbildung~\ref{fig:e-U_A} aufgetragen.
      In den Abbildungen \ref{fig:differentiator_dreieck}, \ref{fig:differentiator_rechteck}, \ref{fig:differentiator_sin} befinden sich die differentierten Ausgangsspannungen einer eingehenden Dreieck-, Rechteck- und Sinusspannung.
      \begin{figure}
        \includegraphics{graphic/e-U_A.pdf}
        \caption{Frequenzabhängigkeit der Amplitude.}
        \label{fig:e-U_A}
      \end{figure}
      \begin{table}
        \caption{Ergebnisse der linearen Regression.}
        \label{tab:e-fit}
        \input{table/e-fit.tex}
      \end{table}
      \begin{figure}
        \includegraphics{graphic/e-dreieck.pdf}
        \caption{Differentiation einer Dreieckspannung.}
        \label{fig:differentiator_dreieck}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/e-rechteck.pdf}
        \caption{Differentiation einer Rechteckspannung.}
        \label{fig:differentiator_rechteck}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/e-sin.pdf}
        \caption{Differentiation einer Sinusspannung.}
        \label{fig:differentiator_sin}
      \end{figure}
      \FloatBarrier
  \section{Schmitt-Trigger}
    \subsection{Theorie}
      Die Schaltung in Abbildung~\ref{fig:schmitt} wird als Schmitt-Trigger bezeichnet.
      Die Ausgangsspanung springt auf den Wert $U_{\t{B}}$, sobald die Eingangsspannung
      \begin{eqn}
        \frac{R_1}{R_{\t{P}}} U_{\t{B}} \equiv f_{\t{oo}} \label{eqn:schmitt}
      \end{eqn}
      überschreitet.
      Sinkt die Eingangsspannung unter $- f_{\t{oo}}$, springt die Ausgangsspannung auf den Wert $- U_{\t{B}}$.
      Die Differenz $2 f_{\t{oo}}$ zwischen den Umschaltpunkten ist die Schalthysterese.
      \begin{figure}
        \includegraphics{graphic/schmitt.pdf}
        \caption{Schmitt-Trigger \cite{anleitung51}.}
        \label{fig:schmitt}
      \end{figure}
      \FloatBarrier
    \subsection{Aufbau und Durchführung}
      Es werden die Betriebspannung $U_{\t{B}}$ des Operationsverstärkers und die Spannung gemessen, an der der Schmitt-Trigger aus Abbildung~\ref{fig:schmitt} anfängt zu schalten.
    \subsection{Messwerte und Auswertung}
      Tabelle~\ref{tab:f} enthält die Daten der verwendeten Bauteile und den Theoriewert $f_{\t{oo}}$ und den gemessenen Wert $U_1$ der Umklappspannung.
      Abbildung~\ref{fig:f} zeigt den Zeitverlauf der Frequenzgeneratoramplitude und des Schmitt-Triggers.
      \begin{table}
        \caption{Daten der verwendeten Bauteile und Theoriewert $f_{\t{oo}}$ und gemessener Wert $U_1$ der Umklappspannung.}
        \label{tab:f}
        \input{table/f.tex}
      \end{table}
      \begin{figure}
        \includegraphics{graphic/f.pdf}
        \caption{Gemessener Zeitverlauf der Frequenzgeneratoramplitude und der Schmitt-Trigger-Amplitude.}
        \label{fig:f}
      \end{figure}
  \section{Signalgenerator}
    \subsection{Theorie}
      Die Schaltung in Abbildung~\ref{fig:dreieck}, die aus einem Schmitt-Trigger und einem Integrator besteht, produziert Rechteck- und Dreiecksignale.
      Die Frequenz der erzeugten Signale hängt von der Zeitkonstante des Integrators und von dem Teilerverhältnis der Mitkopplung des Triggers ab.
      Für die Dreiecksspannung zum Zeitpunkt $t_{\t{s}}$, an dem der Trigger umschaltet, gilt nach Formel~\eqref{eqn:integrator}
      \begin{eqns}
        \f{U_{\t{A}}}(t_{\t{s}}) &=& - \frac{1}{R C} \int_{\,0}^{t_{\t{s}}} \!\! \dif{t} U_{\t{B}} \\
                                 &=& - \frac{1}{R C} U_{\t{B}} t_{\t{s}} \.,
      \end{eqns}
      wobei $U_{\t{B}}$ die Amplitude der Ausgangsspannung des Triggers ist.
      Mit Formel~\eqref{eqn:schmitt} folgt
      \begin{eqns}
        - \frac{1}{R C} U_{\t{B}} t_{\t{s}} &\stackrel{!}{=}& -2 \frac{R_1}{R_{\t{p}}} U_{\t{B}} \\
        \Leftrightarrow \quad t_{\t{s}}     &=              & 2 \frac{R_1}{R_{\t{p}}} R C \..
      \end{eqns}
      Für die Frequenz ergibt sich somit
      \begin{eqn}
        ν = \frac{1}{2 t_{\t{s}}} = \frac{R_{\t{p}}}{R_1} \frac{1}{4 R C} \..
      \end{eqn}
      Für die Spannung gilt
      \begin{eqn}
        \abs{\f{U_{\t{A}}}(t_{\t{s}})} = 2 \frac{R_1}{R_{\t{p}}} U_{\t{B}} \..
      \end{eqn}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/dreieck.pdf}
        \caption{Rechteck- und Dreieckgenerator \cite{anleitung51}.}
        \label{fig:dreieck}
      \end{figure}
      \FloatBarrier
    \subsection{Aufbau und Durchführung}
      An dem Dreieckgenerator aus Abbildung~\ref{fig:dreieck} werden die Frequenz und Amplitude des erzeugten Signals gemessen.
      Außerdem wird die Form des Signals mit einem Oszilloskop überprüft.
    \subsection{Messwerte und Auswertung}
      Abbildung~\ref{fig:g} zeigt die gemessenen Rechteck- und Dreiecksignale.
      Tabelle~\ref{tab:g} enthält die gemessenen Werte für die Frequenz $ν$ und die Größe $U_{\t{A}}$ des Dreiecksignals.
      Dabei sind $U_{\t{Betrieb}}$ und $U_{\t{B}}$ als Amplituden und $U_{\t{A}}$ als Spitze-Spitze-Spannung angegeben.
      \begin{figure}
        \includegraphics{graphic/g.pdf}
        \caption{Zeitabhängigkeit des Schmitt-Triggers und der erzeugten Dreieckspannung.}
        \label{fig:g}
      \end{figure}
      \begin{table}
        \caption{Frequenz und Größe des Dreiecksignals.}
        \label{tab:g}
        \input{table/g.tex}
      \end{table}
  \section{Sinusschwingungen mit zeitabhängiger Amplitude}
    \subsection{Theorie}
      Die Schaltung in Abbildung~\ref{fig:dgl}, die aus zwei Integratoren und einem gegengekoppelten Linearverstärker besteht, erzeugt Sinusschwingungen mit zu- oder abnehmender Amplitude.
      Die Ausgangsspannung wird durch die Differentialgleichung
      \begin{eqn}
        \d{t}[2]{U_{\t{A}}}; - \frac{η}{10 R C} \d{t}{U_{\t{A}}}; + \frac{1}{\g(R C)²} U_{\t{A}} = 0
      \end{eqn}
      beschrieben, die durch
      \begin{eqn}
        \f{U_{\t{A}}}(t) = U_0 \exp.\frac{η t}{20 R C}. \sin.\frac{t}{R C}. \., \quad η \ll 100
      \end{eqn}
      gelöst wird.
      Dabei ist $-1 ≤ η ≤ 1$ durch den Potentiometer einstellbar.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/dgl.pdf}
        \caption{Generator zur Erzeugung von Sinusschwingungen mit zeitabhängiger Amplitude \cite{anleitung51}.}
        \label{fig:dgl}
      \end{figure}
      \FloatBarrier
    \subsection{Aufbau und Durchführung}
      Mit einem Speicheroszilloskop wird eine Aufnahme der Ausgangsspannung der Schaltung aus Abbildung~\ref{fig:dgl} angefertigt.
      Dabei wird eine Kapazität von $C = \SI{100}{\nano\farad}$ und eine Potentiometereinstellung von $η = -1$ verwendet.
    \subsection{Auswertung}
      Abbildung~\ref{fig:h} zeigt den zeitlichen Verlauf einer gedämpften Sinusschwingung.
      Die Extrema werden detektiert und mit der linearen Funktion
      \begin{eqn}
        \ln.U_{\t{A}}. = a t + b \label{eqn:extinction_fit}
      \end{eqn}
      gefittet.
      Für die Minima wird $- U_{\t{A}}$ verwendet.
      Die gefundenen Extrema sind in Tabelle \ref{tab:h-extrema} aufgeführt.
      Die Extrema und gefitteten Funktionen sind ebenfalls in Abbildung~\ref{fig:h} gezeigt.
      Tabelle~\ref{tab:h-fit} enthält die resultierenden Fitparameter.
      \begin{table}
        \caption{Gefundene Extrema.}
        \label{tab:h-extrema}
        \input{table/h-extrema.tex}
      \end{table}
      \begin{figure}
        \includegraphics{graphic/h.pdf}
        \caption{Zeitverlauf der gedämpften Sinusschwingung.}
        \label{fig:h}
      \end{figure}
      \begin{table}
        \caption{Ergebnisse der linearen Regressionen.}
        \label{tab:h-fit}
        \input{table/h-fit.tex}
      \end{table}

      Tabelle~\ref{tab:h-results} enthält zum Vergleich die gemessenen und die berechneten Werte für die Frequenz
      \begin{eqn}
        ν = \frac{1}{2 \PI R C}
      \end{eqn}
      und die Zeitkonstante
      \begin{eqn}
        τ = \frac{20 R C}{\abs{η}} \..
      \end{eqn}
      Dabei wird die Frequenz über den Kehrwert der Differenz Nachfolgender Extrema berechnet.
      Für die Zeitkonstante gilt
      \begin{eqn}
        τ = \frac{-1}{a} \..
      \end{eqn}
      \begin{table}
        \caption{Vergleich von gemessener und berechneter Frequenz $ν$ und Zeitkonstante $τ$.}
        \label{tab:h-results}
        \input{table/h-results.tex}
      \end{table}
  \section{Logarithmierer und Exponentialgenerator}
    \subsection{Theorie}
      Eine Halbleiterdiode kann verwendet werden, um die Eingangsspannung logarithmisch oder exponentiell zu verstärken.
      Aufgrund der exponentiellen Kennlinie der Diode gilt für den Logarithmierer in Abbildung~\ref{fig:logarithmierer}
      \begin{eqn}
        U_{\t{A}} = \frac{k_{\t{B}} T}{e} \ln.\frac{U_{\t{E}}}{I_0 R}. \label{eqn:theory-log}
      \end{eqn}
      und für den Exponentialgenerator in Abbildung~\ref{fig:exponential}
      \begin{eqn}
        U_{\t{A}} = R I_0 \exp.\frac{e}{k_{\t{B}} T} U_{\t{E}}. \., \label{eqn:theory-exp}
      \end{eqn}
      wobei $k_{\t{B}}$ die Boltzmann-Konstante, $T$ die Temperatur und $e$ die Elementarladung sind.
      \begin{figure}
        \includegraphics{graphic/logarithmierer.pdf}
        \caption{Logarithmierer \cite{anleitung51}.}
        \label{fig:logarithmierer}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/exponential.pdf}
        \caption{Exponentialgenerator \cite{anleitung51}.}
        \label{fig:exponential}
      \end{figure}
      \FloatBarrier
    \subsection{Aufbau und Durchführung}
      Für den Logarithmierer aus Abbildung~\ref{fig:logarithmierer} und den Exponentialgenerator aus Abbildung~\ref{fig:exponential} wird die Ausgangsspannung $U_{\t{A}}$ in Abhängigkeit der Eingangsspannung $U_{\t{E}}$ gemessen.
      Dabei werden eine Gleichspannung als Eingangsspannung und ein Gleichspannungsmessgerät verwendet.
    \subsection{Messwerte}
      Die Messwerte für den Logarithmierer stehen in Tabelle~\ref{tab:i-log-data}, die für den Exponentialgenerator in Tabelle~\ref{tab:i-exp-data}.
      \begin{table}
        \caption{Messwerte für den Logarithmierer.}
        \label{tab:i-log-data}
        \input{table/i-log-data.tex}
      \end{table}
      \begin{table}
        \caption{Messwerte für den Exponentialgenerator.}
        \label{tab:i-exp-data}
        \input{table/i-exp-data.tex}
      \end{table}
      \FloatBarrier
    \subsection{Auswertung}
      Für den Logarithmierer wird Gleichung~\eqref{eqn:theory-log} durch
      \begin{eqn}
        U_{\t{A}} = a \ln.\frac{U_1}{b}.
      \end{eqn}
      ausgedrückt.
      Die Parameter $a$ und $b$ werden durch eine lineare Ausgleichsrechnung von
      \begin{eqn}
        U_{\t{A}} = a \ln.U_1. - a \ln.b.
      \end{eqn}
      bestimmt.
      Die Sperrschichttemperatur ergibt sich durch
      \begin{eqn}
        T = \frac{e}{k_{\t{B}}} a \..
      \end{eqn}
      Die Ergebnisse für $a$, $b$ und $T$ stehen in Tabelle~\ref{tab:i-log-fit}.
      Die Messwerte und die Ausgleichsgerade sind in Abbildung~\ref{fig:i-log} aufgetragen.
      \begin{figure}
        \includegraphics{graphic/i-log.pdf}
        \caption{Messwerte und Ausgleichsgerade für den Logarithmierer.}
        \label{fig:i-log}
      \end{figure}
      \begin{table}
        \caption{Ergebnisse der Ausgleichsrechnung und berechnete Sperrschichttemperatur für den Logarithmierer.}
        \label{tab:i-log-fit}
        \input{table/i-log-fit.tex}
      \end{table}

      Für den Exponentialgenerator wird Gleichung~\eqref{eqn:theory-exp} durch
      \begin{eqn}
        \ln.\frac{U_{\t{A}}}{d}. = c U_1
      \end{eqn}
      ausgedrückt.
      Die Parameter $a$ und $b$ werden durch eine lineare Ausgleichsrechnung von
      \begin{eqn}
        \ln.U_{\t{A}}. = c U_1 + \ln.d.
      \end{eqn}
      bestimmt.
      Die Sperrschichttemperatur ergibt sich durch
      \begin{eqn}
        T = \frac{e}{k_{\t{B}}} \frac{1}{c} \..
      \end{eqn}
      Die Ergebnisse für $c$, $d$ und $T$ stehen in Tabelle~\ref{tab:i-exp-fit}.
      Die Messwerte und die Ausgleichsgerade sind in Abbildung~\ref{fig:i-exp} aufgetragen.
      \begin{figure}
        \includegraphics{graphic/i-exp.pdf}
        \caption{Messwerte und Ausgleichsgerade für den Exponentialgenerator.}
        \label{fig:i-exp}
      \end{figure}
      \begin{table}
        \caption{Ergebnisse der Ausgleichsrechnung und berechnete Sperrschichttemperatur für den Exponentialgenerator.}
        \label{tab:i-exp-fit}
        \input{table/i-exp-fit.tex}
      \end{table}
      \FloatBarrier
    \subsection{Diskussion}
      Der Schmitt-Trigger zeigte das erwartete Verhalten ab einer in Tabelle~\ref{tab:f} angegebenen Frequenzgeneratoramplitude.

      Es war nicht möglich, den Funktionsbereich des Logarithmierers zu bestimmen, da die Spannungsquelle innerhalb dieses Bereichs sein Maximum erreichte.
      Der Exponentialgenerator ist ab einem Punkt im Bereich \SIrange{0.65}{0.70}{\volt} gesättigt und kann ab diesem Punkt nicht mehr verwendet werden.

      Die berechneten Werte für die Sperrschichttemperatur $T$ in den Tabellen~\ref{tab:i-log-fit} und~\ref{tab:i-exp-fit} stimmen gut überein.
  \makebibliography
\end{document}
