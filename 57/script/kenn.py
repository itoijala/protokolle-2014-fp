import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

U_a, V_080, U_080, V_085, U_085, V_090, U_090, V_100, U_100 = np.loadtxt('data/kenn', unpack=True, skiprows=4)

Ut_080 = U_080 / (1000**2 * V_080**2 * 10)
Ut_085 = U_085 / (1000**2 * V_085**2 * 10)
Ut_090 = U_090 / (1000**2 * V_090**2 * 10)
Ut_100 = U_100 / (1000**2 * V_100**2 * 10)

tab_Ua = lt.SymbolColumnTable(valign="t")
tab_Ua.add_si_column(r'U_{\t{a}}', U_a, r'\volt', places=0)

tab_080 = lt.SymbolColumnTable(valign="t")
tab_080.add_column(lt.VerticalSpace())
tab_080.add_si_column(r'V_{\t{N}}', V_080, places=0)
tab_080.add_si_column(r'U', U_080, r'\volt', figures=3)
tab_080.add_si_column(r'\tilde{U}', Ut_080 * 1e9, r'\nano\volt', figures=3)
tab_080.add_column(lt.VerticalSpace())

tab_085 = lt.SymbolColumnTable(valign="t")
tab_085.add_column(lt.VerticalSpace())
tab_085.add_si_column(r'V_{\t{N}}', V_085, places=0)
tab_085.add_si_column(r'U', U_085, r'\volt', places=3)
tab_085.add_si_column(r'\tilde{U}', Ut_085 * 1e9, r'\nano\volt', figures=3)
tab_085.add_column(lt.VerticalSpace())

tab_090 = lt.SymbolColumnTable(valign="t")
tab_090.add_column(lt.VerticalSpace())
tab_090.add_si_column(r'V_{\t{N}}', V_090, places=0)
tab_090.add_si_column(r'U', U_090, r'\volt', places=3)
tab_090.add_si_column(r'\tilde{U}', Ut_090 * 1e9, r'\nano\volt', figures=3)
tab_090.add_column(lt.VerticalSpace())

tab_100 = lt.SymbolColumnTable(valign="t")
tab_100.add_column(lt.VerticalSpace())
tab_100.add_si_column(r'V_{\t{N}}', V_100, places=0)
tab_100.add_si_column(r'U', U_100, r'\volt', places=3)
tab_100.add_si_column(r'\tilde{U}', Ut_100 * 1e9, r'\nano\volt', figures=3)

tab = lt.Table()
tab.add(lt.Row())
tab.add(lt.Row())
tab.add(lt.Column(["$I_0$", tab_Ua],"@{}c"))
tab.add(lt.Column([r"\SI{0.80}{\ampere}", tab_080],"@{}c@{}"))
tab.add(lt.Column([r"\SI{0.85}{\ampere}", tab_085],"@{}c@{}"))
tab.add(lt.Column([r"\SI{0.90}{\ampere}", tab_090],"@{}c@{}"))
tab.add(lt.Column([r"\SI{1.00}{\ampere}", tab_100],"@{}c@{}"))
tab.savetable('build/table/kenn.tex')

fig, ax = plt.subplots(1, 1)
ax.plot(U_a, Ut_080 * 1e9, 'gx', label=r'\SI{0.80}{\ampere}')
ax.plot(U_a, Ut_085 * 1e9, 'rx', label=r'\SI{0.85}{\ampere}')
ax.plot(U_a, Ut_090 * 1e9, 'bx', label=r'\SI{0.90}{\ampere}')
ax.plot(U_a, Ut_100 * 1e9, 'mx', label=r'\SI{1.00}{\ampere}')
ax.set_xlabel(r'$\silabel{U_{\t{a}}}{\volt}$')
ax.set_ylabel(r'$\silabel{\tilde{U}}{\nano\volt}$')
ax.legend(loc='best')
fig.savefig('build/graphic/kenn.pdf')
