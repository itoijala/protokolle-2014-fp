import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

def calc_Δν(ν, Δν):
    res = np.empty(Δν.shape, np.object)
    for i in range(len(ν)):
        if Δν[i] != 0:
            res[i] = Δν[i]
        elif ν[i] <= 100:
            res[i] =  0.15 * ν[i] - 0.3
        elif ν[i] <= 10e3:
            res[i] =  0.140 * ν[i] + 0.7
        elif ν[i] <= 50e3:
            res[i] =  0.135 * ν[i] + 0.05e3
        elif ν[i] <= 100e3:
            res[i] =  0.109 * ν[i] + 1.2e3
    return res

e = lt.constant('elementary charge')

R = 4680
I0 = 2.5e-3

ν, τ, V, U, δU, Δν, δΔν = np.loadtxt('data/rein', unpack=True, skiprows=1)
U = unp.uarray(U, δU)
Δν = unp.uarray(Δν, δΔν)
ν *= 1e3
Δν *= 1e3

Δν = calc_Δν(ν, Δν)

Ut = U / (1000**2 * V**2 * 10**2 * 10) # Selektivverstärker

I = Ut / R**2
W = I / Δν

def f(x, a):
    return a

νsh = ν[np.logical_and(ν > 1e2, ν <= 1e5)]
Wsh = W[np.logical_and(ν > 1e2, ν <= 1e5)]
W_sh, = lt.curve_fit(f, νsh, Wsh, p0=[1e-19])

tab = lt.SymbolColumnTable()
tab.add_si_column(r'ν', ν * 1e-3, r'\kilo\hertz', figures=([1] * 15 + [2] * 5 + [3] * 20), squeeze_unit=True)
tab.add_si_column(r'τ', τ, r'\second', squeeze_unit=True)
tab.add_si_column(r'V', V, squeeze_unit=True)
tab.add_si_column(r'U', U, r'\volt', squeeze_unit=True)
tab.add_si_column(r'\tilde{U}', Ut * 1e9, r'\nano\volt', squeeze_unit=True)
tab.add_si_column(r'\Del{ν}', Δν * 1e-3, r'\kilo\hertz', squeeze_unit=True)
tab.add_si_column(r'W', W * 1e21, r'\ampere\squared\second', exp='e-21', squeeze_unit=True)
tab.savetable('build/table/rein.tex')

tab = lt.SymbolRowTable()
tab.add_si_row(r'W_{\t{sh}}', W_sh * 1e22, r'\ampere\squared\second', exp='e-22')
tab.add_si_row(r'W_{\t{sh}, \t{th}}', lt.noms(2 * e * I0 * 1e22), r'\ampere\squared\second', exp='e-22', figures=3)
tab.savetable('build/table/rein-result.tex')

fig, ax = plt.subplots(1, 1)
ax.errorbar(ν * 1e-3, lt.vals(W), yerr=lt.stds(W), fmt='g_', zorder=4)
ax.errorbar(νsh * 1e-3, lt.vals(Wsh), yerr=lt.stds(Wsh), fmt='r_', zorder=4)
ax.axhline(y=lt.noms(W_sh))
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlim(7e-3, 1e3)
ax.set_xlabel(r'$\silabel{ν}{\kilo\hertz}$')
ax.set_ylabel(r'$\silabel{W}{\ampere\squared\second}$')
fig.savefig('build/graphic/rein.pdf')
