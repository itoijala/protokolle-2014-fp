import pickle

import numpy as np

import scipy.integrate

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

A = dict()

U0 = 200e-3 * 1e-3 / np.sqrt(2)

for circuit, xlim, ylim, places in [('simple',     (-2, 105), (-30,   800),   3),
                                    ('correlator', ( 0, 102), (-3000, 45000), 2)]:
    ν, V, U = np.loadtxt('data/A-' + circuit, unpack=True, skiprows=2)

    ν *= 1e3
    Ut = U / (1000**2 * V**2 * 10)
    Utt = Ut / U0**2

    As = scipy.integrate.simps(Utt, ν)
    At = scipy.integrate.trapz(Utt, ν)
    A[circuit] = unc.ufloat(As, np.abs(As - At))

    tab = lt.SymbolColumnTable()
    tab.add_si_column('ν', ν[:len(ν) / 3 + 1] * 1e-3, r'\kilo\hertz', places=3, squeeze_unit=True)
    tab.add_si_column(r'V_{\t{N}}', V[:len(ν) / 3 + 1], places=0, squeeze_unit=True)
    tab.add_si_column(r'U', U[:len(ν) / 3 + 1], r'\volt', places=places, squeeze_unit=True)
    tab.add_si_column(r'\tilde{\tilde{U}}', Utt[:len(ν) / 3 + 1] * 1e3, r'\per\volt', figures=4, exp='e-3', squeeze_unit=True)
    #tab.add_column(lt.VerticalSpace())
    tab.add_si_column('ν', ν[len(ν) / 3 + 1:2 * len(ν) / 3 + 2] * 1e-3, r'\kilo\hertz', places=3, squeeze_unit=True)
    tab.add_si_column(r'V_{\t{N}}', V[len(ν) / 3 + 1:2 * len(ν) / 3 + 2], places=0, squeeze_unit=True)
    tab.add_si_column(r'U', U[len(ν) / 3 + 1:2 * len(ν) / 3 + 2], r'\volt', places=places, squeeze_unit=True)
    tab.add_si_column(r'\tilde{\tilde{U}}', Utt[len(ν) / 3 + 1:2 * len(ν) / 3 + 2] * 1e3, r'\per\volt', figures=4, exp='e-3', squeeze_unit=True)
    #tab.add_column(lt.VerticalSpace())
    tab.add_si_column('ν', ν[2 * len(ν) / 3 + 2:] * 1e-3, r'\kilo\hertz', places=3, squeeze_unit=True)
    tab.add_si_column(r'V_{\t{N}}', V[2 * len(ν) / 3 + 2:], places=0, squeeze_unit=True)
    tab.add_si_column(r'U', U[2 * len(ν) / 3 + 2:], r'\volt', places=places, squeeze_unit=True)
    tab.add_si_column(r'\tilde{\tilde{U}}', Utt[2 * len(ν) / 3 + 2:] * 1e3, r'\per\volt', figures=4, exp='e-3', squeeze_unit=True)
    tab.savetable('build/table/A-' + circuit + '.tex')

    fig, ax = plt.subplots(1, 1)
    ax.plot(ν * 1e-3, Utt * 1e3, 'rx')
    ax.set_xlim(*xlim)
    ax.set_ylim(*ylim)
    ax.set_xlabel(r'$\silabel{ν}{\kilo\hertz}$')
    ax.set_ylabel(r'$\silabel{\tilde{\tilde{U}}}[e-3]{\per\volt}$')
    fig.savefig('build/graphic/A-' + circuit + '.pdf')

tab = lt.SymbolRowTable()
tab.add_si_row(r'A_{\t{simple}}', A['simple'] * 1e-3, r'\kilo\hertz\per\volt')
tab.add_si_row(r'A_{\t{correlator}}', A['correlator'] * 1e-3, r'\kilo\hertz\per\volt')
tab.savetable('build/table/A.tex')

pickle.dump(A, open('build/result/A.pickle', 'wb'))
