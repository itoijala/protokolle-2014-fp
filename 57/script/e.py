import pickle

import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

e0 = lt.constant('elementary charge')

A = pickle.load(open('build/result/A.pickle', 'rb'))['simple']

ν, R, U_a = np.loadtxt('data/e-const', skiprows=2)
Δν = 0.135 * ν + 0.05

I, V, U = np.loadtxt('data/e', unpack=True)
I *= 1e-3

Ut = U / (1000**2 * V**2 * 10)

a, b = lt.linregress(I, Ut)
e = a / (2 * A * R**2)

tab = lt.SymbolColumnTable()
tab.add_si_column(r'I', I[:len(I) / 3] * 1e3, r'\milli\ampere', places=1)
tab.add_si_column(r'V', V[:len(I) / 3], places=0)
tab.add_si_column(r'U', U[:len(I) / 3], r'\volt', figures=2)
tab.add_si_column(r'\tilde{U}', Ut[:len(I) / 3] * 1e9, r'\nano\volt', figures=2)
tab.add_column(lt.VerticalSpace())
tab.add_si_column(r'I', I[len(I) / 3: 2 * len(I) / 3] * 1e3, r'\milli\ampere', places=1)
tab.add_si_column(r'V', V[len(I) / 3: 2 * len(I) / 3], places=0)
tab.add_si_column(r'U', U[len(I) / 3: 2 * len(I) / 3], r'\volt', figures=2)
tab.add_si_column(r'\tilde{U}', Ut[len(I) / 3: 2 * len(I) / 3] * 1e9, r'\nano\volt', figures=2)
tab.add_column(lt.VerticalSpace())
tab.add_si_column(r'I', I[2 * len(I) / 3:] * 1e3, r'\milli\ampere', places=1)
tab.add_si_column(r'V', V[2 * len(I) / 3:], places=0)
tab.add_si_column(r'U', U[2 * len(I) / 3:], r'\volt', figures=2)
tab.add_si_column(r'\tilde{U}', Ut[2 * len(I) / 3:] * 1e9, r'\nano\volt', figures=2)
tab.savetable('build/table/e-IUcharac.tex')

tab = lt.SymbolRowTable()
tab.add_si_row('ν', ν, r'\kilo\hertz', places=0)
tab.add_si_row('R', R, r'\ohm', places=0)
tab.add_si_row(r'U_\t{a}', U_a, r'\volt', places=0)
tab.savetable('build/table/e-const.tex')

tab = lt.SymbolRowTable()
tab.add_si_row('a', a * 1e6, r'\volt\per\ampere', exp='e-6')
tab.add_si_row('b', b * 1e10, r'\volt', exp='e-10')
tab.add_hrule()
tab.add_si_row(r'e_{\t{exp}}', e * 1e19, r'\coulomb', exp='e-19')
tab.add_si_row(r'e_{\t{lit}}', e0 * 1e19, r'\coulomb', exp='e-19')
tab.savetable('build/table/e-result.tex')

fig, ax = plt.subplots(1, 1)
x = np.linspace(0, 6e-3)
ax.plot(I * 1e3, Ut * 1e9, 'rx', zorder=4)
ax.plot(x * 1e3, lt.noms(a * x + b) * 1e9, 'b-')
#ax.set_ylim(-0.01, 0.25)
ax.set_xlabel(r'$\silabel{I}{\milli\ampere}$')
ax.set_ylabel(r'$\silabel{\tilde{U}}{\nano\volt}$')
fig.savefig('build/graphic/e.pdf')
