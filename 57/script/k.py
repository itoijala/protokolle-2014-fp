import pickle

import numpy as np

from scipy.constants import C2K

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

A = pickle.load(open('build/result/A.pickle', 'rb'))

k0 = lt.constant('Boltzmann constant')

T, R_sc, t_RC, V_dca, V_pa, V_sa, Q = np.loadtxt('data/k-const', skiprows=2)
T = C2K(T)

tab = lt.SymbolRowTable()
tab.add_si_row('T', T, r'\kelvin', places=2)
tab.add_si_row('τ', t_RC, r'\milli\second', places=0)
tab.add_si_row(r'V_{\t{sa}}', V_sa, places=0)
tab.add_si_row(r'Q', Q, places=0)
tab.savetable('build/table/k-const.tex')

tabF = lt.SymbolRowTable()

for circuit, xlim, ylim in [('simple',     (-10, 1010), ( 0,   0.45)),
                            ('correlator', (-10, 1010), (-0.2, 9))]:
    R, V, U = np.loadtxt('data/k-' + circuit + '-small', unpack=True, skiprows=2)

    Ut = U / (1000**2 * V**2 * 10)

    tab = lt.SymbolColumnTable()
    tab.add_si_column('R', R, r'\ohm', places=0)
    tab.add_si_column(r'V_{\t{N}}', V, places=0)
    tab.add_si_column(r'U', U, r'\volt', figures=3)
    tab.add_si_column(r'\tilde{U}', Ut * 1e12, r'\pico\volt', figures=3)
    tab.savetable('build/table/k-' + circuit + '-small.tex')

    a, b = lt.linregress(R, Ut)
    k = a / (4 * T * A[circuit])
    F = Ut[5] / 4 / k0 / T / R[5] / 10 / A[circuit] # /10 because V= was not squared yet

    tab = lt.SymbolRowTable()
    if(circuit == 'simple'):
        tab.add_si_row(r'a_{\t{simple, small}}', a * 1e15, r'\volt\per\ohm', exp='e-15')
        tab.add_si_row(r'b_{\t{simple, small}}', b * 1e12, r'\micro\volt', exp='e-12')
        tab.add_hrule()
        tab.add_si_row(r'k_{\t{B, simple, small}}', k * 1e23, r'\joule\per\kelvin', exp='e-23')
        tab.add_si_row(r'k_{\t{B, lit}}', k0 * 1e23, r'\joule\per\kelvin', exp='e-23')
        tabF.add_si_row(r'F_{\t{simple, small}}', F)
        tabF.add_si_row(r'F_{\t{simple, small}}', 10 * unp.log10(F), r'\decibel')
    else:
        tab.add_si_row(r'a_{\t{correlator, small}}', a * 1e15, r'\volt\per\ohm', exp='e-15')
        tab.add_si_row(r'b_{\t{correlator, small}}', b * 1e12, r'\micro\volt', exp='e-12')
        tab.add_hrule()
        tab.add_si_row(r'k_{\t{B, correlator, small}}', k * 1e23, r'\joule\per\kelvin', exp='e-23')
        tab.add_si_row(r'k_{\t{B, lit}}', k0 * 1e23, r'\joule\per\kelvin', exp='e-23')
        tabF.add_hrule()
        tabF.add_si_row(r'F_{\t{correlator, small}}', F)
        tabF.add_si_row(r'F_{\t{correlator, small}}', 10 * unp.log10(F), r'\decibel')
    tab.savetable('build/table/k-res-' + circuit + '-small.tex')
    tabF.savetable('build/table/F.tex')

    fig, ax = plt.subplots(1, 1)
    ax.plot(R, Ut * 1e12, 'rx', zorder=4)
    x = np.linspace(*xlim)
    ax.plot(x, lt.noms(a * x + b) * 1e12, 'b-')
    ax.set_xlim(*xlim)
    ax.set_ylim(*ylim)
    ax.set_xlabel(r'$\silabel{R}{\ohm}$')
    ax.set_ylabel(r'$\silabel{\tilde{U}}{\pico\volt}$')
    fig.savefig('build/graphic/k-' + circuit + '-small.pdf')

for circuit, Rlim, xlim, ylim, figures in [('simple',     20e3, (-1, 100), (-2,   15), 4),
                                           ('correlator', 15e3, (-1,  51), (-20, 150), 3)]:
    R, V, U = np.loadtxt('data/k-' + circuit + '-large', unpack=True, skiprows=2)
    R *= 1e3

    Ut = U / (1000**2 * V**2 * 10)

    tab = lt.SymbolColumnTable()
    tab.add_si_column('R', R[:len(R) / 2 + 1] * 1e-3, r'\ohm', places=1)
    tab.add_si_column(r'V_{\t{N}}', V[:len(R) / 2 + 1], places=0)
    tab.add_si_column(r'U', U[:len(R) / 2 + 1], r'\volt', figures=figures)
    tab.add_si_column(r'\tilde{U}', Ut[:len(R) / 2 + 1] * 1e12, r'\pico\volt', figures=figures)
    tab.add_column(lt.VerticalSpace())
    tab.add_si_column('R', R[len(R) / 2 + 1:] * 1e-3, r'\ohm', places=1)
    tab.add_si_column(r'V_{\t{N}}', V[len(R) / 2 + 1:], places=0)
    tab.add_si_column(r'U', U[len(R) / 2 + 1:], r'\volt', figures=figures)
    tab.add_si_column(r'\tilde{U}', Ut[len(R) / 2 + 1:] * 1e12, r'\pico\volt', figures=figures)
    tab.savetable('build/table/k-' + circuit + '-large.tex')

    Rbad, Ubad = R[R > Rlim], Ut[R > Rlim]
    R, Ut = R[R <= Rlim], Ut[R <= Rlim]

    a, b = lt.linregress(R, Ut)
    k = a / (4 * T * A[circuit])
    F = Ut[5] / 4 / k0 / T / R[5] / 10 / A[circuit] # /10 because V= was not squared yet
    # F still to be included into table!!

    tab = lt.SymbolRowTable()
    if(circuit == 'simple'):
        tab.add_si_row(r'a_{\t{simple, large}}', a * 1e15, r'\volt\per\ohm', exp='e-15')
        tab.add_si_row(r'b_{\t{simple, large}}', b * 1e12, r'\micro\volt', exp='e-12')
        tab.add_hrule()
        tab.add_si_row(r'k_{\t{B, simple, large}}', k * 1e23, r'\joule\per\kelvin', exp='e-23')
        tab.add_si_row(r'k_{\t{B, lit}}', k0 * 1e23, r'\joule\per\kelvin', exp='e-23')
    else:
        tab.add_si_row(r'a_{\t{correlator, large}}', a * 1e15, r'\volt\per\ohm', exp='e-15')
        tab.add_si_row(r'b_{\t{correlator, large}}', b * 1e12, r'\micro\volt', exp='e-12')
        tab.add_hrule()
        tab.add_si_row(r'k_{\t{B, correlator, large}}', k * 1e23, r'\joule\per\kelvin', exp='e-23')
        tab.add_si_row(r'k_{\t{B, lit}}', k0 * 1e23, r'\joule\per\kelvin', exp='e-23')
    tab.savetable('build/table/k-res-' + circuit + '-large.tex')

    fig, ax = plt.subplots(1, 1)
    ax.plot(R * 1e-3, Ut * 1e12, 'rx', zorder=4)
    ax.plot(Rbad * 1e-3, Ubad * 1e12, 'gx', zorder=4)
    x = np.linspace(*xlim)
    ax.plot(x, lt.noms(a * x * 1e3 + b) * 1e12, 'b-')
    ax.set_xlim(*xlim)
    ax.set_ylim(*ylim)
    ax.set_xlabel(r'$\silabel{R}{\kilo\ohm}$')
    ax.set_ylabel(r'$\silabel{\tilde{U}}{\pico\volt}$')
    fig.savefig('build/graphic/k-' + circuit + '-large.pdf')
