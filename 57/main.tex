\input{header/report.tex}

\SetExperimentNumber{57}

\begin{document}
  \selectlanguage{english}
  \maketitlepage{Electrical Noise}{24.02.2014}{14.10.2014}
  \section{Abstract}
    In this experiment electrical noise phenomena are investigated and exploited to determine the Boltzmann constant and the elementary electrical charge.
    Furthermore, reciprocal frequency noise in two types of diodes is examined and their frequency spectrum is discussed.
  \section{Theory}
    Transport and surface-emission of electrons in solids are statistical processes which result from the electrons’ disordered movement in conductors and the discontinuity of electric charge.
    However, this statistical character, which is hard to prove for a large number of electrons, is only visible using instruments with sufficient resolution.

    Three types of noise are investigated here: thermal noise in resistors and noise occuring during emission of electrons by anodes; the latter can be subdivided into two phenomena named shot-effect and flicker-effect.
    Furthermore, current-noise exists in conductors of small volume exposed to large current density and in semi-conductors.
    Noise in semiconductors follows from the small lifetime of excitons compared to the time the electrons and holes need to reach the contacts attached to the sample.

    As current is proportional to voltage, a restriction to voltage suffices for finding a sensible quantity to measure the noise.
    \subsection{Statistical Properties of Noise}
      The time dependence of statistical processes usually contains no physical significance, whereas mean values provide a better way to describe such systems.

      Dealing with voltage-noise, the mean value
      \begin{eqns}
        \mean{U} &\equiv& \lim_{τ \rightarrow \infty} \int_0^{τ} \dif{t} \f{U}(t)
        \itext{vanishes, while}
        \mean{U²} &\equiv& \int_0^{τ} \dif{t} \f{U²}(t) \label{eqn:square-mean}
      \end{eqns}
      can be measured.
      If this fluctuation is independent of variation of the integration interval, it is called stationary.
      Moreover, the ensemble average
      \begin{eqn}
        \f{\avg{U²}}(t_0) \equiv \frac{1}{N} \sum_{i = 1}^{N} \f{U_i^2}(t_0)
      \end{eqn}
      can be defined.
      If this relation is stationary (independent of $t_0$) and equals equation \eqref{eqn:square-mean}, the fluctuation is called ergodic.

      The thermal noise of a resistor and the shot-effect are stationary and ergodic if temperature and resistance as well as the anode current remain constant.
    \subsection{Thermal Noise}
      Thermal noise appears in resistors, for instance.
      The time dependence of the voltage tapped there originates from the thermal movement of the electrons.
      Due to a changing center of charge, a fluctuating potential is induced, which can be measured or transformed into a noisy sound using a loudspeaker.
      Against this background, hereafter a relation between voltage $\f{U}(t)$ and temperature $T$ shall be derived.

      A set of two short-circuited conducting wires of length $L$ constitutes a system which allows oscillations fulfilling
      \begin{eqn}
        L = n \frac{λ}{2} \., \quad n \in \setN \., \quad n > 1\label{eqn:L}
      \end{eqn}
      with a wavelength $λ$.
      Using the relation $v = λ ν$ between phase velocity $v$, frequency $ν$ and wavelength $λ$, equation \eqref{eqn:L} can be written as a difference
      \begin{eqn}
        \Del{n} = \frac{2 L}{v} \Del{ν} \label{eqn:deln}
      \end{eqn}
      between successive oscillation modes.
      For $L \gg λ$ this spectrum becomes continuous.
      In thermal equilibrium, the mean energy of every oscillation mode is, according to the equipartition law, given by
      \begin{eqn}
        \mean{E} = \frac{h ν}{\E^{h ν β} - 1} \., \label{eqn:meanE}
      \end{eqn}
      where $h$ ist the Planck constant, $β = k_{\t{B}} / T$ and $k_{\t{B}}$ is the Boltzmann constant.
      As a consequence, the product of equations \eqref{eqn:deln} and \eqref{eqn:meanE} yields:
      \begin{eqn}
        \Del{n} \mean{E} = \frac{2 L}{ν} \frac{h ν}{\E^{h ν β} - 1} \Del{ν} \.. \label{eqn:nE}
      \end{eqn}
      One effect of the short-circuit is the occurence of standing waves, regarded as a superposition of two contra-running waves.
      For this reason, for obtaining the power $P$ per direction and frequency interval $\Del{ν}$, equation \eqref{eqn:nE} has to be divided by two and the time $t = \frac{L}{ν}$ the energy needs to travel through a conductor of length $L$ with frequency $ν$:
      \begin{eqn}
        P = \frac{h ν}{\E^{h ν β} -1} \Del{ν} \.. \label{eqn:P}
      \end{eqn}

      When the set of conducting wires is connected by two resistors, there is no short-circuit anymore.
      However, electric energy is converted into thermal energy.
      An oscillation in such a circuit can only take place given the fact that the resistor generates a noise-voltage, just as large as to compensate the dissipated thermal energy in temporal average.
      The compensation of thermal energy by the noise source can be quantitatively expressed by the averaged power
      \begin{eqn}
        \mean{N} = Z \mean{I²} = \mean{U²} \frac{Z}{\g(R + Z)²}
      \end{eqn}
      going back into the system.
      In this equation, $I$ is the noise-current, $Z$ is the wave impendance and $R$ is the resistance.
      The power is maximal for $R = Z$, so that
      \begin{eqn}
        \mean{N}_{\t{max}} = \frac{\mean{U²}}{4 R}\.. \label{eqn:N_max}
      \end{eqn}
      In order to gain a relation with temperature, equation \eqref{eqn:N_max} can be compared to equation \eqref{eqn:P}, resulting in
      \begin{eqn}
        \f{\mean{U²}}(ν) = 4 R \frac{h ν}{\exp.\frac{h ν}{k_\t{B} T}. - 1} \Del{ν} \.. \label{eqn:thermal-voltage}
      \end{eqn}
      Considering room temperature and experimental frequencies only reaching up to \SI{1}{\tera\hertz}, the exponential function in equation \eqref{eqn:thermal-voltage} can be expanded in a Taylor series so that the equation simplifies to
      \begin{eqn}
        \mean{U²} = 4 k_{\t{B}} T R \Del{ν} \., \label{eqn:nyquist-voltage}
      \end{eqn}
      the Nyquist relation.

      Since this relation is not dependent on frequency $ν$ but on the width of the frequency band $\Del{ν}$, determined by the measuring apparatus, this is called the \enquote{white noise} of a resistor.

      Under real conditions, the inherent capacity of resistors cannot be neglected, so that, effectively,
      \begin{eqn}
        \mean{U²_{\t{RC}}} = \frac{4 k_{\t{B}} T R \Del{ν}}{1 + \g(2 \PI ν RC)²} \..
      \end{eqn}
      is measured, as defined in the equivalent circuit diagram \ref{fig:resistor_circuit} of a resistor.
      \begin{figure}
        \includegraphics{graphic/resistor-circuit.pdf}
        \caption{Equivalent circuit diagram of a resistor \cite{anleitung57}.}
        \label{fig:resistor_circuit}
      \end{figure}
      Due to this effect, the measured voltage is smaller than the Nyquist voltage \eqref{eqn:nyquist-voltage}.
    \subsection{Current Noise}
      The main phenomena of current noise are the shot- and flicker-effects.
      Basically, they describe fluctuations in the anode current of diodes, induced by the quantization of electric charge or the time dependence of the work function.
      Both effects differ in their frequency and current dependency.
      \subsubsection{Shot-Effect}
        For studying the shot-effect, diodes are best operated in their saturation region (all electrons emitted at the cathode reach the anode).
        This current can be subdivided into a direct current $I_0$ and an alternating current $\f{I}(t)$, which represents the noise.
        As mentionend above, the mean of the noise vanishes, whereas the direct current is determined by the mean number of electrons reaching the anode per time unit.
        In the saturation region several assumptions can be made:
        Emission and movement of the electrons in the diode do not depend on neighbouring electrons.
        Furthermore, electrons cover the same distance in equal times.
        Finally, no secondary electrons are created at the anode and electrons do not possess kinetic energy after emission, which is only approximately fulfilled for small frequencies.
        If the diode is operated outside the saturation region, noise effects also come from spacial charge densities within the diode rather than only from emission by the cathode.

        Electrostatically, one electron induces, on its way from the cathode to the anode, a current
        \begin{eqn}
          \f{i_n}(t) = e \f{f}(t - t_n) \.,
        \end{eqn}
        in which $e$ is the elementary charge and $f$ describes the character of the current pulse that is equal for every electron $n$.
        Every electron carries an elementary charge.
        Furthermore, the function $f$ only makes a contribution for the time interval $τ$, moreover,
        \begin{eqn}
          \int_{0}^{τ} \dif{θ} \f{f}(θ) = 1
        \end{eqn}
        holds.
        This leads to
        \begin{eqn}
          e = \int_{t_n}^{t_n + τ} \dif{t} \f{i_n}(t) \..
        \end{eqn}
        In this equation, $t_n$ is the starting time of the time interval $τ$.
        By using the Campbell theorem it can be shown that
        \begin{eqn}
          \f{\mean{I²}}(t) = e I_0 \int_{0}^{τ} \dif{θ} \f{f²}(θ) \.. \label{eqn:meanIsqf}
        \end{eqn}

        The frequency spectrum of shot noise $W_{\t{sh}}$ is defined by
        \begin{eqn}
          \f{\mean{I²}}(t) = \int_{0}^{\infty} \dif{ν} \f{W_{\t{sh}}}(ν) \.. \label{eqn:Wdef}
        \end{eqn}
        Using the Parseval identity and considering that only positive frequencies make sense and that $f$ only contributes for $0 < t < τ$, the relation
        \begin{eqn}
          \int_{0}^{τ} \dif{t} \f{f²}(t) = 2 \int_0^{\infty} \dif{ν} \abs{\f{F}(ν)}²
        \end{eqn}
        between $f$ and its Fourier transform $F$ follows.
        By using this and equation \eqref{eqn:meanIsqf},
        \begin{eqn}
          \mean{I²} = 2 e I_0 \int_{0}^{\infty} \dif{ν} \abs{\f{F}(ν)}²
        \end{eqn}
        can be obtained.
        Moreover, by using equation \eqref{eqn:Wdef},
        \begin{eqn}
          \f{W_{\t{sh}}}(ν) = 2 e I_0 \abs{\f{F}(ν)}^2
        \end{eqn}
        follows.
        For small frequencies
        \begin{eqn}
          ν \ll \frac{1}{2 \PI τ}
        \end{eqn}
        the Fourier transform of $f$ evaluates to $\f{F}(ν) \approx 1$, so that
        \begin{eqn}
          \f{W_{\t{sh}}}(ν) = 2 e I_0 \.. \label{eqn:W_sh}
        \end{eqn}
        Thus, for small frequencies up to \SI{100}{\mega\hertz}, the shot-effect is frequency-independent.
        Inserted into equation \eqref{eqn:Wdef}, this yields
        \begin{eqn}
          \f{\mean{I²}}(t) = \int_{ν_{\t{l}}}^{ν_{\t{u}}} \dif{ν} 2 e I_0 = 2 e I_0 \Del{ν}
        \end{eqn}
        with an upper and a lower frequency boundary $ν_{\t{u, l}}$ of the frequency band and their difference $\Del{ν}$. 
        This equation is the Schottky relation.
      \subsubsection{Reciprocal Frequency Noise}
        The frequency spectrum of many phenomena even of non-physical fashion shows a reciprocal frequency behavior $\f{W}(ν) \propto 1 / ν$.
        For oxide diodes there is a theory ascribing this to generation and recombination of excitons.
        According to this theory, the frequency spectrum is given by
        \begin{eqn}
          \f{W_τ}(ν) = c \frac{τ}{1 + \g(2 \PI ν τ)²} \.,
        \end{eqn}
        where $τ$ is the relaxation time for the generation–recombination effect and $c$ a constant.
        This spectrum is nearly constant for $ν \ll 1 / τ$, whereas for $ν \approx 1 / τ$, $W_τ$ decreases as shown by the dashed lines in Figure \ref{fig:1/f}.
        \begin{figure}
          \includegraphics{graphic/noise-spectrum.pdf}
          \caption{Frequency spectrum of the shot-effect \cite{anleitung57}.}
          \label{fig:1/f}
        \end{figure}
        In the interval $τ_{\t{min}} < τ < τ_{\t{max}}$, $τ$ is dependent on an activation energy $E_{\t{min}} < E < E_{\t{max}}$:
        \begin{eqn}
          \f{τ}(E) = τ_0 \exp.\frac{E}{k_{\t{B}} T}. \..
        \end{eqn}
        Thus, full frequency spectrum can be obtained by integration
        \begin{eqn}
          \f{W}(ν) = \int_{E_{\t{min}}}^{E_{\t{max}}} \dif{E} \frac{c \f{τ}(E)}{1 + \g(2 \PI ν \f{τ}(E))²} = \frac{c k_{\t{B}} T}{ν} \g(\arctan.2\PI ν τ_{\t{min}}. - \arctan.2 \PI ν τ_{\t{max}}.)
        \end{eqn}
        This function is plotted as the solid line.
        Basically, there are three significant sections: a frequency-independent section, one which is proportional to $1 / ν$ and one which is proportional to $1 / ν²$ (they yield considering corresponding approximations).
      \subsubsection{Flicker Effect}
        The flicker effect is one example for reciprocal frequency noise, occuring in diodes with oxide cathode.
        Multiplied by the white Schottky noise, its frequency spectrum is given by
        \begin{eqn}
          \f{W_{\t{F}}}(ν) = \frac{c I_0^2}{ν^α}
        \end{eqn}
        with $α \approx 1$.
        Ordered ascending with respect to time scale, substantial mutation, diffusion processes causing discrete local change in resistance and local fluctuations of the work function induced by foreign atoms, contribute to the flicker effect.
  \section{Experimental Setup}
    The Experimental setup for the resistor measurement is illustrated in Figure \ref{fig:noise-spectro}.
    \begin{figure}
      \includegraphics[width=\textwidth]{graphic/circuit-simple.pdf}
      \caption{Simple amplifier circuit \cite{anleitung57}.}
      \label{fig:noise-spectro}
    \end{figure}
    The resistor is connected to a preamplifier amplifying the signal in a linear fashion with amplification rate $V_{\t{V}}$.
    After that, the signal runs through a band pass filter, filtering signals with frequencies in $\Del{ν}$, passing by a postamplifier with amplification rate $V_{\t{N}}$, to end up in a device which squares the voltage.
    The temporal mean value is measured by filtering the direct current part using a low-pass filter and an amplifier amplifying only the direct current with amplification rate $V_=$.
    Thus, the voltage at the end amounts to
    \begin{eqn}
      U_{\t{a}}^2 = V_{\t{V}}² V_{\t{N}}² V_= \Del{ν} 4 k_{\t{B}} T R = Ξ 4 k_{\t{B}} T R \..
    \end{eqn}
    The constant $Ξ$ is apparatus-dependent, so that a calibration measurement has to be run.

    Noise voltage in modern resistors is very small, meaning that high amplification is necessary to measure it.
    One side-effect is noise in the amplifier caused by semi-conductor elements.
    Specially noise in the preamplifier is devastating for measurement, as it is amplified most (even more than the signal sometimes).
    A solution for this problem consists in a so-called correlator circuit which exploits the fact that noise of two independent amplifiers is uncorrelated; it is shown in Figure \ref{fig:cor}.
    \begin{figure}
      \includegraphics[width=\textwidth]{graphic/circuit-correlator.pdf}
      \caption{Correlator circuit \cite{anleitung57}.}
      \label{fig:cor}
    \end{figure}
    The channels are multiplied and the signal is sent through a low-pass filter and a direct-current amplifier.
    One significant advantage of this circuit is that the outcoming voltage is independent of inner voltages of the amplifiers and only dependent on the resistor’s voltage.
    This leads to higher resolution.

    To quantify the quality of an amplifier with respect to its own noise, a noise number can be defined:
    \begin{eqn}
      \f{F}(ν_0, Z) \equiv \frac{\f{\mean{U_{\t{a}}²}}(Z)}{4 k_{\t{B}} T \Re(Z) \Del{ν} V_{\t{ges}}^2}\..
    \end{eqn}
    In this formula $U_{\t{a}}$ is the outgoing voltage, $Z$ the impendance of the resistor to be measured, $T$ the temperature, $V_{\t{ges}}$ the entire amplification and $ν_0$ the position of the frequency band $\Del{ν}$ inside the frequency spectrum.
    If the amplifier is optimal, $F$ is equal to one.

    For the shot-noise measurement the circuit in Figure \ref{fig:shot} is used.
    \begin{figure}
      \includegraphics{graphic/shot.pdf}
      \caption{Circuit for measuring shot noise \cite{anleitung57}.}
      \label{fig:shot}
    \end{figure}
  \section{Experimental Realisation}
      The first part of the experiment consists in determing the Boltzmann constant by examining the thermal noise of a resistor and evaluating the noise number $F$.
      For this measurement the circuit in Figure \ref{fig:noise-spectro} is used.
      The band-pass therein is set in such a way that frequencies from the grid or their harmonics do not disturb the measurement and it does not exceed upper frequency boundaries of other devices.
      Furthermore, the preamplifier, possessing an amplifcation of $V_{\t{V}} = 1000$  has to be set to AC-coupling.
      In order to determine the apparatus-dependent calibration value
      \begin{eqn}
        A \equiv \int \dif{ν} \f{V_{\t{V}}²}(ν) \f{V_{\t{N}}²}(ν) V_= \., \label{eqn:A}
      \end{eqn}
      a sine generator is connected instead of the resistor.
      As the amplitude of the generator is to large for the apparatus, a calibrated attenuator, which damps the signal by a factor 1000, has to be interconnected.
      Moreover, the amplification of the direct-current amplifier has to be set to $V_= = 10$, alternating current coupling and sensitive to a voltage of \SI{1}{\volt}.
      Fluctuations in the voltage $U_\t{a}$ can be damped by tuning the time-constant of the low-pass filter.
      Finally the heating period of the system has to be considered.
      For determing the Boltzmann constant, the resistor has to be plugged in again.
      Moreover, it has to be shielded sufficiently and not connected with too long a wire, as a wire also has a capacity of \SI{100}{\pico\farad\per\meter}.
      Due to zero-point drift, $U_{\t{a}}$ has to be larger than \SI{90}{\milli\volt}.
      For comparison, the same measurements are run with the circuit in Figure \ref{fig:cor}.
      Both channels have to be set to the same amplification and frequency response.
      The selective amplifiers have to be set to an amplification of $10$ at a $Q$-factor of $Q = 10$; its pass-band characteristics are given by
      \begin{eqn}
        U_{\t{a}} = \frac{1}{Q²} \frac{1}{\frac{ν²}{ν_0^2} + \frac{ν_0^2}{ν²} + \frac{1}{Q²} - 2} U_{\t{e}} \..
      \end{eqn}

      For the determination of the elementary electric charge, the circuit in Figure \ref{fig:shot} is used, which is already set up in a box.
      The current noise is converted into voltage noise by the resistance $R$ so that it can be connected to the simple circuit \ref{fig:noise-spectro}.
      The saturation region of the diode is determined by measuring the tube characteristics.

    For determination of the diodes’ frequency spectrums, the band-pass in the circuit shown in Figure \ref{fig:noise-spectro} is substituted by a selective amplifier.
    However, the problem arises that its pass-band $\Del{ν}$ is dependent on $ν_0$, which has to be taken into account later.
    For low frequencies, the grid frequency has to be considered as well as its odd multiples, as they can disturb the measurement, especially at \SI{50}{\hertz} and \SI{150}{\hertz}.
    Furthermore, a large time-constant has to be set for the low-pass filter.
    Finally, the anode current should not exceed \SI{4}{\milli\ampere}, as disturbing voltages increase proportionally to $I_0^2$.
    Measurements are done with a $Q$-factor set to $10$.

    A frequency spectrum is also determined for an oxide diode.
    This measurement should not be run in the saturation region, otherwise the diode is damaged.
    Low frequencies do not cause problems here, as the diode runs on a battery.
    Its current $I_0$ has to be constant during the measurement; small fluctuations can be suppressed with a resistor.
    For this diode, the spectrum has to be measured in the interval from \SIrange{0.01}{500}{\kilo\hertz}.
    For frequencies above \SI{100}{\kilo\hertz} the bandpass has to be used.
  \section{Data and Analysis}
    For each analysis the amplifications are removed using
    \begin{eqn}
      \tilde{U} = \frac{U}{V_{\t{V}}² V_{\t{N}}² V_=} \..
    \end{eqn}
    If uncertainties for the measured voltages are missing then it is due to forgetting to record them.
    \subsection{Calibration}
      \subsubsection{Data}
        Table \ref{tab:A-simp} contains the data of the calibration measurement for the simple circuit, used to determine $A$ (see Equation \eqref{eqn:A}); Table \ref{tab:A-cor} contains analogous data for the correlator circuit.
        The input amplitude is $U₀ = \SI{200}{\milli\volt}$.
        \begin{table}
          \caption{Calibration measurement for the simple circuit: frequency $ν$, amplification of the post-amplifier $V_{\t{N}}$ and output voltage $U$.}
          \label{tab:A-simp}
          \OverfullCenter{\input{table/A-simple.tex}}
        \end{table}
        \begin{table}
          \caption{Calibration measurement for the correlator circuit: frequency $ν$, amplification of the post-amplifier $V_{\t{N}}$ and output voltage $U$.}
          \label{tab:A-cor}
          \OverfullCenter{\input{table/A-correlator.tex}}
        \end{table}
      \subsubsection{Analysis}
        Table \ref{tab:A} contains the apparatus constants $A$, defined by Equation \eqref{eqn:A}, for the simple circuit in Figure \ref{fig:noise-spectro} and the correlator circuit in Figure \ref{fig:cor}.
        The signal is divided by the square of the effective value of the incoming signal and the damping factor of the attenuator, giving
        \begin{eqn}
          \tilde{\tilde{U}} = \tilde{U} \g(\frac{\sqrt{2} U₀}{\num{1000}})^{\!\! -2} \..
        \end{eqn}
        The constant is calculated by integrating the curve in Figure \ref{fig:A-simple} or Figure \ref{fig:A-correlator} with the Simpson method.
        The uncertainty is the difference between the integrals calculated using the Simpson und trapezoid methods.
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/A-simple.pdf}
          \caption{Calibration data for the simple circuit to determine the apparatus constant $A$, which is the area under the curve.}
          \label{fig:A-simple}
        \end{figure}
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/A-correlator.pdf}
          \caption{Calibration data for the correlator circuit to determine the apparatus constant $A$, which is the area under the curve.}
          \label{fig:A-correlator}
        \end{figure}
        \begin{table}
          \caption{Results of the numeric integration for the simple and the correlator circuit.}
          \input{table/A.tex}
          \label{tab:A}
        \end{table}
        Table \ref{tab:F} contains the noise figures for the simple and the correlator circuit.
        \begin{table}
          \caption{Noise figures for simple and correlator circuit.}
          \label{tab:F}
          \input{build/table/F.tex}
        \end{table}
      \FloatBarrier
    \subsection{Boltzmann Constant}
      \subsubsection{Data}
        The measurement to determine the Boltzmann constant is executed using the simple and the correlator circuits and two different potentiometers to cover small and large resistances.
        The four resulting tables are \ref{tab:k-simp-small}, \ref{tab:k-corr-small}, \ref{tab:k-simp-large} and \ref{tab:k-corr-large}.
        $U$ is the measured output voltage, $V_{\t{N}}$ the amplification of the post-amplifier and $R$ the tunable resistance of the potentiometer.
        \begin{table}
          \caption{Constant data and apparatus settings.}
          \label{tab:k-const}
          \input{table/k-const.tex}
        \end{table}
        \begin{table}
          \caption{Data for the simple ciruit for small resistances.}
          \label{tab:k-simp-small}
          \input{table/k-simple-small.tex}
        \end{table}
        \begin{table}
          \caption{Data for the correlator ciruit for small resistances.}
          \label{tab:k-corr-small}
          \input{table/k-correlator-small.tex}
        \end{table}
        \begin{table}
          \caption{Data for the simple ciruit for large resistances.}
          \label{tab:k-simp-large}
          \input{table/k-simple-large.tex}
        \end{table}
        \begin{table}
          \caption{Data for the correlator ciruit for large resistances.}
          \label{tab:k-corr-large}
          \input{table/k-correlator-large.tex}
        \end{table}
        Table \ref{tab:k-const} contains the constant measurement data.
        $T$ is the assumed room temperature, $τ$ the time-constant of the low-pass filter.
        The variable $V_{\t{sa}}$ is the amplification and $Q$ the $Q$-factor of the selective amplifier used in the correlator circuit.
      \subsubsection{Analysis}
        Figures \ref{fig:k-simple-small}, \ref{fig:k-correlator-small}, \ref{fig:k-simple-large} and \ref{fig:k-correlator-large} show the measurement data contained by Tables \ref{tab:k-simp-small}, \ref{tab:k-corr-small}, \ref{tab:k-simp-large} and \ref{tab:k-corr-large}.
        Furthermore, the linear fits
        \begin{eqn}
          \f{U}(R) = a R + b
        \end{eqn}
        are shown there for the four circuit configurations arising by using the simple and correlator circuit with two potentiometers covering small and large resistances.
        The results of the fits and the Boltzmann constants
        \begin{eqn}
          k_{\t{B}} = \frac{a}{4 T A}
        \end{eqn}
        are shown in Tables \ref{tab:k-res-simple-small} to \ref{tab:k-res-correlator-large}; they can be compared to a literature value for the Boltzmann constant \cite{scipy}.
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/k-simple-small.pdf}
          \caption{Linear fit of the data for the simple circuit with small resistance.}
          \label{fig:k-simple-small}
        \end{figure}
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/k-correlator-small.pdf}
          \caption{Linear fit of the data for the correlator circuit with small resistance.}
          \label{fig:k-correlator-small}
        \end{figure}
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/k-simple-large.pdf}
          \caption{Linear fit of the data for the simple circuit with large resistance. Green data points are excluded from the linear fit.}
          \label{fig:k-simple-large}
        \end{figure}
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/k-correlator-large.pdf}
          \caption{Linear fit of the data for the correlator circuit with large resistance. }
          \label{fig:k-correlator-large}
        \end{figure}
        \begin{table}
          \caption{Linear-fit parameters and determined Boltzmann constants (for sake of comparison a literature value \cite{scipy} is also given) of the simple circuit with potentiometer covering small resistances.}
          \label{tab:k-res-simple-small}
          \input{table/k-res-simple-small.tex}
        \end{table}
        \begin{table}
          \caption{Linear-fit parameters and determined Boltzmann constants (for sake of comparison a literature value \cite{scipy} is also given) of the correlator circuit with potentiometer covering small resistances.}
          \label{tab:k-res-correlator-small}
          \input{table/k-res-correlator-small.tex}
        \end{table}
        \begin{table}
          \caption{Linear-fit parameters and determined Boltzmann constants (for sake of comparison a literature value \cite{scipy} is also given) of the simple circuit with potentiometer covering large resistances.}
          \label{tab:k-res-simple-large}
          \input{table/k-res-simple-large.tex}
        \end{table}
        \begin{table}
          \caption{Linear-fit parameters and determined Boltzmann constants (for sake of comparison a literature value \cite{scipy} is also given) of the simple circuit with potentiometer covering large resistances.}
          \label{tab:k-res-correlator-large}
          \input{table/k-res-correlator-large.tex}
        \end{table}
      \FloatBarrier
    \subsection{Elementary Charge}
      \subsubsection{Data}
        Table \ref{tab:kenn} contains measurement data in order to determine the characteristic curves of the high-vacuum diode operated at various anode currents $I_0$.
        In this table, $U_{\t{a}}$ is the anode voltage, $V_{\t{N}}$ the amplification of the post-amplifer and $U$ the output voltage of the simple measurement circuit.
        Table \ref{tab:e-measure} contains the measurement data for the current–voltage characteristics of the high-vacuum diode.
        The pass-frequency of the band-pass filter, the resistor’s value $R$ and the anode voltage $U_\t{a}$ can be found in Table \ref{tab:e-const}.
        \begin{table}
          \caption{Characteristic curves of the high-vacuum diode for various anode currents $I_0$.}
          \label{tab:kenn}
          \OverfullCenter{\input{table/kenn.tex}}
        \end{table}
        \begin{table}
          \caption{Current–voltage characteristics of the high-vacuum diode. $I$ is the diode current, $V$ the amplification and $U$ the output voltage.}
          \label{tab:e-measure}
          \OverfullCenter{\input{table/e-IUcharac.tex}}
        \end{table}
        \begin{table}
          \caption{Constant parameters. The variable $ν$ is the pass frequency of the band pass, $R$ the resistance of the diode and $U_\t{a}$ the anode voltage.}
          \label{tab:e-const}
          \input{table/e-const.tex}
        \end{table}
      \subsubsection{Analysis}
        Figure \ref{fig:k-kenn} contains the characteristic curves for various anode currents to determine the saturation region of the diode.
        A linear fit
        \begin{eqn}
          \f{U}(I) = a I + b
        \end{eqn}
        of the data in Table \ref{tab:e-measure} leads to the parameters $a$ and $b$, which can be found in Table \ref{tab:e-res}.
        Finally, the value of the elementary charge
        \begin{eqn}
          e = \frac{a}{2 A R²}
        \end{eqn}
        is compared to a literature value in this table.
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/kenn.pdf}
          \caption{Characteristic curves for various anode currents to determine saturation region.}
          \label{fig:k-kenn}
        \end{figure}
        \begin{table}
          \caption{Results of the elementary charge determination. Literature value from \cite{scipy}.}
          \label{tab:e-res}
          \input{table/e-result.tex}
        \end{table}
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/e.pdf}
          \caption{Plotted measurement data and linear fit to determine the value of elementary charge.}
          \label{fig:e}
        \end{figure}
      \FloatBarrier
    \subsection{Frequency Spectrums}
      \subsubsection{Data}
        Table \ref{tab:rein} contains all the measurement data concerning the frequency spectrum of the pure-metal diode; analogous data for the oxide diode can be found in Table \ref{tab:oxyd}.
        In these tables, $ν$ is the amplification frequency of the selective amplifier, $τ$ is the time-constant of the low-pass filter and $\Del{ν}$ the pass-band of the selective amplifier, which depends on $ν$.
        Spectrum measurements are done with an anode current of \SI{2.5}{\milli\ampere} for the pure-metal diode and \SI{1.6}{\milli\ampere} for the oxide diode.
        The error $\err{U}$ is estimated.
        An amplification factor of $10²$ from the selective amplifier must also be removed.
        \begin{table}
          \caption{Data regarding the frequency spectrum of the pure-metal diode.}
          \label{tab:rein}
          \OverfullCenter{\input{table/rein.tex}}
        \end{table}
        \begin{table}
          \caption{Data regarding the frequency spectrum of the oxide diode.}
          \label{tab:oxyd}
          \OverfullCenter{\input{table/oxyd.tex}}
        \end{table}
      \subsubsection{Analysis}
        Figure \ref{fig:freq-diode} shows the frequency spectrum for the pure-metal diode, Figure \ref{fig:freq-oxide} shows the frequency spectrum for the oxide diode.
        The frequency spectrum of the oxide diode includes contributions from both the shot und flicker effects.
        The exponent $α$ of the flicker effect is extracted using the linear regression
        \begin{eqn}
          \ln.W. = α \ln.ν. + β \..
        \end{eqn}
        The results are shown in Figure \ref{fig:freq-oxide} and Table \ref{tab:oxyd-result}.
        The results for the shot effect $W_{\t{sh}}$, extracted by fitting the shown data to a constant, are in Tables \ref{tab:rein-result} and \ref{tab:oxyd-result}.
        The theoretical value for $W_{\t{sh}}$ is given by equation \eqref{eqn:W_sh}.
        The attenuation factor for the oxyde diode is defined by
        \begin{eqn}
          Γ² = \frac{W_{\t{sh}, \t{th}}}{W_{\t{sh}}}
        \end{eqn}
        and included in Table \ref{tab:oxyd-result}.
        \begin{figure}
          \includegraphics{graphic/rein.pdf}
          \caption{Frequency spectrum for the pure-metal diode.}
          \label{fig:freq-diode}
        \end{figure}
        \begin{figure}
          \includegraphics{graphic/oxyd.pdf}
          \caption{Frequency spectrum for the oxide diode. Red and black points are included in the fits.}
          \label{fig:freq-oxide}
        \end{figure}
        \begin{table}
          \caption{Fit results for the pure-metal diode.}
          \label{tab:rein-result}
          \input{table/rein-result.tex}
        \end{table}
        \begin{table}
          \caption{Fit results for the oxide diode.}
          \label{tab:oxyd-result}
          \input{table/oxyd-result.tex}
        \end{table}
  \section{Discussion}
    The noise figures $F$ show that the correlator circuit produces less noise, leading to higher resolution.
    The results for the Boltzmann constant $k_{\t{B}}$ differ from the literature by less than a factor of 2.
    The result for the elementary charge $e$ differs from the literature by an order of magnitude.
    The exponent $α$ for the flicker effect is consistent with the expectation of $α \approx 1$.
    The results for the shot effect agree in the order of magnitude.
  \makebibliography
\end{document}
