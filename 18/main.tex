\input{header/report.tex}

\SetExperimentNumber{18}

\begin{document}
  \selectlanguage{english}
  \maketitlepage{The Germanium Detector}{28.10.2013}{25.11.2013}
  \section{Introduction}
    The aim of this experiment is to use a germanium detector to identify \Pphoton-emitting nuclei in a sample.
  \section{Theory}
    For gamma spectroscopy, the most important interactions of photons with matter are the photoelectric effect, Compton scattering and pair production.
    Approximately, all these interactions have in common that the photon intensity $N$, i.e.\ the number of photons crossing matter per time and area, decays exponentially:
    \begin{eqn}
      \f{N}(D) = N_0 \g(1 - \E^{-n σ D}) = N_0 \g(1 - \E^{-μ D}) \.,
    \end{eqn}
    where $N_0$ is the initial intensity, $n$ the electron density, $σ$ the cross section, $D$ the thickness of the material and $μ$ the molar absorptivity, the inverse of which is the mean range of a photon in matter.
    \subsection{The Photoelectric Effect}
      The photoelectric effect is the absorption of a photon by a shell electron, usually one in the K shell due to conservation of momentum.
      It is expressed by
      \begin{eqn}
        \HepProcess{\Pphoton + \t{atom} \to \t{atom}^{+} + \Pelectron} \..
      \end{eqn}
      Due to the interaction, the electron leaves the shell and becomes a free electron.
      Electrons from outer shells refill this hole, emitting characteristic X-ray photons or smashing out so-called Auger electrons.
      The process only takes place if the energy of the photon is at least the binding energy of the electron involved in the interaction.
      Neither the X-ray photons nor the electrons are normally able to leave the absorber material, which explains why all the energy remains in the material.

      The cross section of the photoelectric effect is dependent on the energy $E$ of the incoming photon and the atomic number $z$:
      \begin{eqn}
        σ_{\t{pe}} \propto z^α E^δ \.,
      \end{eqn}
      with $4 < α < 5$ and $δ \approx -3.5$ ($δ \approx -1$ for $E > \SI{5}{\mega\electronvolt}$).
      The cross section decreases for higher photon energies and is discontinuous at the binding energies of the shells.
    \subsection{The Compton Effect}
      The compton effect is the scattering of a photon on an electron:
      \begin{eqn}
        \HepProcess{\Pelectron + \Pphoton \to \Pelectron' + \Pphoton'} \..
      \end{eqn}
      Energy–momentum conservation
      \begin{eqn}
        \tn{p}_{\Pphoton} + \tn{p}_{\Pe} = \tn{p}_{\Pphoton}' + \tn{p}_{\Pe}'
      \end{eqn}
      leads to
      \begin{eqn}
        E'_{\Pphoton} = E_{\Pphoton} \frac{1}{1 + ε \g(1 - \cos.θ.)} \., \quad ε \equiv \frac{E_{\Pphoton}}{m_{\Pe} c²} \., \label{eqn:ε}
      \end{eqn}
      where $m_{\Pe}$ is the electron mass and $c$ ist the speed of light.
      The variable $E_{\Pphoton}$ and $E'_{\Pphoton}$ are the energy of the photon before and after the interaction, respectively.
      The scattering angle is given by $θ$.
      The minimum of $E'_{\Pphoton}$ is at $θ = \PI$, where the most energy is transferred to the electron.
      Due to energy–momentum conservation, the photon can never be completely absorbed by the electron, meaning that only part of the photons energy can be detected.

      The total cross section for the Compton effect is
      \begin{eqn}
        σ_{\t{Co}} = \frac{3}{4} σ_{\t{Th}} \g(\frac{1 + ε}{ε²} \g(\frac{2 + 2ε}{1 + 2ε} - \frac{1}{ε} \ln(1 + 2ε)) + \frac{1}{2ε} \ln(1 + 2ε) - \frac{1 + 3ε}{\g(1 + 2ε)²}) \.,
      \end{eqn}
      with the Thomson cross section
      \begin{eqn}
        σ_{\t{Th}} = \frac{8}{3} \PI \g(\frac{e}{4 \PI ε_0 c² m_{\Pe}})^{\!\! 2} \equiv \frac{8}{3} \PI r_{\Pe}² \.,
      \end{eqn}
      where $e$ is the elementary charge, $ε_0$ is the vacuum permittivity and $r_{\Pe}$ is the classical electron radius.
      The cross section decreases monotonically with increasing energy.
      For small energies the cross section approaches the Thomson cross section, which describes elastic scattering.
    \subsection{Pair Production}
      Pair production means that a photon, with the aid of a collision partner, produces an electron–positron pair.
      The collision partner is either an atom or an electron, which leads to the processes
      \begin{eqns}[rCl'l]
      \HepProcess{\Pphoton + \t{atom}   &\to& \APelectron + \Pelectron + \t{atom}}   \., & E_{\Pphoton} > 2 m_{\Pe} c² \.; \IEEEyesnumber \IEEEyessubnumber* \\
        \HepProcess{\Pphoton + \Pelectron &\to& \APelectron + \Pelectron + \Pelectron} \., & E_{\Pphoton} > 4 m_{\Pe} c² \..
      \end{eqns}
      The remaining energy is statistically distributed to the kinetic energies of the produced electron and positron.

      For the cross section, the location of the interaction must be taken into account.
      If the interaction takes place close to the nucleus, the whole electric field of the nucleus is felt by the interaction, leading to
      \begin{eqn}
        σ_{\t{pp}} = α r_{\Pe}² z² \g(\frac{28}{9} \ln.2ε. - \frac{218}{27})
      \end{eqn}
      for $20 < ε < 50$, with the fine-structure constant $α$.
      For pair production outside the electron shells where the nucleus’ field is shielded, the cross section becomes independent of energy:
      \begin{eqn}
        σ_{\t{pp}} = α r_{\Pe}² z² \g(\frac{28}{9} \ln.\frac{183}{\sqrt[3]{z}} - \frac{2}{27}.)
      \end{eqn}
      for energies over \SI{500}{\mega\electronvolt}.

      The energy of the incoming photon is only measured correctly if the electron–positron pair is completely absorbed by the detector.
      As there is only a probability for the absorption of an electron and a positron and for their bremsstrahlung, the results of spectroscopy do not reflect the proper photon energy and the energy lines become broader.
    \subsection{Functionality of a Germanium Detector}
      The germanium detector is a semiconductor detector which provides a better resolution than common scintillation detectors.
      Basically, it is a diode with p-doped and and n-doped material connected to each other, inducing a depletion layer without free electrons as the charge carriers diffuse to the respective layer and recombine again at finite temperature.
      The remaining donors and acceptors lead to an electric field which stops the diffusion of the charge carriers.
      This is visualized in Figure~\ref{fig:depletion_zone}.
      \begin{figure}
        \includegraphics[width=\textwidth]{build/graphic/depletion-zone.pdf}
        \caption{Schematic illustration of the depletion zone in a Germanium detector \cite{anleitung18}.}
        \label{fig:depletion_zone}
      \end{figure}

      If the incoming photon produces a highly energetic electron from atomic shells or the valence band of the crystal lattice through the effects mentioned earlier, it will produce excitions which lift electrons from the valence band into the conduction band (an energy transfer up to \SI{20}{\kilo\electronvolt} is possible).
      As a consequence, exciton electrons interact with further valence electrons to create more excitons.
      Part of the exciton electron’s energy is also used to produce phonons.
      In this way, a high density of excitons is formed along the track of the primary electron.
      In the ideal case, all the excitons are seperated to the respective electrodes to get a burst proportional to the photon energy.

      As the interaction is to take place in the depletion zone to detect the proper energy of the incoming photon, the zone has to be as large as possible.
      Furthermore, the potential difference between the opposite layers should be large enough to support the seperation.
      One method to achieve these goals consists of the application of an additional voltage to enlarge the potential gap between the p-doped and n-doped zones.
      Another method is to dope the layers asymetrically, which causes the depletion zone to grow.
      These methods have bounds because there are also thermal electrons with density
      \begin{eqn}
        n_{\t{i}}² = 4 \g(\frac{2 \PI k}{h²})^{\! 3} \g(m_{\t{h}} m_{\Pe})^{\! \frac{3}{2}} T³ \exp.\frac{-E_\t{g}}{k_\t{B} T}. \., \label{eqn:backscatter-compton}
      \end{eqn}
      where $k$ is the wave vector, $h$ is the Planck constant, $m_{\t{h}}$ is the effective hole mass, $T$ is the temperature and $E_\t{g}$ is the gap energy.
      The thermal electrons are accelerated by the voltage and lead to noise.
      Therefore, the detector is cooled to the boiling point of nitrogen (\SI{77}{\kelvin}).
    \subsection{Properties of a Semiconductor Detector}
      An essential parameter for gamma spectroscopy is the energy resolution.
      A useful quantity to describe it is the half width $\Del{E_{1/2}}$ of the pulse amplitude distribution; pulses which differ more than $\Del{E_{1/2}}$ can be distinguished.
      This width is determined by the number of excitons and phonons, which are statistically excited by the photon.
      Since there exists a correlation between the excitation of exitons and phonons, a so-called Fano factor of $0.1$ for germanium has to be introduced to rescale the standard deviation of the normal distribution, so that
      \begin{eqn}
        \Del{E_{1/2}} \approx 2.35 \sqrt{0.1 \, E_{\Pphoton} E_{\Pe}} \., \label{eqn:fano}
      \end{eqn}
      where $E_{\Pphoton}$ is the energy of the photon and $E_{\Pe}$ is the energy of the electron.
      Typical photon-energies of \SI{500}{\kilo\electronvolt} lead to a resolution of \SI{895}{\electronvolt}, which is much better than that of scintillation detectors or Geiger counters.
      Further effects which affect the resolution uncorrelatedly are the noise of the leakage current induced by the intrinsic conduction, remaining impurities in the crystal and the high tension as well as the noise of the amplifier and field inhomogeneties caused by incomplete charge collection.
      As the quantities are uncorrelated, the half widths of the respective distributions contribute qaudratically to the total half width, so that
      \begin{eqn}
        H_{\t{total}}² = \Del{E_{1/2}²} + H_{\t{L}}² + H_{\t{I}}² + H_{\t{AN}}² \..
      \end{eqn}
      These effects can be minimized through variation of the applied voltages and cooling of the detector and amplifier.
      A second important parameter is the efficiency, namely the dependence on the detection probability on the photon energy.
    \subsection{Germanium Detector Spectrum of a Monochromatic \texorpdfstring{\Pphoton}{γ}-Emitter}
      A spectrum of a monochromatic emitter is more than just a line at a certain energy.
      Due to the effects mentioned earlier, it consists of the photo peak and the Compton continuum containing a backscatter peak at $E_{\t{BS}}$ and ending sharply at the Compton edge $E_{\t{CE}}$, the energy corresponding to the maximum scattering angle.
      The backscattering peak is caused by photons which do not reach the detector directly but are scattered by the surrounding of the detector first.
      The locations of these points of interest are given by
      \begin{eqns}
        E_{\t{CE}} &=& E_{\Pphoton} \frac{2ε}{1 + 2ε} \., \\
        E_{\t{BS}} &=& E_{\Pphoton} - E_{\t{CE}} \..
      \end{eqns}
      Following the Compton continuum, the photo peak is the most interesting part of the spectrum and the most significant quantity for \Pphoton-spectroscopy.
  \section{Experimental Setup}
    \subsection{The Detector}
      A schematic sketch of the experimental setup is given in Figure~\ref{fig:exp-setup}.
      \begin{figure}
        \includegraphics[width=\textwidth]{build/graphic/exp_setup.pdf}
        \caption{Schematic sketch of the experimental setup \cite{anleitung18}.}
        \label{fig:exp-setup}
      \end{figure}
      The detector is a coaxial cylinder; its surface is strongly n-doped with lithium atoms and well conducting.
      Thus the plus pole is located there for applying the block voltage.
      Along the middle axis of the cylinder there is a coaxial bore hole which is metallized with gold.
      This metal–semiconductor contact conforms with a strong p-doping, so that a big depletion zone is achieved, as the acceptor density in germanium is very small.
      The whole detector is surrounded by a protecting aluminium cap.
      Since incoming photons have to overcome the aluminium and lithium doped layers, a lower energy detection limit between \SIrange{40}{50}{\kilo\electronvolt} results.
      The applied voltage for the detector is \SIrange{5}{6}{\kilo\volt}, which extends the depletion zone from $\SI{3}{\micro\meter}$ to approximately $\SI{3}{\centi\meter}$.
      This detector has a doping of $n_{\t{A}} = \SI{10e10}{\per\cubic\centi\meter}$.
    \subsection{Electronic Mode of Operation}
      The aim of the detector is to generate a voltage peak, the height of which is proportional to the energy of the photon.
      A capacitor regenerated operational amplifier fulfills this task by electronically integrating over the charge which is produced by the photon in a certain time:
      \begin{eqn}
        U_{\t{A}} = -\frac{1}{C_{\t{K}}} \int_0^{t_\t{c}} \dif{t} \f{I}(t) \.,
      \end{eqn}
      where $\f{I}(t)$ is the current at time $t$, $C_{\t{K}}$ is the capacitance and $U_{\t{A}}$ is the integrated voltage.
      The capacitor has to be unloaded for the next event but a resistance produces too much noise.
      Hence the unloading is performed by an optoelectronic feedback.
      After every pulse, the field-effect transistor in the operational amplifier is illuminated by an LED so that the junction becomes temporarily conducting.

      The signal is further amplified for the analog-to-digital converter.
      Due to statistical fluctuations, several pulses can occur during a short time, leading to a large voltage.
      These large voltages are ignored, making accounting for the dead time of the detector necessary.
      The dead time $T_{\t{C}}$ is about \SI{40}{\micro\second}.
      The voltage of the capacitor is lead to a multichannel analizer, which sorts the pulse into one of $8192$ channels.
      Figure~\ref{fig:elec_setup} gives a complete overview of the electronic setup.
      \begin{figure}
        \includegraphics[width=\textwidth]{build/graphic/elec_setup.pdf}
        \caption{Overview of the electronic setup \cite{anleitung18}.}
        \label{fig:elec_setup}
      \end{figure}
  \section{Experimental Realisation}
    First, a baseline measurement is performed in order to be able to subtract the natural activity of the environment.

    The multichannel analyser has to be calibrated so that every channel can be identified with a certain energy.
    For this calibration, a \ce{^{152}Eu} sample is used, the spectrum of which is known.
    Furthermore, this measurement is used to determine the energy-dependent efficiency of the detector, i.e.\ the probability to detect a photon of a given energy.

    The spectrum of a \ce{^{137}Cs} sample is measured for investigating further properties of the detector, the Compton edge and the photo peak.

    After the calibration measurements, the spectra of \ce{^{133}Ba} and a uranium salt are measured.
  \section{Analysis}
    \subsection{Baseline Measurement}
      For the following calculations, the background, consisting of all events not induced by the sample, has to be taken into account.
      The results of the baseline measurement are shown in Figure~\ref{fig:0}, where $Z$ is the number of events per time and bin and $b$ is the bin.
      \begin{figure}
        \includegraphics{build/graphic/0.pdf}
        \caption{Baseline measurement to detect background.}
        \label{fig:0}
      \end{figure}

      The background is subtracted separately for each bin from all of the other measured spectra.
      This simple method does not take into account the energy resolution of the detector.
      However, the method is good enough in order to identify which peaks are part of the signal.
      \FloatBarrier
    \subsection{\texorpdfstring{\ce{^{152}Eu}}{Eu} Spectrum} \label{Eu}
      Figure~\ref{fig:Eu} shows the spectrum of the \ce{^{152}Eu} sample, from which the background has been subtracted.
      The numbered peaks are each fit to normal distribution plus a constant background:
      \begin{eqn}
        \f{Z}(b') = \frac{Z_{\t{A}}}{\sqrt{2 \PI} σ} \exp(-\frac{\g(b - b')²}{2 σ²}) + o \.,
      \end{eqn}
      whereby $b$ is the location of the peak, $σ$ its standard deviation, $Z_{\t{A}}$ the number of events per time in the peak, i.e.\ the area of the peak and $o$ is the constant background.
      Table~\ref{tab:Eu} contains the results of the fits.
      \begin{figure}
        \includegraphics{build/graphic/Eu.pdf}
        \caption{
          Spectrum of \ce{^{152}Eu}, from which the background has been subtracted.
          The peaks used in the analysis are numbered.
        }
        \label{fig:Eu}
      \end{figure}
      \begin{landscape}
        \vspace*{\fill}
        \begin{table}
          \caption{The mean bin index of the fitted peaks in the \ce{^{152}Eu} spectrum, their standard deviations $σ$, registered events per peak and second $Z_{\t{A}}$, the offset $o$ of the normal distribution on y-axis, the matching energy $E_{\Pphoton}$, the probability for these energies $W$ and the efficiency $Q$ dependant on energy.}
          \label{tab:Eu}
          \input{table/Eu.tex}
        \end{table}
        \vspace*{\fill}
      \end{landscape}

      Table~\ref{tab:Eu} also contains the energies $E_{\Pphoton}$ of the peaks.
      These are used to calibrate the detector by fitting the linear function
      \begin{eqn}
        \f{E_{\Pphoton}}(b) = B b + C \label{eqn:E-bins} \.,
      \end{eqn}
      through the data points.
      Table~\ref{tab:calibration} shows the results of the fit, while Figure~\ref{fig:calibration} shows the data and the linear fit.
      \begin{table}
        \caption{Fit parameters of the linear calibration fit shown in Figure~\ref{fig:calibration}.}
        \label{tab:calibration}
        \input{build/table/calibration.tex}
      \end{table}
      \begin{figure}
        \includegraphics{build/graphic/calibration.pdf}
        \caption{
          Calibration fit.
          The red crosses result from fitting the peaks in Figure~\ref{fig:Eu} and identifying the energy $E_{\Pphoton}$.
          A linear fit extrapolates this data to all bins $b$.
        }
        \label{fig:calibration}
      \end{figure}

      The efficiency
      \begin{eqns}
        Q &=& \frac{4 \PI}{Ω} \frac{Z}{A W} \label{eqn:efficiency}
        \itext{of the detector can be calculated using}
        \Omega &=& 2 \PI \g(1 - \frac{d}{\sqrt{d² + r²}}) \label{eqn:solid_angle}
        \itext{and}
        A &=& A_0 \g(\frac{1}{2})^{\!\! \frac{t - t_0}{T_\t{h}}} \.,
      \end{eqns}
      where $A$ the activity of the sample, $W$ the emission probability of a certain energy, $Ω$ the solid angle which the detector covers, $d$ the distance of detector from the source, $r$ the detector's radius, $A_0$ the activity of the sample on 2000-10-01 ($t_0$), $t$ is the experiment's date and $T_{\t{h}}$ the half-life of \ce{^{152}Eu}.
      Table~\ref{tab:const_eff} contains all constant quantities for the efficiency calculation.
      \begin{table}
        \caption{
          Constant quantities for efficiency calculation.
          The times $t_0$ and $t$ are given as Unix timestamps.
        }
        \label{tab:const_eff}
        \input{table/constants.tex}
      \end{table}
      The efficiency $Q$ is fit using the exponential function
      \begin{eqn}
        \f{Q}(E_{\Pphoton}) = D \E^{F E_{\Pphoton}} + G \.. \label{eqn:exp-fit}
      \end{eqn}
      The results of the fit are shown in Table~\ref{tab:efficiency}, while the data and the fit are plotted in Figure~\ref{fig:efficiency}.
      \begin{table}
        \caption{Fit parameters of the efficiency fit.}
        \label{tab:efficiency}
        \input{build/table/efficiency.tex}
      \end{table}
      \begin{figure}
        \includegraphics{build/graphic/efficiency.pdf}
        \caption{Exponential fit of the efficiency $Q$.}
        \label{fig:efficiency}
      \end{figure}
      \FloatBarrier
    \subsection{\texorpdfstring{\ce{^{137}Cs}}{Cs} Spectrum}
      Figure~\ref{fig:Cs} shows the energy spectrum of \ce{^{137}Cs}.
      The registered events $Z$ are corrected using the efficiency of the detector, which decreases exponentially for increasing energies.
      \begin{figure}
        \includegraphics{build/graphic/Cs.pdf}
        \caption{Energy spectrum of \ce{^{137}Cs}.}
        \label{fig:Cs}
      \end{figure}

      As mentionend earlier, the photo peak represents the full energy of the photon.
      Thus, Figure~\ref{fig:Cs-photo} shows a fit of the photo peak with a normal distribution, determining the mean energy of the peak.
      \begin{figure}
        \includegraphics{build/graphic/Cs-photo.pdf}
        \caption{Photo peak in the \ce{^{137}Cs} spectrum.}
        \label{fig:Cs-photo}
      \end{figure}
      The fit results are shown in Table~\ref{tab:Cs}.
      In addition, the value for $\Del{E}$ from \eqref{eqn:fano} and the full width at half maximum (FWMH) are included there.
      Furthermore, the table contains $ε$ (see Equation~\eqref{eqn:ε}), the energy of the Compton edge $E_{\t{CE}}$, also from Equation~\eqref{eqn:ε}, and the energy of the backscatter peak $E_{\t{BS}}$.
      The Compton edge and the backscatter peak are marked in the spectrum in Figure~\ref{fig:Cs-compton}.

      The data fitted to determine $κ$ in
      \begin{eqn}
        \f{Z}(E) = κ \g(2 + \g(\frac{E}{E_{\Pphoton} - E})^{\!\! 2} \g(\frac{1}{ε²} + \frac{E_{\Pphoton} - E}{E_{\Pphoton}} - \frac{2}{ε} \frac{E_{\Pphoton} - E}{E_{\Pphoton}})) \label{eqn:κ}
      \end{eqn}
      is marked in Figure~\ref{fig:Cs-compton}.
      The result is included in Table~\ref{tab:Cs}.

      The theory describes the locations of the Compton edge and the backscatter peak well.
      Equation~\eqref{eqn:κ} describes the Compton continuum in a qualitatively acceptable manner, though the details, for example the slope, are not quite correct.
      \begin{figure}
        \includegraphics{build/graphic/Cs-compton.pdf}
        \caption{
          Compton continuum of the \ce{^{137}Cs} spectrum.
          The calculated energies for the Compton edge and the backscatter peak are marked.
        }
        \label{fig:Cs-compton}
      \end{figure}
      \begin{table}
        \caption{Natural constants used in the calculations \cite{scipy}.}
        \label{tab:natural-constants}
        \input{table/natural-constants.tex}
      \end{table}
      \begin{table}
        \caption{
          Results of fitting the photo peak of the \ce{^{137}Cs} spectrum with a normal distribution.
          The calculated energies of the Compton edge and the backscatter peak and the fit result for $κ$ are included.
        }
        \label{tab:Cs}
        \input{table/Cs.tex}
      \end{table}
      \FloatBarrier
    \subsection{\texorpdfstring{\ce{^{133}Ba}}{Ba} Spectrum}
      The measured spectrum for \ce{^{133}Ba} is shown in Figure~\ref{fig:Ba}.
      As in section~\ref{Eu}, the numbered peaks are fit using a normal distribution; Table~\ref{tab:Ba} contains the results.
      \begin{figure}
        \includegraphics{build/graphic/Ba.pdf}
        \caption{
          The spectrum of \ce{^{133}Ba}.
          The numbered peaks are used for the analysis.
        }
        \label{fig:Ba}
      \end{figure}
      \begin{landscape}
        \vspace*{\fill}
        \begin{table}
          \caption{
            Results of fitting the peaks of the \ce{^{133}Ba} spectrum with normal distributions.
            Furthermore, the probability $W$ of the emission energies and the resulting activity $A$ are included.
          }
          \label{tab:Ba}
          \input{table/Ba.tex}
        \end{table}
        \vspace*{\fill}
      \end{landscape}
      The activity of the sample can be calculated using
      \begin{eqn}
        A = \frac{4 \PI}{Ω} \frac{Z}{Q W} \..
      \end{eqn}
      The results for the activity and a weighted average are shown in Table~\ref{tab:Ba}.
      \FloatBarrier
    \subsection{Uranium Spectrum}
      The spectrum of the uranium salt is shown in Figure~\ref{fig:U}.
      \begin{figure}
        \includegraphics{build/graphic/U.pdf}
        \caption{
          Spectrum of the uranium salt.
          The fitted peaks are numbered.
        }
        \label{fig:U}
      \end{figure}
      As in section~\ref{Eu}, the numbered peaks are fit using a normal distribution; Table~\ref{tab:U} contains the results.
      Determing the locations of the peaks enables matching them to characteristic emission lines of isotopes which appear in natural decay chains; the results can also be found in Table~\ref{tab:U}.
      \begin{landscape}
        \vspace*{\fill}
        \begin{table}
          \caption{
            Results of fitting the peaks of the uranium salt spectrum with normal distributions.
            The matching isotopes are included in the last column.
          }
          \label{tab:U}
          \input{build/table/U.tex}
         \end{table}
        \vspace*{\fill}
      \end{landscape}
  \section{Discussion}
    The calibration fit confirms the linear behaviour of the electronic bin assignment.
    The efficiency decreases exponentially with increasing energy.

    For the \ce{^{137}Cs} spectrum, the theory describes the photo peak and the Compton continuum well.
    The full width at half maximum matches the result of Equation~\eqref{eqn:fano} only approximately.

    The activity of most \ce{^{133}Ba} lines is close to the mean activity.
    Only the first peak deviates significantly.

    The spectrum of the uranium salt shows elements of its natural decay chain.
    The isotope \ce{^{226}Ra} is identified despite it only having one emission line with small emission probability and a long half-life of $\SI{1600}{\year}$.
    This indicates a large amount of this isotope in the salt.
    Furthermore, \ce{^{214}Pb} and \ce{^{214}Bi} are found which succeed \ce{^{226}Ra} directly in the decay chain.
    Peaks of other isotopes are smaller.

    In all cases the fit of the measured peaks with a normal distribution works well.
  \makebibliography
\end{document}
