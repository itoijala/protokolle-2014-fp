import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

T = np.loadtxt('data/T_0')
b, N = np.loadtxt('data/N_0', unpack=True)
Z = N / T

fig, ax = plt.subplots(1, 1)
ax.plot(b, Z, 'r-', drawstyle='steps')
ax.set_xlim(0, 8191)
ax.set_xlabel(r'$b$')
ax.set_ylabel(r'$\silabel{Z}{\per\second\per\bin}$')
fig.savefig('build/graphic/0.pdf')
