import pickle

import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

from common import gauss, fit_gauss

from math import *

e, m, c = lt.constant('elementary charge', 'electron mass', 'speed of light in vacuum')

T_0 = np.loadtxt('data/T_0')
b_0, N_0 = np.loadtxt('data/N_0', unpack=True)
Z_0 = N_0 / T_0

T = np.loadtxt('data/T_Cs')
b, N = np.loadtxt('data/N_Cs', unpack=True)
N = unp.uarray(N, np.sqrt(N))
Z = N / T - Z_0

cal_a, cal_b = pickle.load(open('build/result/calibration.pickle', 'rb'))
E = cal_a * b + cal_b

eff_a, eff_b, eff_c = pickle.load(open('build/result/efficiency.pickle', 'rb'))
Z /= eff_a * unp.exp(eff_b * E) + eff_c

bin_width = r'\SI{' + '{:.2f}'.format(float(lt.noms((np.max(E) - np.min(E)) / len(E)))) + r'}{\kilo\electronvolt}'

fig, ax = plt.subplots(1, 1)
E_γ, σ, Z_peak, o = fit_gauss(lt.noms(E), Z, 655, 670, ax)
ax.set_xlabel(r'$\silabel{E_{\Pphoton}}{\kilo\electronvolt}$')
ax.set_ylabel(r'$\silabel{\frac{Z / Q}{' + bin_width + r'}}{\per\second}$')
fig.savefig('build/graphic/Cs-photo.pdf')

ε = E_γ * 1e3 * e / (m * c**2)
E_CE = E_γ * 2 * ε / (1 + 2 * ε)
E_BS = E_γ - E_CE

def compton(E, κ):
    return lt.noms(κ * (2 + (E / (E_γ - E))**2 * (1 / ε**2 + (E_γ - E) / E_γ - 2 / ε * (E_γ - E) / E_γ)))

κ = lt.ucurve_fit(compton, E[(250 <= E) & (E <= 450)], Z[(250 <= E) & (E <= 450)])

FWHM = np.sqrt(8 * np.log(2)) * σ
ΔE = 2.35 * unp.sqrt(0.1 * E_γ * 2.9e-3)

fig, ax = plt.subplots(1, 1)
ax.plot(lt.noms(E), lt.noms(Z), 'r-', drawstyle='steps')
ax.set_xlim(0, 750)
ax.set_ylim(-0.3, 3)
ax.set_xlabel(r'$\silabel{E_{\Pphoton}}{\kilo\electronvolt}$')
ax.set_ylabel(r'$\silabel{\frac{Z / Q}{' + bin_width + r'}}{\per\second}$')
fig.savefig('build/graphic/Cs.pdf')

fig, ax = plt.subplots(1, 1)
ax.plot(lt.noms(E)[E < 250], lt.noms(Z)[E < 250], 'r-', drawstyle='steps')
ax.plot(lt.noms(E)[E > 450], lt.noms(Z)[E > 450], 'r-', drawstyle='steps')
ax.plot(lt.noms(E)[(250 <= E) & (E <= 450)], lt.noms(Z)[(250 <= E) & (E <= 450)], 'y-', drawstyle='steps', label='Data in fit')
ax.axvline(x=lt.noms(E_CE), c='g', ls='--', label=r'Compton edge $E_{\t{CE}}$')
ax.axvline(x=lt.noms(E_BS), c='k', ls='--', label=r'Backscatter peak $E_{\t{BS}}$')
x = np.linspace(0, 500, 1000)
ax.plot(x, compton(x, lt.noms(κ)), 'b-', label='Fit')
ax.set_xlim(0, 500)
ax.set_ylim(-0.1, 3)
ax.set_xlabel(r'$\silabel{E_{\Pphoton}}{\kilo\electronvolt}$')
ax.set_ylabel(r'$\silabel{\frac{Z / Q}{' + bin_width + r'}}{\per\second}$')
ax.legend(loc='upper left')
fig.savefig('build/graphic/Cs-compton.pdf')

tab = lt.SymbolRowTable()
tab.add_si_row('e', e * 1e19, r'\coulomb', exp='e-19')
tab.add_si_row(r'm_{\Pe}', m * 1e31, r'\kilogram', exp='e-31')
tab.add_si_row('c', lt.noms(c) * 1e-8, r'\meter\per\second', exp='e8', places=8)
tab.savetable('build/table/natural-constants.tex')

tab = lt.SymbolRowTable()
tab.add_si_row(r'E_{\Pphoton}', E_γ, r'\kilo\electronvolt')
tab.add_si_row('σ', σ, r'\kilo\electronvolt')
tab.add_si_row(r'Z_{\t{A}}', Z_peak, r'\per\second')
tab.add_si_row('o', o * 1e3, r'\per\kilo\second')
tab.add_hrule()
tab.add_si_row(r'\t{FWHM}', FWHM, r'\kilo\electronvolt')
tab.add_si_row(r'\Del{E}', ΔE, r'\kilo\electronvolt')
tab.add_hrule()
tab.add_si_row('ε', ε)
tab.add_si_row(r'E_{\t{CE}}', E_CE, r'\kilo\electronvolt')
tab.add_si_row(r'E_{\t{BS}}', E_BS, r'\kilo\electronvolt')
tab.add_hrule()
tab.add_si_row('κ', κ, r'\per\second')
tab.savetable('build/table/Cs.tex')
