import pickle

import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

from common import gauss, fit_gauss

T_0 = np.loadtxt('data/T_0')
b_0, N_0 = np.loadtxt('data/N_0', unpack=True)
Z_0 = N_0 / T_0

T = np.loadtxt('data/T_U')
b, N = np.loadtxt('data/N_U', unpack=True)
N = unp.uarray(N, np.sqrt(N))
Z = N / T - Z_0

cal_a, cal_b = pickle.load(open('build/result/calibration.pickle', 'rb'))
E = cal_a * b + cal_b

eff_a, eff_b, eff_c = pickle.load(open('build/result/efficiency.pickle', 'rb'))
Z /= eff_a * unp.exp(eff_b * E) + eff_c

Ω = pickle.load(open('build/result/Ω.pickle', 'rb'))

_, iso = np.loadtxt('data/isotope', dtype={'names':('a', 'b'), 'formats':('f8', 'S20')}, unpack=True, skiprows=3)
iso = [i.decode('utf8').replace('\\\\', '\\') for i in iso]

bin_width = r'\SI{' + '{:.2f}'.format(float(lt.noms((np.max(E) - np.min(E)) / len(E)))) + r'}{\kilo\electronvolt}'

fig, axs = plt.subplots(8, 1)
fig.set_figheight(8 * 3)

results = []
results.append(fit_gauss(lt.noms(E), Z,  177,  197, axs[0]))
results.append(fit_gauss(lt.noms(E), Z,  232,  252, axs[1]))
results.append(fit_gauss(lt.noms(E), Z,  285,  305, axs[2]))
results.append(fit_gauss(lt.noms(E), Z,  342,  362, axs[3]))
results.append(fit_gauss(lt.noms(E), Z,  600,  620, axs[4]))
results.append(fit_gauss(lt.noms(E), Z,  760,  780, axs[5]))
results.append(fit_gauss(lt.noms(E), Z, 1112, 1132, axs[6]))
results.append(fit_gauss(lt.noms(E), Z, 1755, 1775, axs[7]))

E_peak, σ, Z_peak, o = np.array(results).T

for i, (m, s, a) in enumerate(lt.noms(E_peak, σ, Z_peak).T):
    axs[i].text(0.1, 0.9, i + 1, transform=axs[i].transAxes)
    axs[i].set_xlabel(r'$\silabel{E_{\Pphoton}}{\kilo\electronvolt}$')
    axs[i].set_ylabel(r'$\silabel{\frac{Z / Q}{' + bin_width + r'}}{\per\second}$')

fig.savefig('build/graphic/U-peaks.pdf')

fig, ax = plt.subplots(1, 1)
ax.plot(lt.noms(E), lt.noms(Z), 'r-', drawstyle='steps')
for i, (m, s, a) in enumerate(lt.noms(E_peak, σ, Z_peak).T):
    ax.text(m, a / (s * np.sqrt(2 * np.pi)) + 4, i + 1, va='bottom', ha='center', size='xx-small')
ax.set_xlim(0, lt.noms(E[-1]))
ax.set_ylim(-10, 85)
ax.set_xlabel(r'$\silabel{E_{\Pphoton}}{\kilo\electronvolt}$')
ax.set_ylabel(r'$\silabel{\frac{Z / Q}{' + bin_width + r'}}{\per\second}$')
fig.savefig('build/graphic/U.pdf')

tab = lt.SymbolColumnTable()
tab.add_si_column(r'\t{Peak}', list(range(1, len(E_peak) + 1)), places=0)
tab.add_si_column(r'E_{\Pphoton}', E_peak, r'\kilo\electronvolt')
tab.add_si_column('σ', σ, r'\kilo\electronvolt')
tab.add_si_column(r'Z_{\t{A}}', Z_peak, r'\per\second')
tab.add_si_column('o', o, r'\per\second')
tab.add(lt.Column([lt.Cell(iso)], 'l', 'isotopes'))
tab.savetable('build/table/U.tex')
