import pickle

import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

from common import gauss, fit_gauss

A_0, δA_0 = np.loadtxt('data/A_Eu', unpack=True, skiprows=1)
A_0 = unc.ufloat(A_0, δA_0)
t_0, t = np.loadtxt('data/t_Eu', unpack=True)
T_h = np.loadtxt('data/T_h_Eu', unpack=True) * 24 * 60 * 60
r = np.loadtxt('data/r') * 1e-2
a = np.loadtxt('data/a') * 1e-2

A = A_0 * 0.5**((t - t_0) / T_h)
Ω = 2 * np.pi * (1 - a / np.sqrt(a**2 + r**2))

T_0 = np.loadtxt('data/T_0')
b_0, N_0 = np.loadtxt('data/N_0', unpack=True)
Z_0 = N_0 / T_0

T = np.loadtxt('data/T_Eu')
b, N = np.loadtxt('data/N_Eu', unpack=True)
N = unp.uarray(N, np.sqrt(N))
Z = N / T - Z_0

fig, axs = plt.subplots(11, 1)
fig.set_figheight(11 * 3)

results = []
results.append(fit_gauss(b, Z,  475,  510, axs[ 0]))
results.append(fit_gauss(b, Z,  960, 1000, axs[ 1]))
results.append(fit_gauss(b, Z, 1350, 1400, axs[ 2]))
results.append(fit_gauss(b, Z, 1625, 1675, axs[ 3]))
results.append(fit_gauss(b, Z, 1750, 1800, axs[ 4]))
results.append(fit_gauss(b, Z, 3090, 3140, axs[ 5]))
results.append(fit_gauss(b, Z, 3440, 3500, axs[ 6]))
results.append(fit_gauss(b, Z, 3830, 3880, axs[ 7]))
results.append(fit_gauss(b, Z, 4300, 4400, axs[ 8]))
results.append(fit_gauss(b, Z, 4400, 4500, axs[ 9]))
results.append(fit_gauss(b, Z, 5590, 5670, axs[10]))
b_peak, σ, Z_peak, o = np.array(results).T

for i, (m, s, a) in enumerate(lt.noms(b_peak, σ, Z_peak).T):
    axs[i].text(0.1, 0.9, i + 1, transform=axs[i].transAxes)

fig.savefig('build/graphic/Eu-peaks.pdf')

E_peak, W = np.loadtxt('data/E_Eu', unpack=True)
W *= 1e-2

cal_a, cal_b = lt.linregress(b_peak, E_peak)

Q = 4 * np.pi / Ω * Z_peak / (A * W)

def exp(x, a, b, c):
    return a * np.exp(b * x) + c

eff_a, eff_b, eff_c = lt.ucurve_fit(exp, lt.noms(cal_a * b_peak + cal_b), Q, p0=(0.05, -0.01, 0.003))

pickle.dump(Ω, open('build/result/Ω.pickle', 'wb'))

pickle.dump((cal_a, cal_b), open('build/result/calibration.pickle', 'wb'))

pickle.dump((eff_a, eff_b, eff_c), open('build/result/efficiency.pickle', 'wb'))

fig, ax = plt.subplots(1, 1)
ax.plot(b, lt.noms(Z), 'r-', drawstyle='steps')
for i, (m, s, a) in enumerate(lt.noms(b_peak, σ, Z_peak).T):
    ax.text(m, a / (s * np.sqrt(2 * np.pi)) + 0.05, i + 1, va='bottom', ha='center', size='xx-small')
ax.set_xlim(0, 6000)
ax.set_ylim(-0.2, 1.7)
ax.set_xlabel(r'$b$')
ax.set_ylabel(r'$\silabel{Z}{\per\second\per\bin}$')
fig.savefig('build/graphic/Eu.pdf')

fig, ax = plt.subplots(1, 1)
ax.plot(lt.noms(b_peak), E_peak, 'rx')
x = np.linspace(0, 6000)
ax.plot(x, lt.noms(cal_a * x + cal_b), 'b-', zorder=-1)
ax.set_ylim(0, 1600)
ax.set_xlabel(r'$b$')
ax.set_ylabel(r'$\silabel{E_{\Pphoton}}{\kilo\electronvolt}$')
fig.savefig('build/graphic/calibration.pdf')

fig, ax = plt.subplots(1, 1)
ax.errorbar(lt.noms(cal_a * b_peak + cal_b), lt.noms(Q), yerr=lt.stds(Q), fmt='rx')
x = np.linspace(0, 1600, 1000)
ax.plot(x, exp(x, *lt.noms(eff_a, eff_b, eff_c)), 'b-', zorder=-1)
ax.set_xlim(0, 1600)
ax.set_xlabel(r'$\silabel{E_{\Pphoton}}{\kilo\electronvolt}$')
ax.set_ylabel(r'$Q$')
fig.savefig('build/graphic/efficiency.pdf')

tab = lt.SymbolRowTable()
tab.add_si_row(r'T_{\t{h}}', T_h / 24 / 60 / 60, r'\day', places=0)
tab.add_si_row(r'A_0', A_0, r'\becquerel')
tab.add_si_row(r't_0', t_0, r'\second', places=0)
tab.add_si_row(r't', t, r'\second', places=0)
tab.add_si_row(r'A', A, r'\becquerel')
tab.add_hrule()
tab.add_si_row('r', r * 1e2, r'\centi\meter', places=1)
tab.add_si_row('a', a * 1e2, r'\centi\meter', places=0)
tab.add_si_row('Ω', Ω, r'\steradian', places=2)
tab.savetable('build/table/constants.tex')

tab = lt.SymbolColumnTable()
tab.add_si_column(r'\t{Peak}', list(range(1, len(E_peak) + 1)), places=0)
tab.add_si_column('b', b_peak)
tab.add_si_column('σ', σ)
tab.add_si_column(r'Z_{\t{A}}', Z_peak, r'\per\second')
tab.add_si_column('o', o * 1e3, r'\per\kilo\second')
tab.add_si_column(r'E_{\Pphoton}', E_peak, r'\kilo\electronvolt', places=2)
tab.add_si_column('W', W * 1e2, r'\percent', places=1)
tab.add_si_column('Q', Q * 1e2, r'\percent')
tab.savetable('build/table/Eu.tex')

tab = lt.SymbolRowTable()
tab.add_si_row('B', cal_a, r'\kilo\electronvolt')
tab.add_si_row('C', cal_b, r'\kilo\electronvolt')
tab.savetable('build/table/calibration.tex')

tab = lt.SymbolRowTable()
tab.add_si_row('D', eff_a)
tab.add_si_row('F', eff_b, r'\per\kilo\electronvolt')
tab.add_si_row('G', eff_c)
tab.savetable('build/table/efficiency.tex')
