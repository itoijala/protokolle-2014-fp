import pickle

import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

from common import gauss, fit_gauss

T_0 = np.loadtxt('data/T_0')
b_0, N_0 = np.loadtxt('data/N_0', unpack=True)
Z_0 = N_0 / T_0

T = np.loadtxt('data/T_Ba')
b, N = np.loadtxt('data/N_Ba', unpack=True)
N = unp.uarray(N, np.sqrt(N))
Z = N / T - Z_0

cal_a, cal_b = pickle.load(open('build/result/calibration.pickle', 'rb'))
E = cal_a * b + cal_b

eff_a, eff_b, eff_c = pickle.load(open('build/result/efficiency.pickle', 'rb'))
Z /= eff_a * unp.exp(eff_b * E) + eff_c

Ω = pickle.load(open('build/result/Ω.pickle', 'rb'))
E_peak, W = np.loadtxt('data/E_Ba', unpack=True)
W *= 1e-2

bin_width = r'\SI{' + '{:.2f}'.format(float(lt.noms((np.max(E) - np.min(E)) / len(E)))) + r'}{\kilo\electronvolt}'

fig, axs = plt.subplots(4, 1)
fig.set_figheight(4 * 3)

results = []
results.append(fit_gauss(lt.noms(E), Z, 60, 100, axs[0]))
results.append(fit_gauss(lt.noms(E), Z, 280, 320, axs[1]))
results.append(fit_gauss(lt.noms(E), Z, 335, 375, axs[2]))
results.append(fit_gauss(lt.noms(E), Z, 374, 394, axs[3]))

E_peak, σ, Z_peak, o = np.array(results).T

A = 4 * np.pi / Ω * Z_peak / W
A_mean = lt.umean(A)

for i, (m, s, a) in enumerate(lt.noms(E_peak, σ, Z_peak).T):
    axs[i].text(0.1, 0.9, i + 1, transform=axs[i].transAxes)
    axs[i].set_xlabel(r'$\silabel{E_{\Pphoton}}{\kilo\electronvolt}$')
    axs[i].set_ylabel(r'$\silabel{\frac{Z / Q}{' + bin_width + r'}}{\per\second}$')

fig.savefig('build/graphic/Ba-peaks.pdf')

fig, ax = plt.subplots(1, 1)
ax.plot(lt.noms(E), lt.noms(Z), 'r-', drawstyle='steps')
for i, (m, s, a) in enumerate(lt.noms(E_peak, σ, Z_peak).T):
    ax.text(m, a / (s * np.sqrt(2 * np.pi)) + 2, i + 1, va='bottom', ha='center', size='xx-small')
ax.set_xlim(0, 500)
ax.set_ylim(-10, 80)
ax.set_xlabel(r'$\silabel{E_{\Pphoton}}{\kilo\electronvolt}$')
ax.set_ylabel(r'$\silabel{\frac{Z / Q}{' + bin_width + r'}}{\per\second}$')
fig.savefig('build/graphic/Ba.pdf')

tab = lt.SymbolColumnTable()
tab.add_si_column(r'\t{Peak}', list(range(1, len(E_peak) + 1)), places=0)
tab.add_si_column(r'E_{\Pphoton}', E_peak, r'\kilo\electronvolt')
tab.add_si_column('σ', σ, r'\kilo\electronvolt')
tab.add_si_column(r'Z_{\t{A}}', Z_peak, r'\per\second')
tab.add_si_column('o', o * 1e3, r'\per\kilo\second')
tab.add_si_column('W', W * 1e2, r'\percent', places=1)
tab.add_si_column('A', A, r'\becquerel')

tab1 = lt.SymbolRowTable()
tab1.add_si_row(r'\mean{A}', A_mean, r'\becquerel')

tab2 = lt.Table()
tab2.add(lt.Row())
tab2.add_hrule()
tab2.add(lt.Row())
tab2.add(lt.Column([tab, tab1], "@{}c@{}c@{}"))
tab2.savetable('build/table/Ba.tex')
