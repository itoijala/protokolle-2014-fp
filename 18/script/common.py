import numpy as np

from scipy.stats import norm

import labtools as lt

def gauss(x, mean, std, area, offset):
    return area * norm.pdf(x, mean, std) + offset

def fit_gauss(b, Z, xmin, xmax, ax):
    b1, Z1 = b[(xmin <= b) & (b <= xmax)], Z[(xmin <= b) & (b <= xmax)]

    mean, std, area, offset = lt.ucurve_fit(gauss, b1[lt.stds(Z1) > 0], Z1[lt.stds(Z1) > 0], p0=((xmax + xmin) / 2, 1, 1, 0))

    ax.errorbar(b1, lt.noms(Z1), xerr=(xmax - xmin) / (2 * len(b1)),yerr=lt.stds(Z1), fmt='r ', capsize=0, zorder=3)
    x = np.linspace(xmin, xmax, 1000)
    ax.plot(x, gauss(x, *lt.noms(mean, std, area, offset)), 'b-')
    ax.set_xlim(xmin, xmax)
    ax.set_xlabel(r'$b$')
    ax.set_ylabel(r'$\silabel{Z}{\per\second\per\bin}$')

    return mean, std, area, offset
