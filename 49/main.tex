\input{header/report.tex}

\SetExperimentNumber{49}

\addbibresource{lit.bib}

\begin{document}
  \maketitlepage{Messung von Diffusionskonstanten mittels gepulster Kernspinresonanz}{30.06.2014}{06.10.2014}
  \section{Ziel}
    Ziel dieses Experimentes ist es, die Relaxationszeiten $T_1$ und $T_2$ zweier Relaxationsvorgänge des Kernspins in Wasser zu messen.
    Des Weiteren wird die Diffusionskonstante von Wasser bestimmt; mihilfe einer zusätzlichen Viskositäts- und Temperaturmessung kann eine Abschätzung für den Molekülradius von Wasser getroffen werden, die mit anderen verglichen wird.
  \section{Theorie}
    Die magnetischen Momente von Atomkernen können in äußeren Magnetfeldern teilweise orientiert werden; dies resultiert in einer makroskopischen Magnetisierung.
    Sie kann durch hochfrequente elektromagnetische Strahlung geändert werden.
    Dabei können Resonanzphänomene, oder aber auch, wie hier, mikroskopische Relaxationsprozesse, untersucht werden.
    Dies geschieht mit zeitlich begrenzten Hochfrequenzpulsen, weshalb es sich hier um die gepulste Kernspinresonanz handelt.
    Eine Abnahme des Ausgangssignals kann auch durch Diffusionsvorgänge mit der Diffusionskonstanten $D$ hervorgerufen werden.
    Hierbei diffundieren die magnetischen Momente in Volumenbereiche in denen, wegen der Änderung des statischen Magnetfeldes, die Resonanzbedingung nicht mehr erfüllt ist.
    \subsection{Kernspinresonanz}
      \subsubsection{Magnetisierung einer Probe im thermischen Gleichgewicht mit der Umgebung}
      Analog zum Wasserstoffmodell sind die Kernspinzustände  in ihrer Quantenzahl $l$ im feldfreien Raum mit der Multiplizität $\g(2 l + 1)$ entartet.
        Ein Magnetfeld führt zu einer energetischen Aufspaltung
        \begin{eqn}
          \Del{E} = γ B_0
        \end{eqn}
        der zuvor entarteten Niveaus, wobei $γ$ das gyromagnetische Verhältnis und $B_0$ das Magnetfeld in $z$-Richtung sind.
        Diese können nun mit der Orientierungsquantenzahl $m$, die bestimmte Orientierung des Spins relativ zum Magnetfeld repräsentiert, beschrieben werden.
        Speziell für protonische Kernspins (Spin $\frac{1}{2}$) werden die zwei Unterniveaus mit $m = ± \frac{1}{2}$ indiziiert.

        Das Besetzungsverhältnis benachbarter Unterniveaus ist durch die Boltzmannverteilung
        \begin{eqn}
          \frac{\f{N}(m)}{\f{N}(m - 1)} = \exp.-\frac{γ B_0}{k_{\t{B}} T}. \label{eqn:boltzmann-distribution}
        \end{eqn}
        gegeben, wobei $k_\t{B}$ die Boltzmannkonstante und $T$ die Temperatur sind.
        Die ungleiche Besetzung führt zu einer Kernspinpolarisation, die über die mit dem Spin verknüpften magnetischen Dipolmomente $\v{μ}_l$ eine makroskopische Magnetisierung, die als Summe über die magnetischen Momente pro Volumen definiert ist, hervorruft.


        Für typische Felder von \SI{1}{\tesla} gilt $γ B_0 \ll k_{\t{B}} T$, sodass die Exponentialfunktion der Boltzmann-Verteilung \eqref{eqn:boltzmann-distribution} durch die 1. Ordnung ihrer Taylorreihe approximiert werden kann.
        Unter diesen Annahmen ergibt sich insgesamt für die Magnetisierung
        \begin{eqn}
          M_0 = \frac{1}{4} μ_0 \frac{γ^2}{k_\t{B}} N \frac{B_0}{T} \.,
        \end{eqn}
        wobei $μ_0$ die Permeabilität des Vakuums und $N$ die Anzahl der Spins ist.
      \subsubsection{Blochsche Gleichungen mit Relaxationstermen}
        Die für die Kernspinresonanz relevante makroskopische Magnetisierung wird durch Hochfrequenzquanten aus ihrer Gleichgewichtslage gebracht, sie lässt sich klassisch beschreiben, da sie sich aus \SI{e28}{\per\cubic\meter} Einzelmomenten zusammensetzt.
        Die Dynamik und damit die Änderung des Drehimpulses ist daher ein Drehmoment, das zur einer Präzession um die Magnetfeldachse führt; es gilt außerdem für den Zusammenhang zwischen Drehimpuls und Magnetisierung
        \begin{eqn}
          \v{L} = γ \v{M} \..
        \end{eqn}
        Nach dem Auslenken aus der Gleichgewichtslage kommt es zu Relaxationsprozessen, die die präzedierende Magnetisierung wieder in die Gleichgewichtslage bringen.

        Insgesamt lassen sich diese beiden Effekte durch die blochschen Gleichungen
        \begin{eqns}
          \IEEEyesnumber \label{eqn:bloch} \IEEEyessubnumber*
          \d{t}{M_z}; &=& \frac{M_0 - M_z}{T_1}     \\
          \d{t}{M_x}; &=& γ B_0 M_y - \frac{M_x}{T_2} \\
          \d{t}{M_y}; &=& γ B_0 M_x - \frac{M_y}{T_2} \.,
        \end{eqns}
        beschreiben, wobei $T_1$ die longitudinale oder Spin-Gitter-Relaxationszeit und $T_2$ die transversale oder Spin-Spin-Relaxationszeit ist.
        Werden auf der rechten Seite in den Gleichungen \eqref{eqn:bloch} die Terme mit den Relaxationszeiten weggelassen, so präzediert der Spin um die $z$-Achse mit der Larmorfrequenz $ω_{\t{L}} = γ B_0$.
        Die longitudinale Relaxationszeit $T_1$ beschreibt die Veränderung der Magnetisierungskomponente parallel zur Magnetfeldrichtung.
        Ihr zweiter Name kommt von der Zweitbedeutung, dass sie die charakteristische Zeit ist, in der im Festkörper Energie von dem Kernspinsystem in Gitterschwingungen, beziehungsweise umgekehrt, übergeht; im übertragenen Sinn gilt diese Vorstellung auch für Flüssigkeiten.
        Die transversale Relaxationszeit $T_2$ beschreibt das Zeitverhalten der Magnetisierungskomponente senkrecht zum Magnetfeld, deren Zerfall oft durch die Wechselwirkung direkt benachbarter Spins bedingt ist.
        Dies ist damit Ursprung für den zweiten Namen von $T_2$, obwohl auch Spin-Gitter-Relaxationsprozesse wie bei $T_1$ dazu beitragen können.
      \subsubsection{Funktion des hochfrequenten Feldes}
        Das Feld des hochfrequenten Magnetfeldes, das dazu dient, die makroskopische Magnetisierung des Spinensembles aus der Gleichgewichtslage auszulenken, ist senkrecht zum statischen Magnetfeld und durch
        \begin{eqn}
          \f{\v{B}_{\t{HF}}}(t) = 2 \v{B}_1 \cos.ωt.
        \end{eqn}
        gegeben.
        Für eine bessere theoretische Beschreibbarkeit wird das Feld als eine Überlagerung zweier zirkular polarisierter Felder, deren Frequenz $ω$ entgegengesetzt ist, betrachtet.
        Ist $ω$ resonant mit $ω_{\t{L}}$, so ist der Beitrag mit Frequenz $-ω$ vernachlässigbar; es wird eine Transformation in das rotierende System durchgeführt, das mit $ω$ um $\v{B}_0$ rotiert.
        Der Beitrag, der nun mit der doppelten Frequenz rotiert, wird weggelassen.
        Der Vorteil dieser Näherung ist, dass das hochfrequente Feld $\v{B}_{\t{HF}}$ nun nicht mehr zeitabhängig ist und sich so die blochschen Gleichungen vereinfachen.
        Da rotierende, also beschleunigte, Systeme keine Inertialsysteme sind, gilt, unter Vernachlässigung der Relaxationsterme, für die Änderung der Magnetisierung
        \begin{eqn}
          \d{t}{\v{M}}; = \v{M} \times \g(γ \v{B}_{\t{ges}} + \v{ω}) \.; \label{eqn:magRotatingSystem}
        \end{eqn}
        es entsteht also noch ein zu den Scheinkräften der Mechanik analoger Term.
        Es stellt sich heraus, dass Gleichung \eqref{eqn:magRotatingSystem} durch Einführen eines effektiven Feldes
        \begin{eqn}
          \v{B}_{\t{eff}} = \v{B}_0 + \v{B}_1 + \frac{\v{ω}}{γ}
        \end{eqn}
        wieder die Form
        \begin{eqn}
          \d{t}{\v{M}}; = γ \g(\v{M} \times \v{B}_{\t{eff}})
        \end{eqn}
        annimmt.
        Damit präzediert der Magnetisierungsvektor um die Feldrichtung des effektiven Feldes.

        Falls die eingestrahlte Frequenz des Hochfrequenzfeldes resonant mit der Larmorfrequenz ist, gilt $\v{B}_{\t{eff}} = \v{B}_1$ und die Magnetisierung präzediert mit einem Öffnungswinkel des Präzessionskegels von \SI{90}{\degree} um die Feldachse des Feldes $\v{B}_1$.

        Für den Drehwinkel δ gilt die Zeitabhängigkeit
        \begin{eqn}
          \f{δ}(\Del{t}) = γ B_1 \Del{t} \..
        \end{eqn}
        Mit dieser Beziehung lassen sich zwei wohldefinierte Nichtgleichgewichtszustände erzeugen, indem die Pulsdauer des hochfrequenten Feldes variiert wird.
        Dies sind hier zum einen der \SI{90}{\degree}-Puls, der die Magnetisierung bei einer Pulszeit von
        \begin{eqn}
          \Del{t}_{\SI{90}{\degree}} = \frac{\PI}{2 γ B_1}
        \end{eqn}
        aus der $-z$-Richtung in die $y$-Richtung rotiert.
        Zum anderen wird mit der doppelten Einstrahlzeit eine \SI{180}{\degree}-Rotation erzeugt, die die Magnetisierung von der $z$ in die $-z$-Richtung dreht.
    \subsection{Messmethoden}
      \subsubsection{Bestimmung der Relaxationszeit \texorpdfstring{$T_2$}{T₂}} \label{T2}
        Wenn ein hochfrequentes Magnetfeld eingeschaltet wird, das resonant mit der Larmorfrequenz ist, flippt der Spin von $z$ nach $-z$ und wieder zurück.
        Der Magnetisierungsvektor präzidiert mit einem Öffnungswinkel von \SI{90}{\degree} um $\v{B}_1$, also die $x$-Achse.
        Bei einem \SI{90}{\degree}-Puls wird der Magnetisierungsvektor so lange gedreht, bis er parallel zur $y$-Achse ist.
        Ab dann folgt er der Dynamik, die durch die blochschen Gleichungen \eqref{eqn:bloch} gegeben ist, führt also eine Larmorpräzession in der $x$-$y$-Ebene aus und relaxiert in die Gleichgewichtslage; dieser Vorgang wird freier Induktionszerfall genannt.
        Durch diese Präzession in der $x$-$y$-Ebene wird eine Induktionsspannung in der Spule, die die Probe umschließt, erzeugt, die am Oszillographen gemessen werden kann.

        Das statische Magnetfeld ist nicht für alle Spins gleich, da zum einen das Feld durch lokale Dipolfelder von Nachbarspins und Spins aus Elektronenhüllen überlagert ist und zum anderen das statische Magnetfeld nicht perfekt homogen ist.
        Dies hat eine Verteilung der Larmorfrequnzen zur Folge, die zu einer Auffächerung der Spins bei der Präzessionsbewegung um die $z$-Achse führt; somit zerfällt die $z$-Komponente der Magnetisierung.
        Der apparative Effekt dieser beiden führt zu einem zusätzlichen Beitrag $T_{\t{B}}$, der die messbare transversale Relaxationszeit
        \begin{eqn}
          \frac{1}{T_2^{*}} = \frac{1}{T_2} + \frac{1}{T_{\t{B}}}
        \end{eqn}
        verändert und von der Größenordnung $γGd$ ist; hierbei ist $G$ der Gradient des $B_0$ Feldes und $d$ der Durchmesser der Probe.
        Bei hinreichend homogenem Feld kann also $T_2$ aus dem freien Induktionszerfall bestimmt werden, viel häufiger ist jedoch der Fall, bei dem die Inhomogenität des Feldes stark ist und die gemessene Relaxationszeit nicht mit der tatsächlichen übereinstimmt.

        Ist dieser apparative Störeffekt zeitlich konstant, kann er durch die Spin-Echo-Methode eliminiert werden.
        Nach dem \SI{90}{\degree}-Puls dephasieren die einzelnen Spins; im rotierten Koordinatensystem drehen sie sich beispielsweise in unterschiedliche Richtungen, wenn ihre Larmorfrequenz größer oder kleiner als die Frequenz des hochfrequenten Feldes ist.
        Wenn die Spins nach der Zeit $τ$ soweit dephasiert sind, dass makroskopisch kein Induktionssignal mehr vorliegt, wird ein \SI{180}{\degree}-Puls abgestrahlt, der die dephasierten Spins so dreht, dass sie durch die Inhomogenität wieder zusammenlaufen und nach der Zeit $τ$ wieder in Phase sind.
        Dies funktioniert, da die Drehrichtung um die $z$-Achse durch das Magnetfeld $\v{B}_0 \perp \v{B}_1$ gegeben ist und somit der Drehsinn von $\v{B}_1$ nicht beeinflusst wird.
        Die Induktionsspannung, die nun wieder messbar ist, wird als Spin- oder Hahn-Echo bezeichnet; im Grenzfall $T \rightarrow \infty$ wird wieder der selbe Betrag der Amplitude erreicht.
        Der Gesamtverlauf der Spannung ist in Abbildung \ref{fig:spin-echo} veranschaulicht.
        Die Inhomogenitäten, die durch lokale Felder der Spins auftreten, führen zu irreversiblen Dephasierungen und insgesamt zu einem exponentiellen Zerfall
        \begin{eqn}
          \f{M_y}(t) = M_0 \exp.-\frac{t}{T_2}. \label{eqn:T2-naive}
        \end{eqn}
        der Magnetisierung in $y$-Richtung, wobei $M_0$ die Magnetisierung direkt nach dem \SI{90}{\degree}\-/Puls ist.
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/spin-echo.pdf}
          \caption{Schematische Pulssequenz und gesamter Spannungsverlauf bei der Spin-Echo-Methode \cite{anleitung49}.}
          \label{fig:spin-echo}
        \end{figure}

        Obwohl mit Formel \eqref{eqn:T2-naive} eine Bestimmung von $T_2$ möglich ist, ist sie unpraktikabel, da die Relaxation nach jedem Messvorgang abgewartet werden muss.
        Besser ist die Pulsfolge nach Carr\--Purcell; Abbildung \ref{fig:CP-pulse_sequence} enthält die Pulsfolge und das dadurch auftretende Signal.
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/CP-pulse_sequence}
          \caption{Schema für die Pulssequenz bei der Carr\--Purcell-Methode \cite{anleitung49}.}
          \label{fig:CP-pulse_sequence}
        \end{figure}
        Die Carr\--Purcell Methode misst die Komponente der Magnetisierung, die in der $x$-$y$-Ebene liegt.
        Wenn die \SI{180}{\degree}-Pulse nicht exakt sind, wird der Spin nicht exakt wieder in die Ebene gedreht; die Fehler durch aufeinander folgende Pulse addieren sich.
        Insgesamt wird eine zu kleine Zeit gemesssen.

        Diese Nachteile werden in der Meiboom\--Gill-Methode beseitigt; diese ist in Abbildung \ref{fig:MG} veranschaulicht.
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/MG.pdf}
          \caption{Geometrische Veranschaulichung der Meiboom\--Gill-Sequenz \cite{anleitung49}.}
          \label{fig:MG}
        \end{figure}
        Die Pulsfolge ist äquivalent zur Carr\--Purcell-Methode, durch eine Phasenverschiebung zeigt $\v{B}_1$ jedoch in $y$-Richtung, was zu einem Flip der Spins senkrecht dazu führt.
        Durch unvermeidbare systematische Fehler sind die \SI{180}{\degree}-Pulse nicht exakt, der Summenvektor der Spins ist nach dem $(2n + 1)$.\ Puls geringfügig (Abbildung \ref{fig:MG}a) und nach der Refokussierung (Abbildung \ref{fig:MG}b) um $δ$ aus der $x$-$y$-Ebene gekippt, es wird daher $M\cos.δ.$ gemessen.
        Nach dem $(2n)$.\ Puls ist der Gesamtspin wieder in der Ebene, da wieder ein Fehler $δ$ auftritt.
        Die Amplituden der Echos sind bei dieser Methode alle positiv und die nach den geradzahligen Pulsen besitzen die richtige Amplitude.
      \subsubsection{Bestimmung der Relaxationszeit \texorpdfstring{$T_1$}{T₁}} \label{T1}
        Die Pulssequenz beginnt hier mit einem \SI{180}{\degree}-Puls, der die Probenmagnetisierung flippt, sodass keine Komponente in der $x$-$y$-Ebene existiert.
        Nach der Zeit $τ$, in der das System Richtung Gleichgewicht relaxiert, wird ein \SI{90}{\degree}-Puls angewendet, der die noch vorhandene $z$-Komponente in die $x$-$y$-Ebene schiebt und durch Induktion ein Signal erzeugt.
        Die Lösung der blochschen Gleichungen ergibt für die $z$-Komponente der Magnetisierung
        \begin{eqn}
          \f{M_z}(τ) = M_0 \g(1 - 2 \exp.-\frac{τ}{T_1}.) \.. \label{eqn:T1-theory}
        \end{eqn}
      \subsubsection{Einfluss der Diffusion auf eine flüssige Probe} \label{diffusion}
        Die Inhomogenität des statischen Feldes, das die Spins lokal spüren, wird zeitabhängig, wenn die Spins wegen der brownschen Molekularbewegung innerhalb der Zeit $2τ$ ihren Ort wechseln; die Refokussierung wird gestört und die Amplitude nimmt schneller als sonst ab.
        Die blochschen Gleichungen werden um einen Term erweitert, der die Diffusionskonstante $D$ enthält.
        Nach kurzer Rechnung stellt sich heraus, dass die Zeitabhängigkeit der Magnetisierung \eqref{eqn:T2-naive} um einen weiteren Exponentialterm erweitert werden muss, der zum Abfall des Signals beiträgt; dieser hat die Zeitkonstante
        \begin{eqn}
          T_{\t{D}} \equiv \frac{3}{D γ² G² τ²} \..
        \end{eqn}
        Da $T_{\t{D}} \propto τ^{-2}$ ist, kann der Beitrag dieses Terms beliebig gesteuert werden.

        Falls der Diffusionskoeffizient bestimmt werden soll, wird nur das erste Echo betrachtet; dabei muss des Weiteren der Feldgradient bekannt sein.
        Insgesamt gilt in diesem Fall für die Zeitabhängigkeit der Magnetisierung
        \begin{eqn}
          \f{M_y}(t) = M_0 \exp(-\frac{t}{T_2}) \exp(-\frac{D γ² G² t³}{12}) \., \label{eqn:diffusion-theory}
        \end{eqn}
        wobei $t \equiv 2τ$ ist.
  \section{Aufbau}
    \subsection{Teachspin-Apparatur}
      Ein Foto des Aufbaus ist in Abbildung \ref{fig:setup} zu sehen.
      Das hier verwendete Teachspin-System ist bereits richtig verkabelt und bietet die Möglichkeit, die relevanten Parameter einzustellen.
      Links auf dem Bild ist ein starker Permanentmagnet zu sehen, der ein statisches Magnetfeld in $z$-Richtung erzeugt (siehe dazu auch Abbildung \ref{fig:setup-scheme}).
      Die Probe wird in eine gewickelte Spule eingefügt, die ein zur $z$-Achse senkrechtes hochfrequentes Magnetfeld erzeugt.
      Weitere Details wurden bereits in der Theorie beschrieben.
      Rechts steht die Steuerungsapparatur.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/setup.pdf}
        \caption{Aufbau des Experimentes \cite{anleitung49}.}
        \label{fig:setup}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/setup-scheme.pdf}
        \caption{Schematischer Aufbau des Experimentes \cite{anleitung49}.}
        \label{fig:setup-scheme}
      \end{figure}
    \subsection{Viskosimeter}
      \begin{figure}
        \includegraphics[scale=0.30]{graphic/viscosimeter.png}
        \caption{Schematischer Aufbau des Viskosimeters \cite{anleitung49}.}
        \label{fig:viscosimeter}
      \end{figure}
      Ein schematischer Aufbau des Viskosimeters ist in Abbildung \ref{fig:viscosimeter} gegeben.
      Die Zahlen eins bis drei bezeichnen Öffnungen, 9 ist die Vorlaufkugel, 8 die Messkugel, 7 eine Kapillare, 6 der Wasserstand im Anfangszustand und 5 und 4 sind Vorratsgefäße.
      Die Bezeichnungen $\mathup{M}₁$ und $\mathup{M}₂$ markieren die Messmarken, zwischen denen die Zeit beim Messvorgang gestoppt wird.
  \section{Durchführung}
    \subsection{Justierung}
      Tabelle \ref{tab:alignment-init_param} enthält die Resonanzfrequenz $ν$, die Phase $φ$, die Dauer des A-Pulses $t_{\t{A}}$, die Periode $p$ und die Ströme $I_x$, $I_y$ und $I_z$ der Gradienten der Dipolfelder in $x$-, $y$- und $z$-Richtung sowie des Quadropolfeldes in $z$-Richtung.

      Für die Justage wird zunächst eine Wasserprobe mit paramagnetischen Zentren, die zu für die Justage praktischeren und schnelleren Relaxationszeiten führen, in die Apparatur eingesetzt. Dann werden die Parameter des Spektrometers ausgehend von den Werten in Tabelle \ref{tab:alignment-init_param} dahingehend verändert, dass sich eine Kurvenform wie in Abbildung \ref{fig:CP-pulse_sequence} zwischen dem ersten \SI{90}{\degree}-Puls und den darauffolgenden \SI{180}{\degree}-Puls ausbildet.
      Dazu muss zusätzlich zu dem A-Puls (\SI{90}{\degree}) noch ein B-Puls (\SI{180}{\degree}) eingeschaltet werden, der die doppelte Länge hat.
      \begin{table}
        \caption{Startparameter zur Justierung des Spektrometers.}
        \label{tab:alignment-init_param}
        \input{table/align-init_param}
      \end{table}

      Sobald dies geschehen ist, wird die Wiederholzeit zwischen sequentiellen Messungen eingestellt.
      Die Zeit wird solange erhöht, bis eine Änderung dieser keinen Einfluss mehr auf die Spannungsamplitude hat.
      Dazu wird zunächst die längere Zeit $T_1$ gemessen und die Wiederholzeit als ein Vielfaches derer festgelegt.
      Für die Probe mit destilliertem Wasser sind die gefundenen Parameter zwar ein guter Anfangswert, allerdins müssen sie noch weiter optimiert werden.
    \subsection{Bestimmung der Relaxationszeit \texorpdfstring{$T_1$}{T₁}}
      Zur Bestimmung der Relaxationszeit $T_1$ wird das Spannungssignal nach dem ersten Puls (A-Puls: \SI{180}{\degree}) in Abhängigkeit vom Zeitabstand $τ$ zwischen A-Puls und dem zweiten Puls (B-Puls: \SI{90}{\degree}) gemessen.
      Dazu werden die Dauer des A-Pulses und des B-Pulses aus der Messung von $T_2$ vertauscht.
      Der theoretische Hintergrund der Messung befindet sich in Kapitel \ref{T1}.
    \subsection{Bestimmung der Relaxationszeit \texorpdfstring{$T_2$}{T₂}}
      Eine Bestimmung der Relaxationszeit $T_2$ ist mit der Carr\--Purcell- und der Meiboom\--Gill-Methode möglich.
      Da mit der Carr\--Purcell-Methode allerdings systematische Fehler einhergehen, wird die Relaxationszeit nur anhand der Meiboom\--Gill-Methode ausgewertet, die ein besseres Ergebnis für $T_2$ liefert.
      Stellt sich daher ein Bild ähnlich zu Abbildung~\ref{fig:CP-pulse_sequence} ein, so wird das zeitaufgelöste Spannungssignal bei der Carr\--Purcell-Methode auf einen USB-Stick gespeichert.

      Ein Umschalten zwischen den unterschiedlichen Messmethoden geschieht über einen Schalter.
      Für die Meiboom\--Gill-Methode wird die Anzahl der B-Pulse und ihr Pulsabstand so groß gewählt, dass die Echoamplitude etwa um einen Faktor $3$ abfällt; durch die große Anzahl kann nicht mehr jeder Puls aufgelöst werden.
      Um die Forderung $T_2 \ll T_{\t{D}}$ zu erfüllen, muss der Abstand $τ_1$ zwischen den ersten beiden Pulsen so klein gewählt werden, dass eine kleine Änderung keinen Einfluss mehr auf das Abklingverhalten des Spin-Echos hat.
      Weitere theoretische Details befinden sich in Kapitel \ref{T2}.
    \subsection{Bestimmung der Diffusionskonstanten}
      Zunächst wird das Gradientenfeld in $z$-Richtung durch Aufdrehen des Stromes maximiert und die Wiederholzeit hinreichend groß eingestellt, sodass es bei kleiner Änderung dieser keinen Einfluss auf das Abklingverhalten gibt.
      Zur Bestimmung der Diffusionskonstanten wird das Spannungssignal des Spinechos, das nach dem ersten Puls (A-Puls: \SI{90}{\degree}) erzeugt wird, in Abhängigkeit vom zeitlichen Abstand zwischen dem A-Puls und dem zweiten Puls (B-Puls: \SI{180}{\degree}) gemessen.
      Die Spannungs- und Zeitwerte werden dem Oszillographen entnommen.
      Der theoretische Hintergrund dieser Messmethode wird näher in Kapitel \ref{diffusion} beschrieben.
    \subsection{Bestimmung der Halbwertsbreite}
      Bei dieser Messung soll die Halbwertsbreite des Peaks, der bei der Diffusionsmessung (siehe letztes Unterkapitel) untersucht wird, bestimmt werden; dies geschieht mit einem Messprogramm des Oszillographen und fließt in die spätere Berechnung zum Molekülradius ein.
      Des Weiteren wird das zeitaufgelöste Spannungssignal des Peaks auf einen USB-Stick abgespeichert.
    \subsection{Bestimmung der Viskosität von Wasser}
  Bei konstanter Temperatur wird zu Beginn die Öffnung 2 (siehe Abbildung \ref{fig:viscosimeter}) verschlossen, die Öffnung 3 geöffnet und eine an 1 angeschlossene Wasserstrahlpumpe angeschaltet, sodass die Wassermarke 6 bis in Kugel 9 steigt.
  Die Pumpe muss sofort danach abgetrennt werden, dies geschieht mit einem Drei-Wege-Hahn; für die Messung werden 1 und 2 belüftet und die Zeit gestoppt, die die Wassermarke braucht, um von $\mathup{M}_1$ nach $\mathup{M}_2$ zu wandern.
      Nach Beendigung der Messung soll 1 belüftet und 2 und 3 geschlossen werden.
      Das Öffnen und Schließen von 1–3 erfolgt mithilfe eines Gummistopfens.
  \section{Messwerte und Auswertung}
    \subsection{Justierung}
      Tabelle \ref{tab:alignment-H2OCu_param} enthält die Parameter, unter denen die Spannungsamplitude $U_y \propto M_y$ des Spinechos den in Abbildung \ref{fig:alignment} gezeigten, von der Zeit abhängigen Verlauf zeigt.
      Im Einzelnen sind dort die Resonanzfrequenz $ν$, die Phase $φ$, die Dauer des A-Pulses $t_{\t{A}}$, die Periode $p$ und die Ströme $I_x$, $I_y$ und $I_z$ zur Erzeugung der magnetischen Dipolgradientenfelder in $x$-, $y$- und $z$-Richtung, sowie des magnetischen Quadropolfeldes in $z$-Richtung enthalten.
      Diese Paramter sind optimal für die Probe mit dem Wasser, das mit Kupfer versetzt ist.
      \begin{table}
        \caption{Optimale Einstellung für die Parameter der Spektrometerapparatur für die mit Kupfer versetzte Wasserprobe.}
        \label{tab:alignment-H2OCu_param}
        \input{table/align-H2OCu.tex}
      \end{table}
      \begin{figure}
        \includegraphics{graphic/align-T2D-spinEcho.pdf}
        \caption{Spannungssignal erzeugt durch die Magnetisierung in $y$-Richtung in Abhängigkeit von der Zeit $t$ für die Wasserprobe mit den magnetischen Zentren.}
        \label{fig:alignment}
      \end{figure}

      Für die Wasserprobe aus destillierten Wasser müssen die Parameter des Spektrometers verändert werden; die entsprechenden Werte befinden sich in Tabelle \ref{tab:align-H2O_param}.
      Die Variablenbedeutungen ändern sich im Vergleich zu Tabelle \ref{tab:alignment-H2OCu_param} nicht; es kommt jedoch noch die Dauer des B-Pulses $t_{\t{B}}$ hinzu, da mit dieser Probe nun die weiteren Messungen durchgeführt werden.
      \begin{table}
        \caption{Optimale Einstellung für die Parameter der Spektrometerapparatur für die Probe mit destilliertem Wasser.}
        \label{tab:align-H2O_param}
        \input{table/align-H2O.tex}
      \end{table}
      \FloatBarrier
    \subsection{Bestimmung von \texorpdfstring{$T_1$}{T₁}}
      Tabelle \ref{tab:T1-measure} enthält die Messwerte für die Bestimmung der Relaxationszeit $T_1$, dabei stehen die Variablen $τ$ für den Abstand des A-Pulses (\SI{180}{\degree}) vom B-Puls (\SI{90}{\degree}) und $U_z$ für die Höhe des nach dem A-Puls auftretenden, zu $M_z$ proportionalen Signals.
      \begin{table}
        \caption{Messwerte für die Bestimmung von $T_1$.}
        \label{tab:T1-measure}
        \input{table/T1-measure.tex}
      \end{table}


      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/T1-measure.pdf}
        \caption{Linearisierte Messwerte aus Tabelle \ref{tab:T1-measure} und Regressionsgerade zu Bestimmung der Relaxationszeit $T_1$.}
        \label{fig:T1-result}
      \end{figure}
      Gemäß Formel \eqref{eqn:T1-theory} werden die Werte aus Tabelle \ref{tab:T1-measure} linearisiert, das heißt
      \begin{eqn}
        y = \ln.\frac{M_0 - \f{M_z}(τ)}{2 M_0}.
      \end{eqn}
      gegen $x = τ$ aufgetragen; dies wird in Abbildung \ref{fig:T1-result} geplottet.
      Auf diesen Werten wird eine lineare Ausgleichsrechnung mit der Formel
      \begin{eqn}
        y = A τ + B
      \end{eqn}
      ausgeführt; die Amplitude $M_0$, die der Sättigungswert des Ausgangssignals ist, und die Ergebnisse der Ausgleichsrechnung sind in Tabelle \ref{tab:T1-result} zu finden.
      Des Weiteren befindet sich dort der damit ermittelte Wert für die Relaxationszeit
      \begin{eqn}
        T_1 = - \frac{1}{A} \., \label{eqn:T1}
      \end{eqn}
      sowie ein Literaturwert \cite{T12}.
      \begin{table}
        \caption{Sättigungsamplitude $M_0$, Fitparameter der linearen Regression (Gleichung \eqref{eqn:T1}) und die sich daraus ergebende Relaxationszeit $T_1$, sowie ein Literaturwert dafür \cite{T12}.}
        \label{tab:T1-result}
        \input{table/T1-result.tex}
      \end{table}
      \FloatBarrier
    \subsection{Das Ausgangssignal der \texorpdfstring{Carr\--Purcell}{Carr–Purcell} Methode und die Vermessung der Relaxationszeit \texorpdfstring{$T_2$}{T₂} mit der \texorpdfstring{Meiboom\--Gill}{Meiboom–Gill} Methode}
    In Abbildung \ref{fig:T2-CP} befindet sich das Ausgangssignal $U_y$ bei Anwendung der Carr\--Purcell\-/Methode (Abstand zwischen A- und B-Puls $τ_1 = \SI{3}{\milli\second}$); Abbildung \ref{fig:T2-MG} enthält das Ausgangssignal $U_y$, das bei Anwenden der Meiboom\--Gill Methode ($τ_1 = \SI{22}{\milli\second}$) aufgezeichnet wurde.
      \begin{figure}
        \includegraphics{graphic/T2-CP.pdf}
        \caption{Ausgangssignal bei der Carr\--Purcell-Methode.}
        \label{fig:T2-CP}
      \end{figure}

      Da, wie schon in der Theorie erwähnt, durch die Meiboom\--Gill Methode eine genauere Bestimmung der Relaxationszeit $T_2$ möglich ist, wird nur dieses Signal diesbezüglich ausgewertet.
      Zunächst werden die Minima ermittelt; auf Basis der so ermittelten Datenpunkte wird für die Meiboom\--Gill-Methode ein Fit mit der Funktion
      \begin{eqn}
        \f{U_{\t{MG}}}(t) = - A \exp(- B t) \label{eqn:fit-T2-MG}
      \end{eqn}
      ausgeführt.
      Die Abklingzeit $T_2$ lässt sich durch die Beziehung
      \begin{eqn}
        T_2 = \frac{1}{B}
      \end{eqn}
      bestimmen.

      Die Parameter der Ausgleichrechnung und die ermittelte Relaxationszeit, sowie ein Literaturwert \cite{T12} befinden sich in Tabelle \ref{tab:T2-MG}.
      Die Datenpunkte, die ermittelten Minima und die gefittete Kurve sind in Abbildung \ref{fig:T2-MG} aufgetragen.
      \begin{figure}
        \includegraphics{graphic/T2-MG.pdf}
        \caption{Ausgangssignal bei der Meiboom\--Gill-Methode, ermittelte Minima und Fitkurve (Gleichung \eqref{eqn:fit-T2-MG}) durch diese Datenpunkte.}
        \label{fig:T2-MG}
      \end{figure}
      \begin{table}
        \caption{Ergebnisse für die Fitparameter und die Relaxationszeit bei dem Signal der Meiboom\--Gill-Methode aus Gleichung \eqref{eqn:fit-T2-MG}. Der Literaturwert für $T_2$ ist aus \cite{T12} entnommen.}
        \label{tab:T2-MG}
        \input{table/T2-MG.tex}
      \end{table}
      \FloatBarrier
    \subsection{Bestimmung der Halbwertsbreite} \label{FWHM}
      Ein Plot des Spinechosignals zur Bestimmung der Halbwertsbreite befindet sich in Abbildung \ref{fig:FWHM}.
      Die Halbwertsbreite wird mit einem Messprogramm des Oszillographen bei $τ_1 = \SI{5}{\milli\second}$ bestimmt und beträgt bei $18$ Messungen im Mittel $\SI{133.8 ± 0.5}{\micro\second}$. %from scope_10.txt
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/FWHM.pdf}
        \caption{Spinechosignal, aus dem der Oszillograph die Halbwertsbreite bestimmt.}
        \label{fig:FWHM}
      \end{figure}
      \FloatBarrier
    \subsection{Bestimmung der Diffusionskonstante}
      Tabelle \ref{tab:diffusion-measurement} enthält die Messwerte für die Bestimmung der Diffusionskonstante, dabei ist $τ$ der zeitliche Abstand zwischen A- (\SI{180}{\degree}) und B-Puls (\SI{90}{\degree}) und $U_y$ das Echosignal nach dem A-Puls, das proportional zu $M_y$ ist.
      \begin{table}
        \caption{Messwerte für die Bestimmung der Difussionskonstanten.}
        \label{tab:diffusion-measurement}
        \input{table/diffusion-measurement.tex}
      \end{table}

      Die Werte aus der Messreihe für die Diffusionskonstante (Tabelle \ref{tab:diffusion-measurement}) werden linearisiert, das heißt
      \begin{eqn}
        y = \ln.\frac{\f{U_y}(t)}{M_0}. + \frac{t}{T_2}
      \end{eqn}
      gegen $x = t³$ in Abbildung \ref{fig:diffusion-result} aufgetragen, wobei $t = 2τ$ ist.
      Dies geschieht auf Basis von Formel \eqref{eqn:diffusion-theory} und der Vorraussetzung
      \begin{eqn}
        T_2^3 \gg \frac{12}{D γ² G²} \.,
      \end{eqn}
      das heißt die Zeitabhängigkeit der Magnetisierung in $y$-Richtung ist im Wesentlichen durch den Diffusionsterm bestimmt.

      Falls sich, wie in diesem Fall, das Spin-Echo durch
      \begin{eqn}
        \f{U}(α) = \frac{2 \BesselJ_1(α)}{α}
      \end{eqn}
      beschreiben lässt, wobei
      \begin{eqn}
        α = \frac{1}{2} γ d t
      \end{eqn}
      und $\BesselJ_1(α)$ die Besselfunktion erster Ordnung ist, gilt für den Feldgradienten
      \begin{eqn}
        G = \frac{8.8}{d γ t_{\frac{1}{2}}} \.,
      \end{eqn}
      mit dem Probendurchmesser $d = \SI{4.4}{\milli\meter}$ und dem gyromagnetischen Verhältnis $γ$.
      Das Ergebnis für den Feldgradienten ist in Tabelle \ref{tab:diffusion-result} zu finden.
      Die Halbwertsbreite befindet sich in Kapitel \ref{FWHM}.

      Damit wird eine lineare Ausgleichsrechnung durchgeführt, deren Parameter $A$ und $B$ in Tabelle \ref{tab:diffusion-result} stehen.
      Aus der Steigung $A$ ergibt sich die Diffusionskonstante
      \begin{eqn}
        D = - \frac{12 A}{γ² G²} \.,
      \end{eqn}
      die ebenfalls in dieser Tabelle steht.
      Zum Vergleich ist dort auch ein Literaturwert bei einer Temperatur von \SI{20}{\celsius} angegeben \cite{D}.
      \begin{figure}
        \includegraphics{graphic/diffusion-result.pdf}
        \caption{Linearisierte Messwerte und Regressionsgerade zu Bestimmung der Diffusionskonstanten $D$.}
        \label{fig:diffusion-result}
      \end{figure}
      \begin{table}
        \caption{Ergebnis der linearen Regressionsrechnung, Verwendete Konstanten zur Berechnung von $G$ und $D$, Ergebnis für die Konstante $G$ und sich daraus ergebende Diffusionskonstante, sowie ein Literaturwert bei einer Temperatur von \SI{20}{\celsius} \cite{D}.}
        \label{tab:diffusion-result}
        \input{build/table/diffusion-result.tex}
      \end{table}
      \FloatBarrier
    \subsection{Diverse Abschätzungen des Molekülradius}
      \subsubsection{Abschätzung mit der stokeschen Formel}
        Der Molekülradius von Wasser kann aus der stokeschen Formel
        \begin{eqn}
          r = \frac{k_{\t{B}} T}{6 \PI D η} \eqlabel{eqn:stokes}
        \end{eqn}
        gewonnen werden; dafür wird zusätzlich noch die Viskosität $η$ und die Temperatur $T$ benötigt.

        Die Viskosität wird aus der Messung mit dem Viskosimeter bestimmt.
        Dazu wird der Zusammenhang
        \begin{eqn}
          \f{η}(T) = ρ α \g(t - δ)
        \end{eqn}
        zwischen der Viskosität $η$ und der gemessenen Durchflusszeit $t$ benutzt.
        Dabei ist $δ$ eine Proportionalitätskonstante, die aus der Anleitung für das Viskosimeter abgeschätzt wird.
        Da dort nur der Wert für eine Durchlaufzeit von $t = \SI{1000}{\second}$ angegeben ist und dies der letzte angegebene Wert ist, muss der Wert für die hier gemessenen $\SI{1034.45}{\second}$ abgeschätzt werden und kann nicht, wie in der Anleitung \cite{anleitung59} angegeben, linear interpoliert werden.
        Unter Berücksichtigung der Steigung zwischen den dort angegebenen zwei letzten Punkten, der sich für $δ$ einstellenden Sättigung und dem Rahmen der Messgenauigkeit wird der Wert für $δ$ im Folgenden als \SI{0.43}{\second} angenommen.

        Schließlich wird noch die Temperatur an der Versuchsapparatur benötigt, die mit einem Stabthermometer ermittelt wird.

        Der Radius, der sich aus \eqref{eqn:stokes} ergibt, und die dafür verwendeten Größen befinden sich in Tabelle \ref{tab:radius}.
      \subsubsection{Abschätzung durch partielle Füllung einer hexagonalen Struktur}
        In dieser Näherung soll das Wassermolekül als Kugel betrachtet werden, die mit einer Packungsdichte von $\SI{74.048}{\percent}$ \cite{gross} eine hexagonale Einheitszelle ausfüllen.
        Das Volumen der hexagonalen Einheitszelle in der Flüssigkeit wird durch das molekulare Gewicht $M$ \cite{nist} der Wassermoleküle und der Dichte $ρ$ der Flüssigkeit festgelegt.
        In jeder hexagonalen Einheitszelle befinden sich zwei Moleküle.
        Somit ergibt sich nach Umstellen für den Radius
        \begin{eqn}
          r = \sqrt[3]{0.74048\frac{3M}{8 \PI ρ}} \.;
        \end{eqn}
        das errechnete Ergebnis befindet sich in Tabelle \ref{tab:radius}.
      \subsubsection{Abschätzung durch die Betrachtung des van-der-Waals Gases am kritischen Punkt}
        Für das Van-der-Waals Gas gilt am kritischen Punkt die universelle Gleichung \cite{kierfeld}
        \begin{eqns}
          8 p_{\t{k}} b &=& k_{\t{B}} T
          \itext{mit dem Virialkoeffizienten}
          b &=& 4 \frac{4}{3} \PI r³\.
        \end{eqns}
        Diese Beziehung lässt sich nach dem Molekülradius
        \begin{eqn}
          r = \sqrt[3]{\frac{3 k_{\t{B}} T}{16 \cdot 8 \PI p_{\t{k}}}}
        \end{eqn}
        umformen.
        Das errechnete Ergebnis, sowie die dafür verwendeten Größen stehen in Tabelle \ref{tab:radius}.
      \begin{table}
        \caption{Verwendete Konstanten \cite{scipy}, um die Viskosität zu berechnen, berechnete Viskosität und Radien, die unter den in diesem Kapitel beschriebenen Annahmen berechnet wurden. Das molekulare Gewicht, der kritische Druck und die kritische Temperatur sind aus \cite{nist}.}
        \label{tab:radius}
        \input{table/radius.tex}
      \end{table}
  \section{Diskussion}
    Die Bestimmung von $T_1$ hat gut funktioniert, die Fehler des Fits sind klein; der letzte Messwert wurde für den Fit ausgelassen.
    Ein Vergleich mit dem Literaturwert ergibt auch hier die selbe Größenordnung aber eine Abweichung um einen Faktor 2.
    Die Zeit $T_1$ ist größer als die für $T_2$, da sich schnell bewegende Wassermoleküle ein gleichmäßige magnetische Umgebung für alle Wasserstoffkerne erzeugen, sodass die Dephasierung nach einem \SI{90}{\degree}-Puls langsam ist \cite{tissue}.

    Auch beim Vergleich der Abbildungen \ref{fig:T2-CP} und \ref{fig:T2-MG} bestätigt sich die Annahme auch experimentell, dass die Relaxationszeit bei der Carr\--Purcell\-/Methode kleiner ist als die bei der Meiboom\--Gill\-/Methode.
    Eine Bestimmung der Relaxationszeit durch die Meiboom\--Gill\-/Methode erweist sich daher als sinnvoll.
    Des Weiteren fällt auf, dass die Fitparameter in Tabelle \ref{tab:T2-MG} kleine Fehler haben.
    Die Minima werden in Abbildung \ref{fig:T2-MG} soweit bestimmt, bis sie nicht mehr vom Rauschen zu unterscheiden sind.
    Ein Vergleich mit dem Literaturwert für Liquor, das hauptsächlich Wasser ist, liefert zwar die selbe Größenordnung aber eine Abweichung um ungefähr einen Faktor 2.


    Bei der Bestimmung der Diffusionskonstanten $D$ tritt ein kleiner Fehler in den Fitergebnissen auf.
    Ein Vergleich mit dem Literaturwert ergibt eine Abweichung um einen Faktor $1.6$.

    Der Molekülradius von Wasser ergibt für die beiden Abschätzungen eines van\-/der\-/Waals\-/Gases und der einer hexagonalen Einheitszelle ungefähr gleiche Werte.
    Der aus den Messungen stammende Stokes-Wert für den Molekülradius ist um eine Größenordnungen kleiner.
    Ein Vergleich mit dem Literaturwert ergibt, dass die Abschätzungen in der selben Größenordnung wie der Literaturwert liegen, sich jedoch um einen Faktor 2 unterscheiden.
    Der stokesche Radius weicht um eine Größenordnung vom Literaturwert ab.
  \makebibliography
\end{document}
