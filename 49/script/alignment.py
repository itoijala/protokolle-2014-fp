import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

ν_i, φ_i, t_A_i, P_i, I_x_i, I_y_i, I_z_i, I_z2_i = np.loadtxt('data/align-init_param', unpack=True, skiprows=2)

ν_i *= 1e6
t_A_i *= 1e-6

tab = lt.SymbolRowTable()
tab.add_si_row('ν', ν_i * 1e-6, r'\mega\hertz', places=5)
tab.add_si_row('φ', φ_i, r'\degree', places=0)
tab.add_si_row(r't_{\t{A}}', t_A_i * 1e6, r'\micro\second', places=2)
tab.add_si_row('P', P_i, r'\second', places=1)
tab.add_si_row('I_x', I_x_i, r'\ampere', places=2)
tab.add_si_row('I_y', I_y_i, r'\ampere',places=2)
tab.add_si_row('I_z', I_z_i, r'\ampere', places=2)
tab.add_si_row('I_{z²}', I_z2_i, r'\ampere', places=2)
tab.savetable('build/table/align-init_param.tex')

ν_d, φ_d, t_A_d, t_B_d, P_d, B_x_d, B_y_d, B_z_d, B_Z2_d = np.loadtxt('data/align-H2O', unpack=True, skiprows=2)

ν_d *= 1e6
t_A_d *= 1e-6
t_B_d *= 1e-6

tab = lt.SymbolRowTable()
tab.add_si_row('ν', ν_d * 1e-6, r'\mega\hertz', places=5)
tab.add_si_row('φ', φ_d, r'\degree', places=0)
tab.add_si_row(r't_{\t{A}}', t_A_d * 1e6, r'\micro\second', places=2)
tab.add_si_row(r't_{\t{B}}', t_B_d * 1e6, r'\micro\second', places=2)
tab.add_si_row('P', P_d, r'\second', places=1)
tab.add_si_row('I_x', B_x_d, r'\ampere', places=2)
tab.add_si_row('I_y', B_y_d, r'\ampere',places=2)
tab.add_si_row('I_z', B_z_d, r'\ampere', places=2)
tab.add_si_row('I_{Z²}', B_Z2_d,  r'\ampere',places=2)
tab.savetable('build/table/align-H2O.tex')

ν_m, φ_m, t_A_m, P_m, I_x_m, I_y_m, I_z_m, I_z2_m = np.loadtxt('data/align-H2OCu', unpack=True, skiprows=2)

ν_m *= 1e6
t_A_m *= 1e-6

tab = lt.SymbolRowTable()
tab.add_si_row('ν', ν_m * 1e-6, r'\mega\hertz', places=5)
tab.add_si_row('φ', φ_m, r'\degree', places=0)
tab.add_si_row(r't_{\t{A}}', t_A_m * 1e6, r'\micro\second', places=2)
tab.add_si_row('P', P_m, r'\second', places=1)
tab.add_si_row('I_x', I_x_m, r'\ampere', places=2)
tab.add_si_row('I_y', I_y_m, r'\ampere',places=2)
tab.add_si_row('I_z', I_z_m, r'\ampere', places=2)
tab.add_si_row('I_{z²}', I_z2_m,  r'\ampere',places=2)
tab.savetable('build/table/align-H2OCu.tex')

t_osc, V = np.loadtxt('data/align-osci_data', unpack=True, skiprows=2)

fig, ax = plt.subplots(1, 1)
ax.plot(t_osc * 1e3, V, 'b-')
ax.set_xlabel(r'$\silabel{t}{\milli\second}$')
ax.set_ylabel(r'$\silabel{U_y}{\volt}$')
ax.set_xlim(0, (t_osc * 1e3)[-1])
ax.set_ylim(0, 8)
fig.savefig('build/graphic/align-T2D-spinEcho.pdf')
