import pickle

import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt
from labtools.peakdetect import peakdetect

t_CP, U_CP = np.loadtxt('data/T2-CP', unpack=True, skiprows=2)
T2_lit = np.loadtxt('data/T2-lit', skiprows=3)

T2_lit *= 1e-3

fig, ax = plt.subplots(1, 1)
ax.plot(t_CP, U_CP, 'b-')
ax.set_xlabel(r'$\silabel{t}{\second}$')
ax.set_ylabel(r'$\silabel{U_{y, \.\t{CP}}}{\volt}$')
ax.set_xlim(0, t_CP[-1])
ax.set_ylim(-10.5, 10.5)
fig.savefig('build/graphic/T2-CP.pdf')

t_MG, U_MG = np.loadtxt('data/T2-MG', unpack=True, skiprows=2)

_, U_min_MG = peakdetect(U_MG, t_MG,lookahead=45)
t_min_MG, U_min_MG = np.array(U_min_MG).T

def MG(t, A, B):
    return A * np.exp(-B * t)

p0 = (-7, 4)
A_min, B_min = lt.curve_fit(MG, t_min_MG[:-1], U_min_MG[:-1], p0 = p0)

T2 = 1 / B_min

tab = lt.SymbolRowTable()
tab.add_si_row('A', A_min, '\second')
tab.add_si_row('B', B_min, '\per\second')
tab.add_hrule()
tab.add_si_row(r'T_{2, \t{MG}}', T2, '\second')
tab.add_si_row(r'T_{2, \t{lit}}', T2_lit, '\second', places=3)
tab.savetable('build/table/T2-MG.tex')

fig, ax = plt.subplots(1, 1)
ax.plot(t_MG, U_MG, 'b-', label='Ausgangssignal')
ax.plot(t_min_MG[:-1], U_min_MG[:-1], 'gx', label='Minima')
ax.plot(t_MG, MG(t_MG, *lt.noms(A_min, B_min)), 'r-', label='Fitfunktion')
ax.set_xlabel(r'$\silabel{t}{\second}$')
ax.set_ylabel(r'$\silabel{U_{y,\.\t{MG}}}{\volt}$')
ax.set_ylim(-10, 2)
ax.set_xlim(0, t_MG[-1])
ax.legend(loc='lower right')
fig.savefig('build/graphic/T2-MG.pdf')

pickle.dump(T2, open('build/result/T2.pickle', 'wb'))
