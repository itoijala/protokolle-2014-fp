import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt
from labtools.peakdetect import peakdetect

t, U = np.loadtxt('data/FWHM-osci-data', skiprows=3, unpack=True)

fig, ax = plt.subplots(1, 1)
ax.plot(t * 1e3, U ,'b-')
ax.set_xlabel(r'$\silabel{t}{\milli\second}$')
ax.set_ylabel(r'$\silabel{U_y}{\volt}$')
ax.set_xlim(9.7, 10.5)
ax.set_ylim(-1, 6)
fig.savefig('build/graphic/FWHM.pdf')
