import pickle
import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt
from labtools.peakdetect import peakdetect

α, δ, ρ, T, t, M, p_k, δp_k, T_k, δT_k = np.loadtxt('data/radius', unpack=True, skiprows=3)
r_lit = np.loadtxt('data/r-lit', skiprows=3)

r_lit *= 1e-10

p_k = unc.ufloat(p_k, δp_k)
T_k = unc.ufloat(T_k, δT_k)

ρ *= 1e3
η = ρ * α * (t - δ)

k_B, N_A = lt.constant('Boltzmann constant', 'Avogadro constant')

D = pickle.load(open('build/result/D.pickle', 'rb'))
M *=  1e-3
m = M / N_A
p_k *= 1e6

r_1 = k_B * T / 6 / np.pi / D / η
r_2 =  (0.74048 *  3 * M / 4 / ρ / N_A / np.pi)**(1 / 3)
r_3 =  (3 * k_B * T_k / 128 / np.pi / p_k)**(1 / 3)

tab = lt.SymbolRowTable()
tab.add_si_row(r'k_{\t{B}}', k_B * 1e23, r'\joule\per\kelvin', exp='e-23')
tab.add_si_row(r'N_{\t{A}}', N_A * 1e-23, r'\per\mol', exp='e23')
tab.add_hrule()
tab.add_si_row('ρ', ρ * 1e-3, r'\gram\per\cubic\centi\meter')
tab.add_si_row('α', α * 1e10, r'\square\centi\meter\per\square\milli\second')
tab.add_si_row('M_{\ce{H2O}}', M * 1e3, r'\gram\per\mol', figures=6)
tab.add_si_row(r'p_{\t{k}}', p_k * 1e-6, r'\pascal')
tab.add_si_row(r'T_{\t{k}}', T_k, r'\kelvin')
tab.add_hrule()
tab.add_si_row('T', T, r'\kelvin', places=0)
tab.add_si_row('δ', δ, r'\second')
tab.add_si_row('η', η * 1e3, r'\gram\per\meter\per\second', figures=3)
tab.add_hrule()
tab.add_si_row(r'r_{\t{Stokes}}', r_1 * 1e12, r'\pico\meter')
tab.add_si_row(r'r_{\t{hex}}', lt.noms(r_2) * 1e12, r'\pico\meter', places=1)
tab.add_si_row(r'r_{\t{Gas}}', r_3 * 1e12, r'\pico\meter')
tab.add_si_row(r'r_{\t{lit}}', r_lit * 1e12, r'\pico\meter', places=2)
tab.savetable('build/table/radius.tex')
