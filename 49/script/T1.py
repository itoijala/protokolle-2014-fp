import pickle

import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt
from labtools.peakdetect import peakdetect

T1_lit = np.loadtxt('data/T1-lit', skiprows=3)

T1_lit *= 1e-3


τ, M = np.loadtxt('data/T1-measure', unpack=True, skiprows=3)

τ *= 1e-3

tab = lt.SymbolColumnTable()
tab.add_si_column('τ', τ[:-2] * 1e3, r'\milli\second', places=0)
tab.add_si_column('U_z', M[:-2] , r'\volt', places=2)
tab.savetable('build/table/T1-measure.tex')

M0 = M[-1]

A, B = lt.linregress(τ[:-3], np.log((M0 - M[:-3]) / 2 / M0))

fig, ax = plt.subplots(1, 1)
ax.plot(τ[:-3], np.log((M0 - M[:-3]) / 2 / M0), 'rx', label='Messwerte, die für den Fit verwendet werden')
ax.plot(τ[-3:-2], np.log((M0 - M[-3:-2]) / 2 / M0), 'gx', label ='Messwert, der nicht für den Fit verwendet wird')
x = np.linspace(0, 2.3, 500)
ax.plot(x, lt.noms(A * x + B), 'b-', label='Regressionsgerade')
ax.set_xlabel(r'$\silabel{τ}{\second}$')
ax.set_ylabel(r'$\ln.\frac{M_0 - \f{M_z}(τ)}{2 M_0}.$')
ax.set_xlim(0, 2.2)
ax.legend(loc='best')
fig.savefig('build/graphic/T1-measure.pdf')

tab = lt.SymbolRowTable()
tab.add_si_row('M_0', M0, r'\volt', places=2)
tab.add_hrule()
tab.add_si_row('A', A, '\per\second')
tab.add_si_row('B', B, '\second')
tab.add_hrule()
tab.add_si_row('T_1', -1 / A, '\second')
tab.add_si_row(r'T_{1, \t{lit}}', T1_lit, '\second', places=3)
tab.savetable('build/table/T1-result.tex')
