import pickle
import numpy as np
import scipy.constants as scc

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

τ, U = np.loadtxt('data/diffusion', skiprows=2, unpack=True)
τ *= 1e-3
D_lit = np.loadtxt('data/D-lit', skiprows=3)

specialplaces = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3]

tab = lt.SymbolColumnTable()
tab.add_si_column('τ', τ[:10] * 1e3, r'\milli\second', places=0)
tab.add_si_column('U_y', U[:10], r'\volt', places=specialplaces[:10])
tab.add(lt.VerticalSpace())
tab.add_si_column('τ', τ[10:] * 1e3, r'\milli\second', places=0)
tab.add_si_column('U_y', U[10:], r'\volt', places=specialplaces[10:])
tab.savetable('build/table/diffusion-measurement.tex')

T2 = pickle.load(open('build/result/T2.pickle', 'rb'))

U0 = U[0]
U /= U0
t = τ
t = 2 * τ

A, B = lt.linregress(t**3, np.log(U) + t / T2)

fig, ax = plt.subplots(1, 1)
x = np.linspace(-1e-4, 1e-4, 500)
ax.plot(t**3 * 1e6, lt.noms(np.log(U) + t / T2), 'rx', zorder=4, label='Messwerte')
ax.plot(x * 1e6, lt.noms(A * x  + B), 'b-', label='Regressionsgerade')
ax.set_xlabel(r'$\silabel{t^3}[e-6]{\cubic\second}$')
ax.set_ylabel(r'$\ln.\frac{\f{U_y}(t)}{U_0}. + \frac{t}{T_2}$')
ax.set_xlim(0, 9)
ax.legend(loc='best')
fig.savefig('build/graphic/diffusion-result.pdf')

t12 = unc.ufloat(133.8, 0.5) * 1e-6
d = 4.4e-3
γ = lt.constant('proton gyromagn. ratio')
G = 8.8 / d / γ / t12

D = -A * 12 / γ**2 / G**2

pickle.dump(D, open('build/result/D.pickle', 'wb'))

tab = lt.SymbolRowTable()
tab.add_si_row('A', A * 1e-5, r'\per\cubic\second', exp='e5')
tab.add_si_row('B', B)
tab.add_hrule()
tab.add_si_row('d', d * 1e3, r'\milli\meter')
tab.add_si_row('γ', γ, r'\per\second\per\tesla')
tab.add_hrule()
tab.add_si_row('G', G * 1e3, r'\tesla\per\meter', exp='e-3')
tab.add_si_row('D', D * 1e9, r'\square\meter\per\second', exp='e-9')
tab.add_si_row(r'D_{\t{lit}}', D_lit * 1e9, r'\square\meter\per\second', exp='e-9', places=3)
tab.savetable('build/table/diffusion-result.tex')
