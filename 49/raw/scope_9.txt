ANALOG
Ch 1 Scale 1.00V/, Pos 2.07500V, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 10.000 : 1, Skew 0.0s

TRIGGER
Sweep Mode Normal, Coup DC, Noise Rej Off, HF Rej Off, Holdoff 40.0ns
Mode Edge, Source Ch 1, Slope Rising, Level 0.0V

HORIZONTAL
Mode Normal, Ref Center, Main Scale 100.0us/, Main Delay 10.037000000ms

ACQUISITION
Mode Normal, Realtime On, Vectors On, Persistence Off

MEASUREMENTS
+ Width(1), Cur 134.18us, Mean 133.90us, Min 132.54us, Max 134.82us, Std Dev 530ns, Count 15

