\input{header/report.tex}

\SetExperimentNumber{46}

\addbibresource{lit.bib}

\begin{document}
  \maketitlepage{Faraday-Effekt an Halbleitern}{16.12.2013}{06.01.2014}
  \section{Ziel}
    Ziel des Versuches ist es, die effektive Masse der Leitungselektronen in Galliumarsenid mittels Faraday-Rotation zu ermitteln.
  \section{Theorie}
    \subsection{Effektive Masse}
      Für die Beschreibung vieler physikalischer Effekte kann die komplizierte Bandstruktur von Kristallen in der Nähe der unteren Bandkante durch eine harmonische Näherung vereinfacht werden.
      Eine schematische Bandstruktur ist in Abbildung \ref{fig:bandstruktur} zu finden, dabei bezeichnet $\v{k}$ die Wellenzahl und $\f{E}(\v{k})$ die Elektronenenergie, die im Folgenden als $\f{ε}(\v{k})$ bezeichnet wird.
      \begin{figure}
        \includegraphics[width=\textwidth]{build/graphic/bandstruktur.pdf}
        \caption{Vereinfachte Darstellung der Bandstruktur eines Festkörpers \cite{anleitung46}.}
        \label{fig:bandstruktur}
      \end{figure}
      Das Koordinatensystem wird so gelegt, dass das Minimum des Leitungsbandes bei $\v{k} = 0$ auftritt.
      Eine Entwicklung der Elektronenenergie in der Nähe des Minimums ergibt
      \begin{eqn}
        \f{ε}(\v{k}) = \f{ε}(0) + \frac{1}{2} \sum_{i = 1}^{3} \underbrace{\eval{\p{k_i}[2]{ε};}_{\v{k} = 0}}_{α_i} k_i² + \LandauO(\v{k}³) \label{eqn:taylor} \..
      \end{eqn}
      Damit wird aus einem dimensionsanalytischen Vergleich mit
      \begin{eqn}
        ε = \frac{ℏ² k²}{2 m}
      \end{eqn}
      die effektive Masse
      \begin{eqn}
        m_{i}^{*} \equiv \frac{ℏ²}{α_i}
      \end{eqn}
      definiert.
      Mit dieser Größe lässt sich Gleichung \eqref{eqn:taylor} in
      \begin{eqn}
        \f{ε}(\v{k}) = \f{ε}(0) + \sum_{i = 1}^{3} \frac{ℏ² k_i²}{2 m_i^{*}}
      \end{eqn}
      umformen.
      Diese ellipsoidförmige Energieflächen vereinfachen sich bei Isotropie der effektiven Masse zu
      \begin{eqn}
        \f{ε}(\v{k}) = \f{ε}(0) + \frac{ℏ² \v{k}²}{2 m^{*}} \.,
      \end{eqn}
      was als kugelförmige Energiefläche zu deuten ist.

      Das Einführen der effektiven Masse ermöglicht eine Behandlung, die analog zu der bei freien Teilchen ist, das heißt das periodische Gitterpotential wird in die effektive Masse definiert.
      Insbesondere lässt sich die Faraday-Rotation unter Annahme kugelförmiger Energieflächen und parabolischer Energiebänder klassisch berechnen.
    \subsection{Zirkulare Doppelbrechung}
      Zirkulare Doppelbrechung beschreibt die Fähigkeit eines Materials, die Polarisationsebene eines linear polarisierten Lichtstrahles bei der Transmission um den Winkel $θ$ zu drehen.
      Qualitativ resultiert diese aus den unterschiedlichen Phasengeschwindigkeiten von links- und rechtszirkular polarisiertem Licht.

      Um den Drehwinkel zu berechnen, wird das elektrische Feld, welches durch ein Kristall der Länge $L$ in $z$-Richtung propagiert, in links- und rechtspolarisierte Anteile aufgeteilt:
      \begin{eqn}
        \f{\v{E}}(z) = \frac{1}{2} \g(\f{\v{E}_{\t{R}}}(z) + \f{\v{E}_{\t{L}}}(z)) \.. \label{eqn:E}
      \end{eqn}
     Für die Anteile gilt
     \begin{eqn}
       \f{\v{E}_{\t{R/L}}}(z) = E_0 \g(\uv{x} ∓ \I \uv{y}) \E^{\I k_{\t{R/L}} z} \.. \label{eqn:rl-pol}
     \end{eqn}
     Damit folgt, dass die Polarisation der Welle beim Eintritt in den Kristall parallel zur $x$-Richtung liegt.
     Nach Durchlaufen der Kristalllänge $L$ folgt nach Einsetzen von \eqref{eqn:rl-pol} in \eqref{eqn:E} für das elektrische Feld
     \begin{eqn}
       \f{\v{E}}(L) = E_0 \E^{\I ψ} \g(\cos(θ) \uv{x} + \sin(θ) \uv{y}) \.,
     \end{eqn}
     mit den Abkürzungen
     \begin{eqn}
       ψ \equiv \frac{L}{2} \g(k_\t{R} + k_\t{L}) \., \qquad θ \equiv \frac{L}{2} \g(k_\t{R} - k_\t{L}) \..
     \end{eqn}
     Der Drehwinkel kann auch durch Phasengeschwindigkeiten $v$ oder Brechungsindices $n$ ausgedrückt werden:
     \begin{eqn}
       θ = \frac{L ω}{2} \g(\frac{1}{v_{\t{R}}} - \frac{1}{v_{\t{L}}}) = \frac{Lω}{2 c} \g(n_{\t{R}} - n_{\t{L}}) \.,
     \end{eqn}
     wobei $ω$ die Frequenz und $c$ die Vakuumlichtgeschwindigkeit sind.

     Mikroskopisch entsteht die zirkulare Doppelbrechung durch elektrische Dipolmomente, die einerseits durch die auf den Gitterplätzen sitzenden Atome und zum anderen durch die Bandelektronen in Wechselwirkung mit den Atomrümpfen erzeugt werden können.
     Relevant sind dabei nur induzierte Dipole, denn permanente Dipole sind wegen ihrer langen Relaxationszeit zu träge, um der Lichtquelle mit einer Frequenz von $ω \approx \SI{e15}{\hertz}$ zu folgen.
     Die Gesamtheit der Dipole erzeugt eine Polarisation, die falls das elektrische Feld nicht zu groß ist, linear in $E$ ist
     \begin{eqn}
       P_i = ε_0 \T{χ}{_i_j} E_j \.. \label{eqn:polarisation}
     \end{eqn}
     Der Tensor $χ$ ist die dielektrische Suszeptibilität, in isotropem Material wird diese zu einem Skalar.

     In den meisten Fällen lässt sich durch die Symmetrie von $χ$ eine Hauptachsentransformation finden, die die Suszeptibilität diagonalisiert.
     Ist der Tensor jedoch schiefsymmetrisch mit komplexen Nebendiagonalelementen, so ist dies ein Indikator für doppeltbrechende Materie.
     Für den einfachsten Fall $\g(\T{χ}{_x_y} = - \T{χ}{_y_x})$ gilt beispielsweise
     \begin{eqn}
       θ \approx \frac{L ω}{2 c²} v_{\t{Ph}} \T{χ}{_x_y} = \frac{L ω}{2 c n} \T{χ}{_x_y} \label{eqn:drehwink_allg} \..
     \end{eqn}
    \subsection{Rotationswinkel beim Faraday-Effekt}
      Materie, bei der die Suszeptibilität diagonal ist wird als optisch inaktive Materie bezeichnet.
      Durch den Faraday-Effekt wird beim Anlegen eines äußeren Magnetfeldes die Polarisationsebene jedoch grundsätzlich gedreht.
      Der Einfluss des Magnetfeldes $\v{B}$ auf die Elektronen der Materie kann durch die Bewegungsgleichung
      \begin{eqn}
        m \d{t}[2]{\v{r}}; + K \v{r} = - e \f{\v{E}}(\v{r}) - e \d{t}{\v{r}}; \times \v{B} \label{eqn:bewegungsgl}
      \end{eqn}
      beschrieben werden, wobei $\v{r}$ die Auslenkung des Elektrons aus der Gleichgewichtslage, $K$ eine Konstante, die die Bindung an die Umgebung beschreibt, und $\v{E}$ die elektrische Feldstärke sind.
      Vernachlässigbar sind hier Dämpfungseffekte und der Einfluss des Magnetfeldes der elektromagnetischen Welle.

      Der Ansatz $\f{E}(t) \propto \E^{-\I ω t}$ für das elektrische Feld wird in Gleichung \eqref{eqn:bewegungsgl} eingesetzt; des Weiteren wird angenommen, dass die Polarisation $\v{P} = -N e \v{r}$ proportional zur Auslenkung ist.
      Damit folgt für die Bewegungsgleichung
      \begin{eqn}
        -m ω² \v{P} + K \v{P} = e² N \v{E} + \I e ω \v{P} \times \v{B} \., \label{eqn:beweg2}
      \end{eqn}
      wobei das Magnetfeld in $z$-Richtung zeigt.
      Mit der Beziehung \eqref{eqn:polarisation} werden nicht-diagonale Elemente bei der Suszeptibilität notwendig, wenn Gleichung \eqref{eqn:beweg2} nicht nur triviale Lösungen haben soll.
      Wenn die Elemente unabhängig von den Feldstärkekomponenten $E_x$ und $E_y$ sein sollen und $E_z$ verschwindet, müssen sie imaginär sein.
      Somit ergeben sich für die $x$-Komponente von Gleichung \eqref{eqn:beweg2} der Realteil
      \begin{eqns}
        \g(-m ω² + K) ε_0 \T{χ}{_x_x} &=& Ne² - e ε_0 ω B \T{χ}{_y_x}
        \itext{und für den Imaginärteil}
        \g(-m ω² + K) ε_0 \T{χ}{_x_y} &=& e ε_0 ω B \T{χ}{_x_x} \..
      \end{eqns}
      Analoges gilt für den Realteil und Imaginärteil der $y$-Komponente:
      \begin{eqns}
        \g(-m ω² + K) ε_0 \T{χ}{_x_x} &=& N e² + e ε_0 ω B \T{χ}{_x_y} \label{eqn:y-real} \\
        \g(-m ω² + K) ε_0 \T{χ}{_y_x} &=& -e ε ω B \T{χ}{_x_x} \label{eqn:y-img} \..
      \end{eqns}
      Aus den Realteilen beider Komponenten folgt
      \begin{eqn}
        \T{χ}{_x_y} = - \T{χ}{_y_x} \.. \label{eqn:schiefsymmetrisch}
      \end{eqn}
      Dies zeigt die Schiefsymmetrie des Suszeptibilitätstensors; des Weiteren folgt daraus, dass das Magnetfeld die Symmetrie des zuvor optisch isotropen Materials erniedrigt, da es jetzt doppeltbrechend geworden ist.

      Für die Berechnung des Drehwinkels $θ$ wird aus Gleichungen \eqref{eqn:y-real}, \eqref{eqn:y-img} und \eqref{eqn:schiefsymmetrisch} die Komponente $\T{χ}{_x_y}$ gewonnen und in Gleichung \eqref{eqn:drehwink_allg} eingesetzt, wodurch sich
      \begin{eqn}
        θ = \frac{e³}{2 ε_0 c} \frac{ω²}{\g(-ω² + \frac{K}{m})^{\! 2} - \g(\frac{e B ω}{m})^{\! 2}} \frac{NBL}{m² n} \label{eqn:winkel}
      \end{eqn}
      ergibt.
      In diesem Ausdruck können die Resonanzfrequenz
      \begin{eqns}
        ω_0^2 &=& \frac{K}{m} 
        \itext{und die Zyklotronfrequenz}
        ω_{\t{Z}} &=& \frac{e B}{m}
      \end{eqns}
      identifiziert werden.
      Die Resonanzfrequenz $ω_0$ und die Messfrequenzen liegen bei Halbleitern im nahen Infrarot \SIrange{e14}{e15}{\hertz}, die Zyklotronfrequenz bei \SI{1}{\tesla} bei \SI{e11}{\hertz}, woraus folgt
      \begin{eqn}
        \g(ω_0^2 - ω²)² \gg ω² ω_{\t{Z}} \..
      \end{eqn}
      Damit folgt für den Winkel \eqref{eqn:winkel} in Abhängigkeit der Wellenlänge $λ$ des einfallenden Lichtes
      \begin{eqn}
        \f{θ}(λ) = \frac{2 \PI² e³ c}{ε_0} \frac{1}{m²} \frac{1}{λ² ω_0⁴} \frac{NBL}{n} \..
      \end{eqn}
      Um einen Zusammenhang mit der effektive Masse herzustellen, werden freie Ladungsträger betrachtet.
      Dazu wird in Gleichung \eqref{eqn:winkel} $\frac{K}{m} \rightarrow 0$ geschickt; es ergibt sich mit den erwähnten Näherungen
      \begin{eqn}
        \f{θ_{\t{frei}}}(λ) = \frac{e³}{8 \PI² ε_0 c³} \frac{1}{\g(m^{*})²} λ² \frac{NBL}{n} \.. \label{eqn:faraday-theorie}
      \end{eqn}
  \section{Aufbau}
    \begin{figure}
      \includegraphics[width=\textwidth]{build/graphic/versuchsaufbau.pdf}
      \caption{Schematische Darstellung des Versuchaufbaus \cite{anleitung46}.}
      \label{fig:versuchsaufbau}
    \end{figure}
    Abbildung \ref{fig:versuchsaufbau} zeigt eine schematische Zeichnung des Versuchsaufbaus.
    Als Lichtquelle dient eine Halogen-Lampe, deren Emissionsspektrum überwiegend im nahen Infrarot liegt.
    Interferenzfilter monochromatisieren die emittierte Strahlung.
    Die Glan–Thompson Prismen aus Kalkspat führen dazu, dass das Licht linear polarisiert wird, und sind im sichtbaren Spektralbereich bis zur einer Wellenlänge von etwa \SI{2.8}{\micro\meter} transparent.
    Die scheibenförmige Probe befindet sich in der Symmetrieebene eines großen Elektromagneten, dessen Polschuhe in Längsrichtung durchbohrt sind und in der Mitte einen Luftspalt zum Einführen der Probe enthalten.
    Da das Magnetfeld nur in einem sehr kleinen Bereich homogen ist, wird eine scheibenförmige Probe verwendet.
    Das Magnetfeld wird mit einem konstanten Strom erzeugt, damit es zeitlich konstant bleibt; des Weiteren ist das Konstantstromgerät mit einer Polwendevorrichtung ausgestattet, die es erlaubt, den Drehwinkel $θ$ zu verdoppeln, ohne gefährliche Induktionsspannungen zu erzeugen.

    Zur Messung der Lichtintensität werden Photowiderstände aus Bleisulfid verwendet, deren spektrale Empfindlichkeit vom Sichtbaren bis ins nahe Infrarot reicht; ihr Innenwiderstand ist über mehrere Zehnerpotenzen proportional zur Lichtintensität.
    Gemessen wird der Spannungsabfall bei Lichteinfall, während ein Gleichstrom durch die Widerstände geschickt wird.
    Um die Rauschspannungen der Photowiderstände zu minimieren, wird mit einer Wechsellichtmethode gearbeitet.
    Aus diesem Grund befindet sich im Strahlengang eine rotierende Sektorschiebe, die das Gleichlicht in Impulse zerhackt.
    Die am Photowiderstand abfallende Wechselspannung wird über einen Kondensator ausgekoppelt und mit einem Oszillographen nachgewiesen.

    Ein zweites Glan–Thompson-Prisma hinter dem Magneten wird benötigt, um die Drehung $θ$ der Polarisationsebene des Lichtes nach dem Durchgang durch die Probe zu messen.
    Mit einem Zweistrahlverfahren wird eine hohe Winkelauflösung erreicht.
    Diese zwei Strahlen, die mit einem Glan–Thompson-Prisma erzeugt werden, sind senkrecht zueinander polarisiert und werden mit Photowiderständen gemessen, deren Signalspannungen an einen Differenzverstärker angeschlossen ist.

    Die Ausgangsspannung des Differenzverstärkers verschwindet genau dann, wenn die Signale an beiden Eingängen in Betrag und Phase übereinstimmen.
    Hinter den Differenzverstärker werden ein Selektivverstärker und ein Oszillograph angeschlossen, der als Nulldetektor fungiert, das heißt prüft, ob die Signale von den Photowiderständen gleich sind oder nicht.
  \section{Durchführung}
    Vor der Messung muss die Apparatur justiert werden.
    Dazu werden die Probe und der Interferenzfilter aus dem Strahlengang genommen, damit mit sichtbarem Licht justiert werden kann; insbesondere wird das Photowiderstandsgehäuse samt Abbildungsoptik demontiert.

    Daraufhin wird die Polarisationsvorrichtung überprüft; kommt trotz entsprechender Polarisationseinstellung immer noch Licht durch, so wird die Intensität durch Drehen des Analysatorprismas um seine vertikale Achse minimiert.

    Im nächsten Schritt wird mit den Linsen die Lichtquelle auf die lichtempfindlichen Flächen der Photowiderstände abgebildet.
    Zur Kontrolle wird der Deckel von den Lichtschutzhauben zwischen Sammellinse und Photowiderstand abgenommen.

    Danach wird der Lichtzerhacker aktiviert und seine Drehfrequenz mit der Mittenfrequenz des Selektivverstärkers abgeglichen.
    Des Weiteren wird der Gütefaktor des Selektivverstärkers auf den Maximalwert eingestellt um seine Durchlasskurve zur Unterdrückung von Störspannung möglichst klein zu machen.
    Die Störspannungen entstehen im Wesentlichen durch das Rauschen der Photowiderstände und des Differenzverstärkers.
    Haben die Rauschspannungen ungefähr die gleiche Frequenz wie die Messfrequenz, so werden diese nur wenig unterdrückt, am Ausgang existiert deshalb immer ein kleines Rauschen.

    Am Ende der Justage wird die durchkommende Intensität maximiert, indem die Lichtquelle und die Kondensorlinse geschickt relativ zur Öffnung des Lichtzerhackers platziert werden.

    Nach der Justage wird die Faraday-Rotation an zwei unterschiedlich n-dotierten Galliumarsenid\-/Kristallen für verschiedene Wellenlängen im nahen Infrarot gemessen, indem die Interferenzfilter ausgetauscht werden.
    Als Referenz wird auch ein hochreiner \ce{GaAs}-Kristall vermessen.

    Für die Messung wird ein Glan–Thomson-Prisma bei maximalem Feld um seine Längsachse gedreht, um das Signal am Oszillographen verschwinden zu lassen.
    Die Winkeleinstellung $θ_1$ wird an einem Goniometer abgelesen, danach wird das Feld umgepolt und erneut ein Winkel $θ_2$ gefunden, bei dem das Signal am Oszillographen minimal wird.
    Da das Feld um insgesamt $2B$ geändert wird, ist der Winkel
    \begin{eqn}
      θ = \frac{1}{2} \g(θ_2 - θ_1) \..
    \end{eqn}

    Am Ende wird die Kraftflussdichte $\f{B}(z)$ bei maximalem Feldstrom mit einer Hallsonde, die in den Magneten eingeführt wird, gemessen; dabei wird relativ zur Mitte des Luftspaltes im Magneten gemessen.
  \section{Messwerte}
    Die Messwerte für das Magnetfeld in Abhängigkeit der Position befinden sich in Tabelle~\ref{tab:B}.
    Die Messwerte für die Winkel in Abhängigkeit der Wellenlänge für die drei Proben befinden sich in den Tabelle \ref{tab:θ-rein}, \ref{tab:θ-n1} und \ref{tab:θ-n2}.
    Dort sind auch ihr Dicken $L$ und Dotierungen $N$ aufgeführt.
    Die in Grad und in Minuten angegebenen Werte für die Winkel werden addiert, um den Winkel zu erhalten.
    \begin{table}
      \caption{Messwerte für das Magnetfeld.}
      \label{tab:B}
      \input{table/B.tex}
    \end{table}
    \begin{table}
      \caption{Messwerte für die reine Probe.}
      \label{tab:θ-rein}
      \input{table/θ-rein.tex}
    \end{table}
    \begin{table}
      \caption{Messwerte für die erste dotierte Probe.}
      \label{tab:θ-n1}
      \input{table/θ-n1.tex}
    \end{table}
    \begin{table}
      \caption{Messwerte für die zweite dotierte Probe.}
      \label{tab:θ-n2}
      \input{table/θ-n2.tex}
    \end{table}
  \section{Auswertung}
    Abbildung \ref{fig:B} zeigt das Magnetfeld in Abhängigkeit der Position.
    Tabelle \ref{tab:const} enthält die Werte verwendeter Naturkonstanten, den Brechungsindex von Galliumarsenid und das maximale Magnetfeld, welches für die weitere Auswertung verwendet wird.
    \begin{figure}
      \includegraphics{graphic/B.pdf}
      \caption{Magnetfeld in Abhängigkeit der Position.}
      \label{fig:B}
    \end{figure}
    \begin{table}
      \caption{Verwendete Naturkonstanten \cite{scipy}, Brechungsindex $n$ \cite{zoroofchi}, Magnetfeld.}
      \label{tab:const}
      \input{table/const.tex}
    \end{table}

    Die Abbildungen \ref{fig:θ-rein}, \ref{fig:θ-n1} und \ref{fig:θ-n2} zeigen die Messwerte für den Rotationswinkel in Abhängigkeit der Wellenlänge für die verschiedenen Proben.
    \begin{figure}
      \includegraphics{graphic/θ-rein.pdf}
      \caption{Rotationswinkel für die reine Probe in Abhängigkeit der Wellenlänge.}
      \label{fig:θ-rein}
    \end{figure}
    \begin{figure}
      \includegraphics{graphic/θ-n1.pdf}
      \caption{Rotationswinkel für die erste dotierte Probe in Abhängigkeit der Wellenlänge.}
      \label{fig:θ-n1}
    \end{figure}
    \begin{figure}
      \includegraphics{graphic/θ-n2.pdf}
      \caption{Rotationswinkel für die zweite dotierte Probe in Abhängigkeit der Wellenlänge.}
      \label{fig:θ-n2}
    \end{figure}

    Die Abbildungen \ref{fig:θ-λ²-n1} und \ref{fig:θ-λ²-n2} zeigen die Faraday-Rotation in Abhängigkeit des Quadrats Wellenlänge für die dotierten Proben.
    Dabei ergibt sich die Faraday-Rotation als Differenz der Rotationswinkel von dotierter und undotierter Probe.
    Aus der linearen Regression
    \begin{eqn}
      \frac{θ}{L} - \frac{θ_{\t{rein}}}{L_{\t{rein}}} = f λ² + g
    \end{eqn}
    werden die Parameter $f$ und $g$ bestimmt.
    Die Ergebnisse stehen in den Tabellen \ref{tab:m-n1} und \ref{tab:m-n2} und sind in den Abbildungen eingezeichnet.
    Aus Gleichung \ref{eqn:faraday-theorie} folgt
    \begin{eqn}
      f = \frac{e³}{8 \PI² ε_0 c³} \frac{N B}{n} \frac{1}{\g(m^*)²} \., \\
      m^* = \sqrt{\frac{e³}{8 \PI² ε_0 c³} \frac{N B}{n} \frac{1}{f}} \..
    \end{eqn}
    Die so bestimmten effektiven Massen $m^*$ stehen ebenfalls in den Tabellen \ref{tab:m-n1} und \ref{tab:m-n2}.
    \begin{figure}
      \includegraphics{graphic/θ-λ²-n1.pdf}
      \caption{Faraday-Rotation für die erste dotierte Probe.}
      \label{fig:θ-λ²-n1}
    \end{figure}
    \begin{figure}
      \includegraphics{graphic/θ-λ²-n2.pdf}
      \caption{Faraday-Rotation für die zweite dotierte Probe.}
      \label{fig:θ-λ²-n2}
    \end{figure}
    \begin{table}
      \caption{Ergebnisse für die erste dotierte Probe.}
      \label{tab:m-n1}
      \input{table/m-n1.tex}
    \end{table}
    \begin{table}
      \caption{Ergebnisse für die zweite dotierte Probe.}
      \label{tab:m-n2}
      \input{table/m-n2.tex}
    \end{table}
  \section{Diskussion}
    Die Ergebnisse für die effektive Masse für die unterschiedlich dotierten Proben sind innerhalb von zwei Standardabweichungen miteinander kompatibel.
    Allerdings stimmen die gemessenen Werte für die effektive Masse normiert auf die Elektronenmasse $m^* / m_{\Pe}$ nicht mit dem Literaturwert von \num{0.066} \cite{blakemore} überein.
  \makebibliography
\end{document}
