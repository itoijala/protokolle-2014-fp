import numpy as np
from numpy.polynomial.polynomial import polyval

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

a, B = np.loadtxt('data/B', unpack=True)

fig, ax = plt.subplots(1, 1)
ax.plot(a, B, 'rx')
ax.set_xlim(69, 91)
ax.set_xlabel(r'$\silabel{a}{\milli\meter}$')
ax.set_ylabel(r'$\silabel{B}{\milli\tesla}$')
fig.savefig('build/graphic/B.pdf')

tab = lt.SymbolColumnTable()
tab.add_si_column('a', a, r'\milli\meter', places=0)
tab.add_si_column('B', B, r'\milli\tesla', places=0)
tab.savetable('build/table/B.tex')
