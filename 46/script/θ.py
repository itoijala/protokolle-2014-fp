import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

e, ε0, c, me = lt.constant('elementary charge', 'electric constant', 'speed of light in vacuum', 'electron mass')

n = unc.ufloat(2.4, 0.1)
B = 442e-3

tab = lt.SymbolRowTable()
tab.add_si_row('e', e * 1e19, r'\coulomb', exp='e-19')
tab.add_si_row('ε_0', lt.noms(ε0) * 1e12, r'\farad\per\meter', exp='e-12', figures=12)
tab.add_si_row('c', lt.noms(c) * 1e-8, r'\meter\per\second', exp='e8', figures=9)
tab.add_si_row(r'm_{\Pe}', me * 1e31, r'\kilogram', exp='e-31')
tab.add_hrule()
tab.add_si_row('n', n)
tab.add_hrule()
tab.add_si_row('B', B * 1e3, r'\milli\tesla', figures=3)
tab.savetable('build/table/const.tex')

for name, ymin, ymax in [('rein', -1.0, 2.5), ('n1', 2.0, 3.6), ('n2', 2.0, 7.0)]:
    L = np.loadtxt('data/L-' + name)
    if name != 'rein':
        N = np.loadtxt('data/N-' + name, skiprows=2)
    λ, θ1, θ1min, θ2, θ2min = np.loadtxt('data/θ-' + name, unpack=True, skiprows=2)
    θ = 1 / 2 * ((θ2 + θ2min / 60) - (θ1 + θ1min / 60))

    fig, ax = plt.subplots(1, 1)
    ax.plot(λ, θ / L, 'rx')
    ax.set_ylim(ymin, ymax)
    ax.set_xlabel(r'$\silabel{λ}{\micro\meter}$')
    ax.set_ylabel(r'$\silabel{\frac{θ}{L}}{\degree\per\milli\meter}$')
    fig.savefig('build/graphic/θ-' + name + '.pdf')

    tab = lt.SymbolRowTable()
    tab.add_si_row('L', L, r'\milli\meter', places=2)
    if name != 'rein':
        tab.add_si_row('N', N, r'\per\cubic\centi\meter', exp='e18', places=1)

    tab2 = lt.SymbolColumnTable()
    tab2.add_si_column('λ', λ, r'\micro\meter', places=2)
    tab2.add_si_column('θ_1', θ1, r'\degree', places=0)
    tab2.add_si_column('θ_1', θ1min, r'\arcminute', places=0)
    tab2.add_si_column('θ_2', θ2, r'\degree', places=0)
    tab2.add_si_column('θ_2', θ2min, r'\arcminute', places=0)

    tab3 = lt.Table()
    tab3.add(lt.Row())
    tab3.add_hrule()
    tab3.add(lt.Row())
    tab3.add(lt.Column([tab, tab2], "@{}c@{}c@{}"))
    tab3.savetable('build/table/θ-' + name + '.tex')

L_rein = np.loadtxt('data/L-rein')
λ, θ1_rein, θ1min_rein, θ2_rein, θ2min_rein = np.loadtxt('data/θ-rein', unpack=True, skiprows=2)
θ_rein = 1 / 2 * ((θ2_rein + θ2min_rein / 60) - (θ1_rein + θ1min_rein / 60))

for name in ['n1', 'n2']:
    L = np.loadtxt('data/L-' + name)
    N = np.loadtxt('data/N-' + name, skiprows=2) * 1e18 * 1e6
    λ, θ1, θ1min, θ2, θ2min = np.loadtxt('data/θ-' + name, unpack=True, skiprows=2)
    θ = 1 / 2 * ((θ2 + θ2min / 60) - (θ1 + θ1min / 60))
    y = θ / L - θ_rein / L_rein

    f, g = lt.linregress(λ**2, y)

    m = unp.sqrt(e**3 * N * B / (8 * np.pi**2 * ε0 * c**3 * n * unp.radians(f) * 1e15))

    fig, ax = plt.subplots(1, 1)
    x = np.linspace(1, 8)
    ax.plot(x, lt.noms(f * x + g), 'b-')
    ax.plot(λ**2, y, 'rx')
    ax.set_xlabel(r'$\silabel{λ²}{\micro\meter\squared}$')
    ax.set_ylabel(r'$\silabel{\frac{θ}{L} - \frac{θ_{\t{rein}}}{L_{\t{rein}}}}{\degree\per\milli\meter}$')
    fig.savefig('build/graphic/θ-λ²-' + name + '.pdf')

    tab = lt.SymbolRowTable()
    tab.add_si_row('f', f, r'\degree\per\cubic\micro\meter', exp='e3')
    tab.add_si_row('g', g, r'\degree\per\milli\meter')
    tab.add_hrule()
    tab.add_si_row('m^*', m * 1e31, r'\kilogram', exp='e-31')
    tab.add_si_row(r'm^* / m_{\Pe}', m / me)
    tab.savetable('build/table/m-' + name + '.tex')
