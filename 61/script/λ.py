import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

λ_lit = 632.8

L = 59.5e1
b = 0.075

x, I, δI = np.loadtxt('data/λ', unpack=True, skiprows=2)
I = unp.uarray(I, δI)

def f(x, A, λ, x0, B):
    return A * b**2 * np.sinc(b * np.sin((x - x0) / L) / λ)**2 + B

p0 = (100 / b**2, 632e-6, 23, 0)
A, λ, x0, B = lt.ucurve_fit(f, x, I, p0=p0)

fig, ax = plt.subplots(1, 1)
ax.errorbar(x, lt.noms(I), yerr=lt.stds(I), fmt='rx', zorder=4)
y = np.linspace(0, 40, 1000)
ax.plot(y, f(y, *lt.noms(A, λ, x0, B)), 'b-')
ax.set_xlabel(r'$\silabel{x}{\milli\meter}$')
ax.set_ylabel(r'$\silabel{I}{\nano\ampere}$')
fig.savefig('build/graphic/λ.pdf')

tab1 = lt.SymbolRowTable()
tab1.add_si_row('L', L * 1e-1, r'\centi\meter', figures=3)
tab1.add_si_row('b', b, r'\milli\meter', figures=2)

tab2 = lt.SymbolColumnTable()
tab2.add_si_column('x', np.array_split(x, 2)[0], r'\milli\meter', places=2)
tab2.add_si_column('I', np.array_split(I, 2)[0], r'\nano\ampere')
tab2.add(lt.VerticalSpace())
tab2.add_si_column('x', np.array_split(x, 2)[1], r'\milli\meter', places=2)
tab2.add_si_column('I', np.array_split(I, 2)[1], r'\nano\ampere')

tab = lt.Table()
tab.add(lt.Row())
tab.add_hrule()
tab.add(lt.Row())
tab.add(lt.Column([tab1, tab2], "@{}c@{}"))
tab.savetable('build/table/λ-data.tex')

tab = lt.SymbolRowTable()
tab.add_si_row('A', A * 1e-3, r'\ampere\per\meter\squared')
tab.add_si_row('x₀', x0, r'\milli\meter')
tab.add_si_row('B', B, r'\nano\ampere')
tab.add_si_row('λ', λ * 1e6, r'\nano\meter')
tab.add_hrule()
tab.add_si_row(r'λ_{\t{lit}}', λ_lit, r'\nano\meter', figures=4)
tab.savetable('build/table/λ-result.tex')
