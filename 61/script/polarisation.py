import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

α, I, δI = np.loadtxt('data/polarisation', unpack=True, skiprows=2)
I = unp.uarray(I, δI)

def f(x, A, B, C, D):
    return A * np.cos(B * x + C)**2 + D

A, B, C, D = lt.ucurve_fit(f, unp.radians(α), I, p0=(25, 1, 0, 0))

fig, ax = plt.subplots(1, 1)
ax.errorbar(α, lt.noms(I), yerr=lt.stds(I), fmt='rx', zorder=4)
x = np.linspace(-10, 360, 1000)
ax.plot(x, f(np.radians(x), *lt.noms(A, B, C, D)), 'b-')
ax.set_xlim(-10, 360)
ax.set_xlabel(r'$\silabel{α}{\degree}$')
ax.set_ylabel(r'$\silabel{I}{\micro\ampere}$')
fig.savefig('build/graphic/polarisation.pdf')

tab = lt.SymbolColumnTable()
tab.add_si_column('α', np.array_split(α, 2)[0], r'\degree', places=0)
tab.add_si_column('I', np.array_split(I, 2)[0], r'\micro\ampere')
tab.add(lt.VerticalSpace())
tab.add_si_column('α', np.array_split(α, 2)[1], r'\degree', places=0)
tab.add_si_column('I', np.array_split(I, 2)[1], r'\micro\ampere')
tab.savetable('build/table/polarisation-data.tex')

tab = lt.SymbolRowTable()
tab.add_si_row('A', A, r'\micro\ampere')
tab.add_si_row('B', B)
tab.add_si_row('C', C)
tab.add_si_row('D', D, r'\micro\ampere')
tab.savetable('build/table/polarisation-result.tex')
