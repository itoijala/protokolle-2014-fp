import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

λ_lit = 632.8

L = 25.2
d = 0.01e-1

m, x = np.loadtxt('data/gitter', unpack=True, skiprows=2)
x0 = float(x[m == 0])

def f(m, λ, x0):
    return m * λ / d * L + x0

λ, x0 = lt.curve_fit(f, m, x, p0=(600e-7, - float(x[m == 0])))

fig, ax = plt.subplots(1, 1)
ax.plot(m, x, 'rx', zorder=4)
y = np.linspace(-10, 10, 1000)
ax.plot(y, f(y, *lt.noms(λ, x0)), 'b-')
ax.set_xlabel(r'$m$')
ax.set_ylabel(r'$\silabel{x}{\centi\meter}$')
fig.savefig('build/graphic/gitter.pdf')

tab1 = lt.SymbolRowTable()
tab1.add_si_row('L', L, r'\centi\meter', figures=3)
tab1.add_si_row('d', d * 1e1, r'\milli\meter', figures=1)

tab2 = lt.SymbolColumnTable()
tab2.add_si_column('m', np.array_split(m, 2)[0], places=0)
tab2.add_si_column('x', np.array_split(x, 2)[0], r'\centi\meter', places=1)
tab2.add(lt.VerticalSpace())
tab2.add_si_column('m', np.array_split(m, 2)[1], places=0)
tab2.add_si_column('x', np.array_split(x, 2)[1], r'\centi\meter', places=1)

tab3 = lt.SymbolRowTable()
tab3.add_si_row('x₀', x0, r'\milli\meter')
tab3.add_si_row('λ', λ * 1e7, r'\nano\meter')
tab3.add_hrule()
tab3.add_si_row(r'λ_{\t{lit}}', λ_lit, r'\nano\meter', figures=4)

tab = lt.Table()
tab.add(lt.Row())
tab.add_hrule()
tab.add(lt.Row())
tab.add_hrule()
tab.add(lt.Row())
tab.add(lt.Column([tab1, tab2, tab3], "@{}c@{}"))
tab.savetable('build/table/gitter.tex')
