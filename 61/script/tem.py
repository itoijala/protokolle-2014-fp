import numpy as np

from scipy.special import hermite

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

def f(l):
    def f(x, A, w, x0, B):
        return A * (hermite(l)(np.sqrt(2) * (x - x0) / w) * np.exp(-((x - x0) / w)**2))**2 + B
    return f

for l, p0 in [
        (1, (1500, 10, 15, 0)),
        (2, ( 300,  5, 13, 0)),
        ]:
    x, I, δI = np.loadtxt('data/tem-{}'.format(l), unpack=True, skiprows=2)
    I = unp.uarray(I, δI)

    A, w, x0, B = lt.ucurve_fit(f(l), x, I, p0=p0)

    fig, ax = plt.subplots(1, 1)
    ax.errorbar(x, lt.noms(I), yerr=lt.stds(I), fmt='rx', zorder=4)
    y = np.linspace(0, 30, 1000)
    ax.plot(y, f(l)(y, *lt.noms(A, w, x0, B)), 'b-')
    ax.set_xlabel(r'$\silabel{x}{\milli\meter}$')
    ax.set_ylabel(r'$\silabel{I}{\nano\ampere}$')
    fig.savefig('build/graphic/tem-{}.pdf'.format(l))

    tab = lt.SymbolColumnTable()
    tab.add_si_column('x', np.array_split(x, 2)[0], r'\milli\meter', places=2)
    tab.add_si_column('I', np.array_split(I, 2)[0], r'\nano\ampere')
    tab.add(lt.VerticalSpace())
    tab.add_si_column('x', np.array_split(x, 2)[1], r'\milli\meter', places=2)
    tab.add_si_column('I', np.array_split(I, 2)[1], r'\nano\ampere')
    tab.savetable('build/table/tem-{}-data.tex'.format(l))

    tab = lt.SymbolRowTable()
    tab.add_si_row('A', A, r'\nano\ampere')
    tab.add_si_row('w', w, r'\milli\meter')
    tab.add_si_row('x₀', x0, r'\milli\meter')
    tab.add_si_row('B', B, r'\nano\ampere')
    tab.savetable('build/table/tem-{}-result.tex'.format(l))
