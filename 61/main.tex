\input{header/report.tex}

\addbibresource{lit.bib}

\DeclareSIUnit \torr{Torr}

\SetExperimentNumber{61}

\begin{document}
  \maketitlepage{\texorpdfstring{Der \ce{HeNe}-Laser}{Der HeNe-Laser}}{23.06.2014}{29.08.2014}
  \section{Ziel}
    Der Versuch dient dazu, die Funktionsweise eines \ce{HeNe}-Lasers kennen zu lernen.
  \section{Theorie}
    Licht aus Lasern (Light Amplification by Stimulated Emission of Radiation) zeichnet sich sowohl durch Monochromatie als auch durch hohe Kohärenz und Intensität aus.
    Dafür bestehen Laser im Wesentlichen aus drei Grundkomponenten: dem aktiven Lasermedium, der Pumpquelle und dem Resonator.
    Hierbei ist das Lasermedium verantwortlich für das Strahlungsspektrum des Lasers, das vom UV- bis zum IR-Bereich reichen kann.
    Die Pumpquelle erzeugt eine Besetzungsinversion, die zur signifikanten Erhöhung der Wahrscheinlichkeit für stimulierte Emission führt; der Resonator schickt durch optische Rückkopplung den Großteil des so emittierten Lichts immer wieder durch das Lasermedium, wodurch ein selbsterregender Oszillator entsteht.
    Dies wird im Folgenden näher erläutert.

    Ein Lasermedium, das im einfachsten Fall durch ein Zwei-Niveau-System modelliert wird, soll bei Interaktion mit Lichtphotonen das Photonfeld verstärken.
    Die drei hier vorkommenden Wechselwirkungsarten sind in Abbildung \ref{fig:interaction} veranschaulicht.
    \begin{figure}
      \includegraphics[width=\textwidth]{graphic/interactions.pdf}
      \caption{Schematische Abbildungen der drei hier vorkommenden Interaktionen zwischen Photonen und Lasermedium \cite{anleitung61}.}
      \label{fig:interaction}
    \end{figure}
    Hierin sind $n_i$ die Besetzungsdichte des $i$-ten Niveaus, $\f{ρ}(ω)$ das Strahlungsfeld und $A_{ij}$ beziehungsweise $B_{ij}$ Einstein-Koeffizienten, die ein Maß für die Übergangswahrscheinlichkeiten zwischen beiden Zuständen und die inversen mittleren Verweilzeiten im angeregten Zustand sind \cite{kneubühl}.
    Im ersten Bild ist der Fall der Absorption zu sehen, bei dem ein Photon absorbiert und dadurch ein Zwei-Niveau-System angeregt wird; die Energie des Photons muss dabei der Energiedifferenz der Niveaus entsprechen.
    Umgekehrt kann in einer spontanen Emission der angeregte Zustand in den Grundzustand übergehen und ein Photon abgestrahlt werden (drittes Bild).
    Bei der stimulierten Emission, veranschaulicht im zweites Bild, regt ein einfallendes Photon ein angeregtes Zwei-Niveau-System an, in den Grundzustand zu gehen und dabei ein Photon abzustrahlen.
    Da das System schon im angeregten Zustand ist, kann das einfallende Photon nicht absorbiert werden; insgesamt werden also zwei Photonen emitiert.
    Das stimulierte Photon aus dem Übergang hat dieselbe Energie, Phase und Ausbreitungsgeschwindigkeit wie das stimulierende Photon.
    Insgesamt wird also bei einem Strahlungsfeld $ρ$ die Besetzungsdichte des angeregten Zustandes bei spontaner und stimulierten Emission vermindert und bei Absorption erhöht.

    Die Anzahl der Photonen, die in diesen unterschiedlichen Arten wechselwirken, kann durch die Diffentialgleichungen
    \begin{eqns}
      \d{t}{N_{\t{A}}}; &=& n_1 \f{ρ}(ν) B_{12}
      \itext{für die Absorption,}
      \d{t}{N_{\t{IE}}}; &=& n_2 \f{ρ}(ν) B_{21}
      \itext{für die stimulierte Emission und}
      \d{t}{N_{\t{SE}}}; &=& n_2 A_{21}
    \end{eqns}
    für die spontane Emission beschrieben werden.
    Da die Absorption und die stimulierte Emission Funktionen des Strahlungsfelds sind, sind dies kohärente Vorgänge; die spontane Emission, die zufällig Photonen hervorbringt, die zudem eine zufällige Phase, Richtung und Polarisation haben, ist jedoch ein inkohärenter Beitrag \cite{kneubühl}.
    Unter der Annahme, dass $n_1 + n_2$ konstant ist, lassen sich die Ratengleichungen
    \begin{eqn}
      \d{t}{n_1}; = -\d{t}{n_2}; = - n_1 B_{12} ρ + \tilde{n}_2 B_{21} ρ + n_2 A_{21}
    \end{eqn}
    für die Besetzungsdichten der Niveaus aufstellen.

    Die stimulierte Emission muss häufiger als die spontane Emission auftreten, sodass Kohärenz und dauerhafte Verstärkung gewährleistet sind.
    Laut der Maxwell–Boltzmann-Verteilung ist im thermischen Gleichgewicht aber vorwiegend der Grundzustand besetzt.
    Stimulierte Emission tritt jedoch nur dann häufiger als spontane Emission auf, wenn der angeregte Zustand stärker als der Grundzustand besetzt ist; es wird von Besetzungsinversion gesprochen.
    Die Besetzungsinversion wird durch so genanntes \enquote{Pumpen}, das heißt permanentes Zuführen äußerer Energie, erreicht.

    In der Realität sind Zwei-Niveau-Laser allerdings nicht möglich.
    Um einen Grundzustand anzuregen, müsste genau ein Photon mit der Energiedifferenz zwischen dem Grundzustand und dem ersten angeregten Zustand eingestrahlt werden.
    Findet jetzt stimulierte Emission statt, indem ein weiteres Photon mit der selben Energie eingestrahlt wird, so treten wieder beide aus und die Gesamtzahl der gewünschten Photonen wurde nicht verändert.
    Bei meheren Niveaus jedoch unterscheidet sich das Photon, das für den Pumpvorgang verantwortlich ist, von dem Photon, das die stimulierte Emission erzeugt in seiner Energie; bei unterschiedlichen Abregungszeitskalen mehrerer Niveaus findet eine Besetzungsinversion statt.
    Atome haben des Weiteren in der Regel mehr als zwei Niveaus, vereinfachend kann aber der wahrscheinlichste Übergang als solch ein Zwei-Niveau-System modelliert werden.

    Die Verstärkung wächst exponentiell mit der Länge des Laufweges im aktiven Lasermedium an.
    Um den Effekt experimentell auszunutzen, wird das Lasermedium zwischen ein Paar reflektierender Spiegel gebracht, wobei ein Spiegel teilreflektierend und einer totalreflektierend ist.
    Der teilreflektierende Spiegel des optischen Resonators koppelt den Laserstrahl aus.
    Die Spiegel in optischen Resonatoren können aus planparallelen, sphärischen oder einer Kombination aus beiden Arten von Spiegeln bestehen; dementsprechend wird von einem planparallen oder sphärischen Resonator gesprochen.
    Die niedrigste Dämpfung des Oszillators durch Verluste im Resonatorspiegel ist bei einem konfokalen Resonator realisiert, bei dem die Spiegelbrennpunkte zusammenfallen.

    Wenn die Verluste im Resonator kleiner als die Verstärkung durch die stimulierte Emission sind, dann handelt es sich um einen selbsterregenden Oszillator, der stabil ist.
    Quantitativ gilt für die Resonatorparameter
    \begin{eqn}
      g_i = 1 - \frac{L}{r_i}
    \end{eqn}
    dann die Beziehung
    \begin{eqn}
      0 \leq g_1 g_2 < 1 \., \label{eqn:stabilität}
    \end{eqn}
    wobei $r_i$ der Krümmungsradius des $i$-ten Spiegels und $L$ die Resonatorlänge sind.

    Da $L \gg λ$ ist, gibt es mehrere Wellenlängen $λ$, die zu stehenden Wellen im Resonator führen.
    Die Anzahl $q$ dieser Wellenlängen wird longitudinale Mode genannt.
    Jedoch führen Spiegelunebenheiten, Verkippung und so weiter dazu, dass der Resonator auch in transversalen Moden schwingt.
    Die Moden des Resonators werden, wie bei Hohlleitern, $\t{TEM}_{lpq}$ genannt, wobei $l$ und $p$ die Anzahl der Knoten in $x$-, und $y$-Richtung sind und als transversale Modenzahl bezeichnet werden.
    In der Regel wird die longitudinale Modenzahl $q$ weggelassen, da sie für die skalare Feldverteilung nicht von Bedeutung ist.
    Die größten Verluste treten bei Moden mit niedriger Symmetrie, also höheren Moden, auf; dies hat den Effekt, dass nur wenige transversale Moden isoliert und verstärkt werden.

    Die Intensitätsverteilung für einen Resonator mit rechteckiger Symmetrie kann durch
    \begin{eqns}
      \f{I_{lp}}(x, y) &\propto& \g(\HermiteH_l(\sqrt{2} \frac{x}{w}) \exp(- \frac{x²}{w²}) \HermiteH_p(\sqrt{2} \frac{y}{w}) \exp(- \frac{y²}{w²}))^{\!\! 2} \label{eqn:field-distribution}
    \end{eqns}
    mit den Hermite-Polynomen $\HermiteH_n;$ und dem Strahlradius $w$ angenähert werden \cite{tem}.
    Die Grundmode $\t{TEM}_{00}$ ist die Mode höchster Symmetrie und somit diejenige mit den geringsten Verlusten; sie weist keine Nullstellen in transversaler Richtung auf.
    Aus Gleichung \eqref{eqn:field-distribution} folgt, dass ihre Intensitätsverteilung durch eine Gaußverteilung gegeben ist.

    Der \ce{HeNe}-Laser ist ein Gaslaser mit einem Helium-Neon-Gemisch.
    Durch die Gasentladung sind ein Teil der Atome ionisiert.
    Durch Stöße der Elektronen mit Helium- und Neonatomen werden die Atome zwar angeregt; dieser Effekt reicht jedoch alleine nicht aus, um eine Besetzungsinversion hervorzurufen.
    Vielmehr sorgt das durch Elektronen angeregte Helium für die Inversion des Neons, sodass das Neon bei seinem intensivsten Niveauübergang die hier untersuchten Photonen der Wellenlänge $λ = \SI{632.8}{\nano\meter}$ aussendet.
    Die Termschemata von Helium und Neon mit den hier relevanten Energieniveaus sind in Abbildung \ref{fig:energy-level-scheme} zu finden.
    \begin{figure}
      \includegraphics{graphic/term_scheme-manipulated.png}
      \caption{Termschema von Helium und Neon mit den hier relevanten Energieniveaus \cite{kneubühl}.}
      \label{fig:energy-level-scheme}
    \end{figure}

    Im Detail führen die Elektronenstöße zu einer Anregung des Heliums in die metastabilen Zustände $2^3_{\t{s}}$ (Lebensdauer \SI{1e-4}{\second}) und $2^1_{\t{s}}$ (Lebensdauer \SI{5e-6}{\second}).
    Das Neon wird, wie oben erwähnt, nur selten durch Elektronenstöße in die benötigten Zustände $3_{\t{s}}$ und $2_{\t{p}}$ angeregt.
    Diese langlebigen, metastabilen Zustände des im Gasgemisches überwiegenden Heliums entsprechen jedoch fast den relevanten Niveaus bei Neon.
    Durch Stöße zweiter Art können daher die Zustände im Neon angeregt werden; als Konsequenz tritt Inversion auf \cite{kneubühl}.

    Die Übergänge erfolgen wegen der Auswahlregeln für elektrische Dipolübergänge nur in $\t{p}$-Zustände des Neons.
    Nach dem Übergang in das $2_{\t{p}}$-Niveau findet eine schnelle Entleerung innerhalb von \SI{10}{\nano\second} in das $1_{\t{s}}$-Niveau statt.
    Es kann vorkommen, dass die Elektronen durch Entladungselektronen in den $2_{\t{p}}$-Zustand des Neons angeregt werden, was die Emission der sichtbaren Laserwellenlänge vermindert.
    Der Übergang, der das hier untersuchte Laserlicht erzeugt, stammt vom Übergang $3_{\t{s}}^2 \rightarrow 2_{\t{p}}^4$ \cite{kneubühl}.

    Diese Stöße zweiter Art treten nur auf, wenn die Energie dabei zwischen den Stoßpartnern völlig übertragen werden kann und Energieresonanz besteht.
    Für die Verstärkung ist es notwendig, dass die Wahrscheinlichkeit für spontane Emission für den Entleerungsübergang hoch ist.
    Die Dicke des Laserrohrs darf nicht zu groß gewählt werden, da sonst eine weitere Entleerung des $1_{\t{s}}$-Niveaus nicht möglich ist.
    Die Laserröhre wird mit Brewsterfenstern abgeschlossen.
    Diese nutzen den Effekt, dass bei bestimmten festen Brechungsindizes $n_1$ und $n_2$ feste Einstrahlwinkel existieren, bei denen es keine Reflexion gibt.
    Die Brewsterfenster müssen daher unter dem so genannten Brewsterwinkel auf das Rohr gesetzt werden; allerdings ist der reflexionsfreie Durchtritt nur für linear polarisiertes Licht möglich \cite{laser-skelett}.

    Höhere Moden sind räumlich mehr ausgedehnt als tiefere.
    Dies kann man ausnutzen, um Laser zur Oszillation in der Grundmode zu zwingen.  Durch das Einfügen einer Blende in den Resonator, deren Durchmesser kleiner ist als die Moden höherer Ordnung, aber größer als die Grundmode, werden für höhere Moden größere Verluste erzeugt.
    Da der Laser vor allem in Moden anschwingt, die minimalen Verlust haben, wird die Grundmode bevorzugt.
    Modenblenden werden bevorzugt bei Lasern mit großen Verstärkungen eingesetzt, die leicht zur Oszillationen in höheren Moden neigen \cite{lecture}.
  \section{Aufbau}
    Abbildung \ref{fig:setup} zeigt ein Foto des Aufbaus.
    \begin{figure}
      \includegraphics[width=\textwidth]{graphic/setup.pdf}
      \caption{Foto des Versuchsaufbaus \cite{anleitung61}.}
      \label{fig:setup}
    \end{figure}
    Auf einer optischen Schiene befindet sich ein Justierlaser mit der Wellenlänge $λ = \SI{532}{\nano\meter}$ und der maximalen Leistung $P_{\t{max}} = \SI{1}{\milli\watt}$, die aber für Justagezwecke reduziert ist auf $P_{\t{red}} = \SI{0.2}{\milli\watt}$.
    Der \ce{HeNe}-Laser besteht aus einem Laserrohr und zwei hochreflektierenden Spiegeln und bildet damit einen Resonator.
    Der \ce{HeNe}-Laser ist ein Gaslaser mit einem Helium-Neon-Verhältnis von $5 : 1$ bei einem Druck von circa \SI{1}{\torr}.
    Das Gasgemisch befindet sich in einem Laserrohr.
    Des Weiteren ist das Rohr mit Brewsterfenstern abgeschlossen.
    Neon erfüllt bei diesem Laser die Rolle des Lasermediums, Helium fungiert als Pumpgas.
    Das Laserrohr hat eine Länge von $l = \SI{408}{\milli\meter}$ und einen Durchmesser von $d = \SI{1.1}{\nano\meter}$ und ist mit Elektroden versehen, sodass mittels Entladung Inversion stattfinden kann.
    Der Durchmesser der Spiegel beträgt $D = \SI{12.7}{\nano\meter}$.
    Zu Bestimmung der Eigenschaften des Lasers stehen Komponenten wie Photodioden, Spalte, Mikrometerschraube und Polarisator zur Verfügung, die in den Strahlengang des Lasers gebracht werden können.
    Für die Justage steht ein Schirm mit Fadenkreuz und ein Schirm mit Beugungsblende zur Verfügung.
    Als Modenblende wird ein Draht gespannt, der höhere Moden unterdrückt.
  \section{Durchführung}
    \subsection{Justage}
      Der Justierlaser wird mit den beiden Beugungsblenden auf die optische Schiene gebracht, sodass die Blenden einen maximalen Abstand voneinander haben.
      Daraufhin wird der Laser mit den sechs Justierschrauben so ausgerichtet, dass die Fadenkreuze sich in der Mitte der Beugungsringe befinden.
      Nun wird nacheinander der Resonatorspiegel und das Laserrohr auf die optische Schiene gestellt und jede einzelne Komponente daraufhin kontrolliert, dass der Rückreflex des Justierlasers wieder die Justierblende im Fadenkreuz trifft.
      Diese Vorjustage ist essentiell für die weiteren Messungen, bei der Justage kann auch eine Wand zur Hilfe genommen werden.
      Jetzt können die restlichen Komponenten, also das Plasmarohr und die Resonatorspiegel auf die optische Bank gestellt werden und der Justierlaser ausgeschaltet werden.

      Der Strom bei der Hochspannung wird nun auf $I = \SI{6.5}{\milli\ampere}$ gestellt; dies hat zur Folge, dass das Laserrohr nun aufgrund der Entladung rot leuchtet.
      Da in der Regel noch keine Lasertätigkeit einsetzt, müssen die Resonatorspiegel vorsichtig mit den Justierschrauben eingestellt werden.
      Die Spiegel sind richtig eingestellt, wenn zwischen Brewsterfenster und Resonatorspiegel ein roter Strahl erscheint.
      Falls dies nicht funktioniert, muss die Justage wieder von vorne begonnen werden.
    \subsection{Überprüfen der Stabilitätsbedingung}
      Der Laser wird mithilfe einer Fotodiode auf die maximale Leistung eingestellt.
      Außerdem wird der Abstand der Resonatorspiegel bei laufendem Laser maximiert; währenddessen muss synchron die Laserleistung nachjustiert werden.
      Bei guter Justage sollte fast der theoretische Wert aus der Stabilitätsbedingung \eqref{eqn:stabilität} erreicht werden.
      Die Messung wird für zwei Resonatoren durchgeführt.
    \subsection{Bestimmung der Polarisation}
      Für die Bestimmung der Polarisation wird der Polarisator hinter den Auskoppelspiegel gestellt und die Intensität mit einer Photodiode als Funktion der Polarisatorrichtung gemessen.
    \subsection{Bestimmung der Wellenlänge}
      Aus den Beugungsmaxima beziehungsweise -minima eines Spaltes und eines Gitters wird die Wellenlänge des \ce{HeNe}-Lasers bestimmt.
    \subsection{Beobachten von TEM-Moden}
      Es wird versucht, möglichst viele verschiedene Moden zu stabilisieren und zu identifizieren.
      Dazu wird ein dünner Wolframdraht des Durchmessers $δ = \SI{0.005}{\milli\meter}$ zwischen Resonatorspiegel und Laserrohr gebracht und so verschoben, dass verschiedene Moden auf einem optischen Schirm erkennbar sind.
      Schließlich wird der optische Schirm durch eine Photodiode ersetzt und zwei Moden werden vermessen.
  \section{Messwerte und Auswertung}
    \subsection{Stabilitätsbedingung}
      Tabelle \ref{tab:stability} enthält die theoretischen und experimentellen Werte für die Stabilitätsmessung.
      Dabei ist $r_{\t{aus}}$ die Brennweite des auskoppelnden Spiegels, $r_{\t{end}}$ die Brennweite des totalreflektierenden Spiegels, $x_i$ die Position der Spiegel auf der Messschiene und $L$ der daraus errechnete Abstand; $L_{\t{th}}$ ist der theoretisch mögliche Abstand für einen stabilen Betrieb des Lasers.
      \begin{table}
        \caption{Experimentell ermittelte und theoretische Grenzen für die Stabilitätsmessung.}
        \label{tab:stability}
        \begin{tabular}{@{}c@{}}
          \toprule
          \begin{tabular}{c S[table-format=1.0] c}
            $r_{\t{aus}}$ & 1 & \si{\meter} \\
          \end{tabular} \\
          \midrule
          \begin{tabular}{S[table-format=1.0] S[table-format=2.1] S[table-format=3.1] S[table-format=3.1] S[table-format=3.0]}
            \multicolumn{1}{c}{$r_{\t{end}} \mathbin{/} \si{\meter}$} &
            \multicolumn{1}{c}{$x₁ \mathbin{/} \si{\centi\meter}$} &
            \multicolumn{1}{c}{$x₂ \mathbin{/} \si{\centi\meter}$} &
            \multicolumn{1}{c}{$L \mathbin{/} \si{\centi\meter}$} &
            \multicolumn{1}{c}{$L_{\t{th}} \mathbin{/} \si{\centi\meter}$} \\
            \midrule
            ∞ & 56.8 & 155.3 &  98.5 & 100 \\
            1 &  1.6 & 197.0 & 195.4 & 200 \\
          \end{tabular} \\
          \bottomrule
        \end{tabular}
      \end{table}
    \subsection{Polarisation}
      Die Ergebnisse der Polarisationsmessung befinden sich in Tabelle \ref{tab:polarisation-data} und sind in Abbildung \ref{fig:polarisation} geplottet.
      Die Freiheitsgrade der entsprechende Theoriekurve
      \begin{eqn}
        I = A \cos^2(B α + C) + D \label{eqn:theory-polarisation}
      \end{eqn}
      werden an die Messwerte angepasst.
      Die angepasste Theoriekurve ist in Abbildung \ref{fig:polarisation} aufgezeichnet und die ermittelten Fitparameter stehen in Tabelle \ref{tab:polarisation-result}.
      \begin{table}
        \caption{Daten der Polarisationmessung.}
        \label{tab:polarisation-data}
        \input{table/polarisation-data.tex}
      \end{table}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/polarisation.pdf}
        \caption{Messwerte und Fitkurve für die Polarisationsmessung.}
        \label{fig:polarisation}
      \end{figure}
      \begin{table}
        \caption{Fitparameter für Gleichung \ref{eqn:theory-polarisation} bezüglich der Messwerte aus Tabelle \ref{fig:polarisation}.}
        \label{tab:polarisation-result}
        \input{table/polarisation-result.tex}
      \end{table}
      \FloatBarrier
    \subsection{Wellenlängenmessung mit einem Spalt}
      Die Messwerte für die Wellenlängenmessung mit Hilfe eines Spaltes stehen in Tabelle \ref{tab:λ-data}.
      Hierbei ist $L$ der Abstand zwischen Spalt und Photodetektor, $b$ die Breite des Spaltes, $x$ die Position des Photodetektors auf der horizontalen Messschiene am Schirm und $I$ der Photostrom.
      Die Daten werden an die Kurve
      \begin{eqn}
        I = A b² \sinc(\frac{b}{λ} \sin(\frac{x - x₀}{L})) + B \label{eqn:theory-intensity}
      \end{eqn}
      gefittet.
      Dabei ist $x_0$ die Position des Hauptmaximums in der Mitte.
      \begin{table}
        \caption{Messwerte der Wellenlängenmessung.}
        \label{tab:λ-data}
        \input{table/λ-data.tex}
      \end{table}
      Abbildung \ref{fig:λ} enthält die Messwerte der Intensitätsverteilung, des Weiteren ist die angefittete Theoriekurve eingezeichnet.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/λ.pdf}
        \caption{Messwerte und gefittete Theoriekurve für die Intensitätsverteilung des Beugungsmusters eines Spaltes zur Bestimmung der Laserwellenlänge $λ$.}
        \label{fig:λ}
      \end{figure}
      Die Ergebnisse der nicht-linearen Ausgleichsrechnung sind in \ref{tab:λ-result} aufgeführt.
      Des Weiteren kann durch Umformen von \eqref{eqn:theory-intensity} die Wellenlänge bestimmt werden; sie ist ebenso wie ein Literaturwert \cite{anleitung61} in dieser Tabelle angegeben.
      \begin{table}
        \caption{
          Ermittelte Parameter für die Fitkurve \eqref{eqn:theory-intensity}.
          Des Weiteren ist die ermittelte Wellenlänge hier aufgeführt, sowie ein Literaturwert für die Wellenlänge eines \ce{HeNe}-Lasers.
        }
        \label{tab:λ-result}
        \input{table/λ-result.tex}
      \end{table}
      \FloatBarrier
    \subsection{Wellenlängenmessung mit einem Gitter}
      Die Formel
      \begin{eqn}
        x = m \frac{λ}{d} L + x₀
      \end{eqn}
      beschreibt die Position des $m$-ten Maximums relativ zum Hauptmaximum bei $x_0$ für eine Wellenlänge $λ$, wobei das Gitter mit der Gitterkonstante $d$ einen Abstand $L$ von dem Photodetektor hat.
      Tabelle \ref{tab:gitter} enthält die Ergebnisse der Wellenlängenmessung; zum Vergleich ist ein Literaturwert \cite{anleitung61} für die Wellenlänge des \ce{HeNe}-Lasers angegeben; auch hier wird die Position des Hauptmaximums gefittet.
      Abbildung \ref{fig:gitter} zeigt die Ergebnisse der Messung.
      \begin{table}
        \caption{Messwerte und Ergebnisse der Gitter-Messung.}
        \label{tab:gitter}
        \input{table/gitter.tex}
      \end{table}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/gitter.pdf}
        \caption{Ergebnisse der Gitter-Messung.}
        \label{fig:gitter}
      \end{figure}
      \FloatBarrier
    \subsection{TEM-Moden}
      Die Formel für die $\t{TEM}_{l 0}$-Mode ist durch
      \begin{eqn}
        I = A \g(\HermiteH_l(\sqrt{2} \frac{x - x₀}{w}) \exp(- \frac{\g(x - x₀)²}{w²}))^{\!\! 2} + B
      \end{eqn}
      gegeben.
      Hierbei sind $A$ und $B$ Freiheitsgrade, $w$ der minimale Radius des fokussierten Laserstrahles, $x$ die horizontale Position des Photodetektors, woraus die Intensitätsverteilung der jeweiligen Mode bestimmt wird, $x_0$ die Mitte der TEM-Mode und $\HermiteH_l(x)$ das $l$-te Hermitepolynom.
      Da $x_0$ und $w$ aus der Messung nicht bekannt sind, werden sie hier auch gefittet.
      \begin{table}
        \caption{
          Messwerte der $\t{TEM}_{10}$-Messung.
          Dabei ist $x$ die horizontale Position des Photodetektors und $I$ der gemessene Photostrom mit einem geschätztem Ablesefehler $\delta{I}$.
        }
        \label{tab:tem-1-data}
        \input{table/tem-1-data.tex}
      \end{table}
      \begin{table}
        \caption{
          Messwerte der $\t{TEM}_{20}$-Messung.
          Dabei ist $x$ die horizontale Position des Photodetektors und $I$ der gemessene Photostrom mit einem geschätztem Ablesefehler $\delta{I}$.
        }
        \label{tab:tem-2-data}
        \input{table/tem-2-data.tex}
      \end{table}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/tem-1.pdf}
        \caption{Messwerte und gefittete Intensitätskurve für $\t{TEM}_{10}$.}
        \label{fig:tem-1}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/tem-2.pdf}
        \caption{Messwerte und gefittete Intensitätskurve für $\t{TEM}_{20}$.}
        \label{fig:tem-2}
      \end{figure}
      Tabellen \ref{tab:tem-1-result} und \ref{tab:tem-2-result} enthalten die aus der nicht-linearen Ausgleichsrechnung ermittelten Fitparameter.
      Abbildungen \ref{fig:tem-1-photo} und \ref{fig:tem-2-photo} zeigen die gemessenen Moden.
      \begin{table}
        \caption{Fitparameter für $\t{TEM}_{10}$.}
        \label{tab:tem-1-result}
        \input{table/tem-1-result.tex}
      \end{table}
      \begin{table}
        \caption{Fitparameter für $\t{TEM}_{20}$.}
        \label{tab:tem-2-result}
        \input{table/tem-2-result.tex}
      \end{table}
      \begin{figure}
        \includegraphics[width=0.8\textwidth]{graphic/tem-1.jpg}
        \caption{Photo von $\t{TEM}_{10}$.}
        \label{fig:tem-1-photo}
      \end{figure}
      \begin{figure}
        \includegraphics[width=0.8\textwidth]{graphic/tem-2.jpg}
        \caption{Photo von $\t{TEM}_{20}$.}
        \label{fig:tem-2-photo}
      \end{figure}
      \FloatBarrier
  \section{Diskussion}
    Die Messung der Stabilität bestätigt die Theorie.
    Die Messung mit $L_{\t{th}} = \SI{200}{\centi\meter}$ war durch die Länge der Messschiene limitiert.

    Die Messung der Polarisation zeigt, dass der Laserstrahl linear polarisiert ist, da die charakteristische Sinuskurve gemessen wurde.

    Da hier nur der qualitative und nicht der quantitative Verlauf der Intensität für die Position der Beugungsmaxima von Belang ist, wurde der Dunkelstrom der Photodetektoren hier nicht vermessen.
    Bei der Messung der Wellenlänge mit dem Spalt war die Wahl der Messpunkte nicht optimal, was die Abweichung vom Literaturwert erklärt.
    Die Messung mit dem Gitter zeigt eine noch größere Abweichung, die aber auf diese Weise nicht erklärt werden kann.

    Die Messung der TEM-Moden passt gut zur Theorie.
  \makebibliography
\end{document}
