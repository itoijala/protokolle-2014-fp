\input{header/report.tex}

\SetExperimentNumber{64}

\addbibresource{lit.bib}

\begin{document}
  \maketitlepage{Moderne Interferometrie}{13.01.2014}{27.01.2014}
  \section{Ziel}
    Ziel des Versuchs ist es, die Brechungsindizes von Glas, Luft und Wasserstoffgas mittels eines Sagnac-Interferometers zu bestimmen.
  \section{Aufbau}
    Der Hauptteil des Versuchsaufbaus besteht aus einem Sagnac-Interferometer, der in Abbildung \ref{fig:aufbau} gezeigt wird.
    Der Interferometer besteht aus einem \ce{He}-\ce{Ne}-Laser, zwei Spiegeln, die den Laserstrahl auf ein Polarising-Beam-Splitter-Cube (PBSC) lenken, und drei weiteren Spiegeln, die den Strahl wieder auf den PBSC lenken.
    Der PBSC teilt den Strahl in seine horizontal und vertikal polarisierten Komponenten auf.
    Eine dieser Komponenten passiert den PBSC, während die andere um $\PI/2$ abgelenkt wird.
    Nachdem die Strahlen von den drei Spiegeln wieder auf den PBSC gelenkt worden sind, werden diese zu einem Strahl kombiniert, der die vierte Seite des PBSC verlässt.
    Dieser kombinierte Strahl fällt auf einen weiteren PBSC, der um $\PI/4$ gedreht ist, so dass er Komponenten beider Strahlen auf dieselbe Polarisation projiziert.
    Diese werden aus zwei Seiten des PBSC emittiert und von zwei Photodioden detektiert.
    Die Differenz der beiden Signale, die durch die Phasenverschiebung der beiden Strahlen entstehen, wird auf einen Oszilloskop gegeben, um störende Einflüsse aus der Umwelt herauszurechnen.
    \begin{figure}
      \includegraphics{graphic/aufbau.pdf}
      \caption{Versuchsaufbau \cite{anleitung64}.}
      \label{fig:aufbau}
    \end{figure}

    Für die Justierung des Interferometers werden zwei Ausrichtungstürme verwendet, die drei Löcher auf der Höhe des Strahls haben.
    So kann die Position des Strahls an mehreren Stellen kontrolliert werden, ohne den Strahl zu blockieren.
    Als erstes werden die Ausrichtungstürme zwischen dem PBSC und einem der Spiegel platziert.
    Dann werden die Spiegel, die den Strahl vom Laser auf den PBSC lenken, so justiert, dass der Strahl auf die Mitte des Spiegels trifft.
    Daraufhin werden die Türme zwischen dem PBSC und dem anderen Spiegel platziert und der PBSC so gedreht, dass der Strahl auf die Mitte des Spiegels trifft.
    Die Türme werden am dritten Spiegel platziert und die anderen Spiegel so justiert, dass beide Strahlen den selben Punkt des Spiegels treffen.
    Durch Verschiebung des zweiten Spiegels zwischen Laser und PBSC kann der Strahl in zwei räumlich getrennte Teile zerlegt werden.
    Dies ist notwendig, da für Interferenz ein Strahl durch die, beziehungsweise neben der Gaskammer, die innerhalb des Sagnac Interferometers platziert wird, verlaufen soll.
  \section{Theorie}
    \subsection{Kontrast}
      Der Kontrast ist allgemein durch
      \begin{eqn}
        C = \frac{I_+ - I_-}{I_+ + I_-} \eqlabel{eqn:kontrast}
      \end{eqn}
      definiert, wobei $I_±$ die maximale beziehungsweise minimale Intensität sind.
      Es wird ein in $z$-Richtung laufender und linear in $x$-Richtung \footnote{Im Experiment ist der Strahl mit einem Winkel von \SI{45}{\degree} zum Tisch polarisiert. In der Theorie ist das Koordinatensystem gedreht, damit die Formel vereinfacht werden.} polarisierter Laserstrahl mit der elektrischen Feldstärke $\sqrt{2} E_0$ verwendet und die Intensität auf der Diode gemessen.
      Dieser Strahl wird durch einen Polarisationsfilter, der auf den Winkel $δ$ eingestellt ist und vor dem Sagnac Interferometer steht, in die orthogonalen Komponenten
      \begin{eqns}
        \v{E}_x &=& \frac{1}{\sqrt{2}} E_0 \cos.δ. \uv{x} \\
        \v{E}_y &=& \frac{1}{\sqrt{2}} E_0 \sin.δ. \uv{y}
      \end{eqns}
      aufgespalten.
      Die $y$-Komponente erhält durch das Durchlaufen der Probe die Phase $φ$.
      Nach dem Interferometer werden beide Komponenten auf die Richtung $\uv{e} = \g(\uv{x} + \uv{y}) / \sqrt{2}$ projiziert, was
      \begin{eqns}
        \v{E}_{x, e} &=& E_0 \cos.δ. \uv{e} \\
        \v{E}_{y, e} &=& E_0 \sin.δ. \E^{\I φ} \uv{e}
      \end{eqns}
      ergibt.
      Die maximale beziehungsweise minimale Intensität, die sich aus dem Quadrat des elektrischen Feldes ergibt, entsteht für $\E^{\I φ} = ±1$ durch konstruktive beziehungsweise destruktive Interferenz.
      Es folgt
      \begin{eqn}
        I_± \propto E_0² \g(1 ± 2 \abs{\sin.δ. \cos.δ.}) \.,
      \end{eqn}
      woraus sich der Kontrast
      \begin{eqn}
        C = \abs{\sin.2 δ.} \eqlabel{eqn:kontrast-funktion}
      \end{eqn}
      ergibt.
    \subsection{Gas}
      Der Brechungsindex ist durch
      \begin{eqn}
        n = \frac{c}{v}
      \end{eqn}
      definiert, wobei $c$ die Lichtgeschwindigkeit im Vakuum,
      \begin{eqn}
        v = \frac{ω}{k}
      \end{eqn}
      die Phasengeschwindigkeit im Material,
      \begin{eqn}
        ω = 2 \PI \frac{c}{λ}
      \end{eqn}
      die Kreisfrequenz des Lichts, $k$ die Wellenzahl und $λ$ die Wellenlänge sind.
      Insgesamt ergibt sich
      \begin{eqn}
        k = \frac{2 \PI}{λ} n \..
      \end{eqn}
      Durch das Durchlaufen eines Gasvolumens der Länge $L$ ergibt sich die zusätzliche Phase
      \begin{eqn}
        φ = \frac{2 \PI}{λ} \g(n - 1) L \.,
      \end{eqn}
      die zu
      \begin{eqn}
        M = \frac{φ}{2 \PI} = \frac{L}{λ} \g(n - 1)
      \end{eqn}
      Maxima, die beim Erhöhen des Druckes durchlaufen werden, führt.
      Daraus kann der Brechungsindex durch
      \begin{eqn}
        n = \frac{λ}{L} M + 1 \eqlabel{eqn:n-air}
      \end{eqn}
      berechnet werden.

      Es gilt außerdem die Lorentz–Lorenz-Gleichung
      \begin{eqn}
        n \approx \sqrt{1 + \frac{3 A p}{R T}} \label{eqn:lorentz-lorenz}
      \end{eqn}
      für den Zusammenhang zwischen Druck $p$ und Brechungsindex $n$.
      Dabei sind $R$ die molare Gaskonstante, $T$ die Temperatur und $A$ die molare Refraktivität.
    \subsection{Glas}\label{glass}
      Der Brechungsindex eines Festkörpers kann gemessen werden, indem eine Platte der Dicke $d$ des Materials in einem Interferometer rotiert wird, während die Anzahl der Maxima gemessen wird.
      Es treten zwei Effekte auf, die zu einer zusätzlichen Phase führen: der Brechungsindex selbst und der geometrische Effekt der Brechung.
      Abbildung \ref{fig:glas-geometrie} zeigt die Geometrie der Platte.
      Diese führt nach zweimaliger Brechung des Lichtstrahls in der Glasplatte zu einem parallel zum einfallenden Strahl ausfallenden Strahl.
      \begin{figure}
        \includegraphics{graphic/glas-geometrie.pdf}
        \caption{Geometrie der Glasplatte \cite{anleitung64}.}
        \label{fig:glas-geometrie}
      \end{figure}
      Ein Strahl, der die Platte durchläuft, erhält die zusätzliche Phase
      \begin{eqn}
        φ = \frac{2 \PI}{λ} \g(n \overline{\mathup{CD}} - \overline{\mathup{AB}})
      \end{eqn}
      gegenüber einem Strahl, der nicht durch die Platte verläuft.
      Dabei gilt
      \begin{eqn}
        \overline{\mathup{CD}} = \frac{d}{\cos.θ'.} = \frac{\overline{\mathup{AB}}}{\cos(θ - θ')} \.,
      \end{eqn}
      was insgesamt
      \begin{eqn}
        φ = \frac{2 \PI}{λ} d \frac{n - \cos(θ - θ')}{\cos.θ'.}
      \end{eqn}
      ergibt.
      Für die Anzahl der Maxima, die bei Variation von $θ$ auftreten, relativ zu einer Platte, die orthogonal zum Strahl steht, ergibt sich
      \begin{eqn}
        M = \frac{d}{λ} \g(\frac{n - \cos(θ - θ')}{\cos.θ'.} - \g(n - 1)) \..
      \end{eqn}

      Bei diesem Versuch werden zwei Platten verwendet, die jeweils von einem der Strahlen, die das Interferometer durchlaufen, durchlaufen werden.
      Die Platten haben den festen relativen Winkel $θ_0$ und die Gesamtanordnung wird um den Gesamtwinkel $α$ gedreht.
      Mit dem Snellius’schen Brechungsgesetz $θ = n \sin.θ'.$, das hier in linearisierter Form angewendet wird, ergibt sich für kleine Winkel $θ_0$ und $α$
      \begin{eqn}
        M = \frac{2 d θ_0 α}{λ} \g(1 - \frac{1}{n}) \.,
      \end{eqn}
      woraus sich der Brechungsindex zu
      \begin{eqn}
        n = \g(1 - \frac{λ M}{2 d θ_0 α})^{\!\! -1} \eqlabel{eqn:n-glass}
      \end{eqn}
      berechnet.
  \section{Durchführung}
    \subsection{Kontrast}
      Zur Messung des Kontrasts des Interferometers wird ein Polarisationsfilter zwischen Laser und PBSC platziert.
      Der Doppelglashalter wird zwischen dem PBSC und einem der Spiegel so platziert, dass jeweils ein Strahl durch eine Platte geht.
      Der Winkel des Filters wird variiert und für jeden Winkel das maximale und minimale Signal eines der Photodioden gemessen, während der Winkel des Glases zwischen \SI{0}{\degree} und \SI{10}{\degree} variiert wird.
      Nach der Messung wird der Filter so eingestellt, dass der Kontrast maximal ist.
    \subsection{Gas}
      In diesem und im nächsten Versuchsteil wird die Differenz beider Photodioden verwendet.
      Statt des Doppelglashalters wird eine Gaszelle in eine der Strahlen gestellt.
      Diese wird mittels einer Vakuumpumpe evakuiert und mit einem Barometer verbunden.
      Die Zelle wird langsam mit dem zu untersuchenden Gas gefüllt und dabei die kumulative Anzahl Maxima in Abhängigkeit vom Druck gemessen.
      Ein Thermometer wird verwendet, um die Raumtemperatur zu bestimmen.
    \subsection{Glas}
      Das Differenzsignal wird mit einem Zähler verbunden, der die Maxima zählt.
      Der Doppelglashalter wird wieder in den Strahl platziert.
      Der Winkel wird von \SI{0}{\degree} bis $α$ erhöht und die Anzahl der Maxima, die vom Zähler angezeigt werden, notiert.
      Die Messung wird mehrmals wiederholt, um die Genauigkeit zu erhöhen.
  \section{Messwerte und Auswertung}
    \subsection{Kontrast}
      Tabelle \ref{tab:C} enthält den eingestellten Winkel $δ$ des Polarisationsfilters, die minimale und maximale Intensität $I_∓$ und den Kontrast $C$, der mit Formel \eqref{eqn:kontrast} berechnet wurde.
      \begin{table}
        \caption{Messwerte zur Bestimmung des Kontrastes. Hierbei sind $δ$ der Winkel des Polarisationsfilters, $I_∓$ die minimale und maximale Intensität und $C$ der Kontrast.}
        \label{tab:C}
        \input{build/table/C.tex}
      \end{table}
      In Abbildung \ref{fig:C} sind die Kontrastwerte in Abhängigkeit des Winkels $δ$ aufgetragen.
      Entsprechend Formel \eqref{eqn:kontrast-funktion} werden mit der Funktion
      \begin{eqn}
        \f{C}(δ) = a \abs{\sin(2 \g(δ - b))} + c
      \end{eqn}
      die Parameter $a$, $b$ und $c$ mit einer Ausgleichsrechnung bestimmt.
      \begin{figure}
        \includegraphics{build/graphic/C.pdf}
        \caption{Messwerte und gefittete Funktion für die Kontrastbestimmung.}
        \label{fig:C}
      \end{figure}
      Tabelle \ref{tab:C-fit} enthält die Ergebnisse der Ausgleichsrechnung.
      \begin{table}
        \caption{Ergebnisse der Ausgleichsrechnung für die Funktion aus Abbildung \ref{fig:C}.}
        \label{tab:C-fit}
        \input{build/table/C-fit.tex}
      \end{table}
      \FloatBarrier
    \subsection{Luft} \label{air}
      Tabelle \ref{tab:gas-constants} enthält die im Folgenden verwendeten Konstanten. Hierbei ist $R$ die molare Gaskonstante, $λ$ die Wellenlänge des einfallenden Laserstrahls, $L$ die Länge der Gaszelle und $T$ die gemessene Temperatur des Versuchaufbaus.
      \begin{table}
        \caption{Verwendete Konstanten. Hierbei ist $R$ die molare Gaskonstante \cite{scipy}, $L$ die Länge der Gaszelle, $λ$ die Wellenlänge des einfallenden Laserstrahls und $T$ die Temperatur des Versuchaufbaus.}
        \label{tab:gas-constants}
        \input{table/gas-constants.tex}
      \end{table}

      Tabelle \ref{tab:air-data} enthält die Messwerte für die Anzahl der Maxima $M$ und den Druck $p$.
      Mit Formel \eqref{eqn:n-air} kann der Brechungsindex $n$ für jedes Wertepaar berechnet werden; die Ergebnisse stehen ebenfalls in Tabelle \ref{tab:air-data}.
      \begin{table}
        \caption{Druck $p$, Anzahl der Maxima $M$ und resultierender Brechungsindex $n$.}
        \label{tab:air-data}
        \input{table/air-data.tex}
      \end{table}
      Um die molare Refraktivität mit einer linearen Ausgleichsrechnung zu ermitteln, werden entsprechend des Lorentz–Lorenz-Gesetzes \eqref{eqn:lorentz-lorenz} die Messwerte für die Brechungsindices quadriert.
      Dies ergibt die Gleichung
      \begin{eqn}
        n² = a p + b \., \label{eqn:fit-gas}
      \end{eqn}
      deren Parameter $a$ und $b$ mit einer linearen Ausgleichsrechnung bestimmt werden können.
      Die Ergebnisse der Ausgleichsrechnung stehen in Tabelle \ref{tab:air}.
      Die Messwerte und die resultierende Gerade aus der Ausgleichsrechnung sind in Abbildung \ref{fig:air} eingezeichnet.
      \begin{figure}
        \includegraphics{graphic/air.pdf}
        \caption{Messwerte und Fitfunktion für das Wasserstoffgas.}
        \label{fig:air}
      \end{figure}
      Für die molare Reflektivität $A$ gilt
      \begin{eqn}
        A = \frac{a R T}{3} \.;
      \end{eqn}
      sie ist ebenfalls in Tabelle \ref{tab:air} aufgeführt.
      Mit Bestimmung dieser unbekannten Konstanten können die Brechungsindices extrapoliert werden, um sie mit einen Literaturwert zu vergleichen; Tabelle \ref{tab:air} enthält sowohl den extrapolierten gemessenen Brechungsindex, als auch den Literaturwert.
      \begin{table}
        \caption{Fitparameter $a$ und $b$ für die Gerade aus Abbildung \ref{fig:air}, daraus resultierende molare Refraktivität $A$, daraus extrapolierter Messwert und ein Literaturwert \cite{n-air}.}
        \label{tab:air}
        \input{table/air.tex}
      \end{table}
      \FloatBarrier
    \subsection{Wasserstoffgas}
      Die Auswertung bei Wasserstoffgas läuft analog zu Kapitel \ref{air}.
      Die Definition der Größen und die Vorgehensweise wird übernommen.

      Tabelle \ref{tab:H2-data} enthält die Messwerte und die berechneten Brechungsindices.
      In Abbildung \ref{fig:H2} sind die angepassten Messwerte und die lineare Funktion \eqref{eqn:fit-gas} aus der Ausgleichsrechnung eingezeichnet.
      \begin{table}
        \caption{Druck $p$, Anzahl der Maxima $M$ und resultierender Brechungsindex $n$.}
        \label{tab:H2-data}
        \input{table/H2-data.tex}
      \end{table}
      \begin{figure}
        \includegraphics{graphic/H2.pdf}
        \caption{Messwerte und Fitfunktion für das Wasserstoffgas.}
        \label{fig:H2}
      \end{figure}
      Tabelle \ref{tab:H2} enthält die Ergebnisse der Ausgleichsrechnung, das Ergebnis für die molare Reflektivität, den extrapolierten Brechungsindex, sowie den Literaturwert dafür.
      \begin{table}
        \caption{Fitparameter $a$ und $b$ für die Gerade aus Abbildung \ref{fig:H2}, daraus resultierende molare Refraktivität $A$, daraus extrapolierter Messwert und ein Literaturwert \cite{n-H2}.}
        \label{tab:H2}
        \input{table/H2.tex}
      \end{table}
      \FloatBarrier
    \subsection{Glas}
      Tabelle \ref{tab:glas-data} enthält die Messwerte für die Anzahl der Maxima $M$ sowie den gemittelten Wert dafür.
      \begin{table}
        \caption{Anzahl der Maxima bei Doppelglas im Interferometer.}
        \label{tab:glas-data}
        \input{build/table/glas-data.tex}
      \end{table}
      In Tabelle \ref{tab:glass} stehen die Wellenlänge $λ$ des einfallenden Laserstrahls, die Dicke $d$ der Glasplatten und die Winkel $θ_0$ und $α$ (siehe \ref{glass}).
      Der Brechungsindex $n$ wird mit Formel \eqref{eqn:n-glass} berechnet und steht auch in Tabelle \ref{eqn:n-glass}.
      \begin{table}
        \caption{Brechungsindex für Doppelglas und für dessen Berechnung verwendete Parameter.}
        \label{tab:glass}
        \input{build/table/glas.tex}
      \end{table}
      \FloatBarrier
  \section{Diskussion}
    Die $\abs{\sin.2 δ.}$-Abhängigkeit des Kontrasts wird durch die Messung bestätigt.
    Der Kontrast sinkt nicht auf $0$, da der Polarisationsfilter bei zur Polarisierung des Laserstrahls senkrecht stehender Stellung nicht perfekt arbeitet und immer noch eine endliche Amplitude durchlässt.
    Der Kontrast wird nicht genau $1$, da dafür $I_{-} = 0$ sein müsste; dies tritt durch in den Detektor einfallendes Umgebungslicht nicht ein.

    Bei der Luftmessung liegt das Ergebnis für den Brechungsindex $5 σ$ vom Literaturwert entfernt.
    Eine Fehlerquelle dieser Messung besteht in der Unterschätzung der tatsächlichen Anzahl der Maxima, da immer nur von einem ganzzahligen Wert ausgegangen wird; somit wird der Rest vernachlässigt.

    Die Messung für die Bestimmung des Brechungsindex von Wasserstoffgas lief sehr gut.
    Auch hier kann die Lorentz–Lorenz-Gleichung bestätigt werden und die Übereinstimmung zwischen Mess- und Literaturwert ist sehr gut.

    Die Messung an Glas ergibt einen viel größeren Fehler als die an Gas.
    Die Bestimmung der Anzahl der Maxima war durch die Methodik nicht ganz exakt, da es schwierig ist, den Winkel des Glashalters genau einzustellen.
  \makebibliography
\end{document}
