import numpy as np

from scipy.constants import C2K, K2C

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

R, p_0 = lt.constant('molar gas constant', 'standard atmosphere')

λ = np.loadtxt('data/λ', skiprows=2) * 1e-9
L, δL = np.loadtxt('data/L', skiprows=2) * 1e-3
L = unc.ufloat(L, δL)
T = C2K(np.loadtxt('data/T', skiprows=2))

for gas in ['H2', 'air']:
    M, p = np.loadtxt('data/{}'.format(gas), unpack=True, skiprows=2)
    p *= 1e2
    n_lit, T_lit = np.loadtxt('data/n_lit-{}'.format(gas), skiprows=2)
    T_lit = C2K(T_lit)

    n = λ * M / L + 1

    a, b = lt.linregress(p, n**2)
    A = a * R * T / 3
    n_ext = unp.sqrt(1 + 3 * (A / T * T_lit) * p_0 / (R * T_lit))

    fig, ax = plt.subplots(1, 1)
    x = np.linspace(0, 120000)
    ax.plot(x * 1e-2, (lt.noms(a * x + b) - 1) * 1e4, 'b-')
    ax.plot(p * 1e-2, (lt.noms(n**2) - 1) * 1e4, 'rx')
    ax.set_xlabel(r'$\silabel{p}{\milli\bar}$')
    ax.set_ylabel(r'$\g(n² - 1) \mathbin{/} \num{e-4}$')
    fig.savefig('build/graphic/{}.pdf'.format(gas))

    tab = lt.SymbolColumnTable()
    tab.add_si_column('M', M, places=0)
    tab.add_si_column('p', p * 1e-2, r'\milli\bar', places=0)
    tab.add_si_column('n', n)
    tab.savetable('build/table/{}-data.tex'.format(gas))

    tab = lt.SymbolRowTable()
    tab.add_si_row('a', a * 1e9, r'\per\bar', exp='e-9')
    tab.add_si_row('b', b)
    tab.add_hrule()
    tab.add_si_row('A', A * 1e6, '\cubic\centi\meter\per\mole')
    tab.add_hrule()
    tab.add_si_row(r'n_{\t{ext}}', n_ext)
    tab.add_si_row(r'n_{\t{lit}}', n_lit, figures=7)
    tab.add_si_row(r'p_{\t{lit}}', lt.noms(p_0) * 1e-2, r'\milli\bar', places=2)
    tab.add_si_row(r'T_{\t{lit}}', K2C(T_lit), r'\degreeCelsius', places=0)
    tab.savetable('build/table/{}.tex'.format(gas))

tab = lt.SymbolRowTable()
tab.add_si_row('R', R, r'\joule\per\mole\per\kelvin')
tab.add_si_row('λ', λ * 1e9, r'\nano\meter', places=3)
tab.add_si_row('L', L * 1e3, r'\milli\meter')
tab.add_si_row('T', K2C(T), r'\degreeCelsius', places=1)
tab.savetable('build/table/gas-constants.tex')
