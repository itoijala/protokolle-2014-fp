import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

δ, I_min, I_max = np.loadtxt('data/C', unpack=True, skiprows=2)
C = np.abs(I_max - I_min) / np.abs(I_max + I_min)

δ = np.radians(δ)

def contrast(x, a, b, c):
    return a * abs(np.sin(2 * (x - b))) + c

a, b, c = lt.curve_fit(contrast, δ, C, p0=(1, 0, 0))

fig, ax = plt.subplots(1, 1)
x = np.linspace(0, 3.5, 1000)
ax.plot(x, contrast(x, *lt.noms(a, b, c)), 'b-')
ax.plot(δ, C, 'rx')
ax.set_xticks(np.pi / 8 * np.arange(9))
ax.set_xticklabels([r'$\frac{{{}}}{{8}} \PI$'.format(n) for n in range(9)])
ax.set_xlabel(r'$\silabel{δ}{\radian}$')
ax.set_ylabel(r'$C$')
ax.set_xlim(-0.05, np.pi + 0.05)
ax.set_ylim(0.05, 0.8)
fig.savefig('build/graphic/C.pdf')

tab = lt.SymbolColumnTable()
tab.add_si_column('δ', np.degrees(δ), r'\degree', places=0)
tab.add_si_column(r'I_+', I_min, r'\volt', places=2)
tab.add_si_column(r'I_-', I_max, r'\volt', places=2)
tab.add_si_column('C', C, places=2)
tab.savetable('build/table/C.tex')

tab = lt.SymbolRowTable()
tab.add_si_row('a', a)
tab.add_si_row('b', b, r'\radian')
tab.add_si_row('c', c)
tab.savetable('build/table/C-fit.tex')
