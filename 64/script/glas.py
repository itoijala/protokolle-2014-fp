import numpy as np

import uncertainties as unc
import uncertainties.unumpy as unp

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf, '')

import labtools as lt

M = np.loadtxt('data/glas', unpack=True, skiprows=2)
λ = np.loadtxt('data/λ', skiprows=2) * 1e-9
d = np.loadtxt('data/d', skiprows=2) * 1e-3
α, θ_0, δθ_0 = np.radians(np.loadtxt('data/angles', unpack=True, skiprows=2))
θ_0 = unc.ufloat(θ_0, δθ_0)

M_mean = lt.mean(M)

n = 1 / (1 - λ * M_mean / (2 * d * α * θ_0))

tab = lt.SymbolColumnTable()
tab.add_si_column('M', M, places=0)

tab2 = lt.SymbolRowTable()
tab2.add_si_row('\mean{M}', M_mean)

tab3 = lt.Table()
tab3.add(lt.Row())
tab3.add_hrule()
tab3.add(lt.Row())
tab3.add(lt.Column([tab, tab2], "@{}c@{}c@{}"))
tab3.savetable('build/table/glas-data.tex')

tab = lt.SymbolRowTable()
tab.add_si_row('λ', λ * 1e9, r'\nano\meter', places=3)
tab.add_si_row('d', d * 1e3, r'\milli\meter', places=0)
tab.add_si_row('θ_0', unp.degrees(θ_0), r'\degree')
tab.add_si_row('α', np.degrees(α), r'\degree', places=0)
tab.add_hrule()
tab.add_si_row('n', n)
tab.savetable('build/table/glas.tex')
